<?php

namespace App\Repository;

use App\Entity\Article;
use App\Request\Article\GetArticlesRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    public function save(Article $article): void
    {
        $this->_em->persist($article);
        $this->_em->flush();
    }

    public function findAllByFilters(GetArticlesRequest $request): array
    {
        $queryBuilder = ($this->createQueryBuilder('a'));
        $this->addVisibilityFilter($queryBuilder, $request->isDeletedArticles());
        $this->addSearchFilter($queryBuilder, $request->getSearchText());
        $this->addDateFilter($queryBuilder, $request->getSearchDateStart(), $request->getSearchDateEnd());
        $this->addPagination($queryBuilder, $request->getStart(), $request->getLimit());
        $this->addUserFilter($queryBuilder, $request->getUserId());

        return $queryBuilder
            ->orderBy('a.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    private function addVisibilityFilter(QueryBuilder $queryBuilder, bool $deleted): void
    {
        if ($deleted) {
            $queryBuilder->andWhere('a.deletedAt IS NOT NULL');
        } else{
            $queryBuilder->andWhere('a.deletedAt IS NULL');
        }
    }

    private function addSearchFilter(QueryBuilder $queryBuilder, ?string $text): void
    {
        if ($text) {
            $queryBuilder
                ->orWhere('a.title LIKE =:name')
                ->setParameter('title', '%'.$text.'%');
        }
    }

    private function addDateFilter(QueryBuilder $queryBuilder, ?\DateTime $fromDate, ?\DateTime $toDate): void
    {
        if ($fromDate) {
            $queryBuilder->andWhere('a.createdAt >= :fromDate')
                ->setParameter('fromDate', $fromDate);
        }

        if ($toDate) {
            $queryBuilder->andWhere('a.createdAt <= :toDate')
                ->setParameter('toDate', $toDate);
        }
    }

    private function addPagination(QueryBuilder $queryBuilder, ?int $start, ?int $limit): void
    {
        $queryBuilder
            ->setFirstResult($start ? $start : 0)
            ->setMaxResults($limit ? $limit : 15);
    }

    private function addUserFilter(QueryBuilder $queryBuilder, ?int $userId): void
    {
        if ($userId) {
            $queryBuilder->select('a', 'u')
                ->leftJoin('a.user', 'u')
                ->andWhere('u = :user')
                ->setParameter('user', $userId);
        }
    }
}

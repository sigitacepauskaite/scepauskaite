<?php

namespace App\Repository;

use App\Entity\PlacePhoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlacePhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlacePhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlacePhoto[]    findAll()
 * @method PlacePhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlacePhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlacePhoto::class);
    }

    public function save(PlacePhoto $placePhoto): void
    {
        $this->_em->persist($placePhoto);
        $this->_em->flush();
    }
}

<?php

namespace App\Repository;

use App\Entity\Trip;
use App\Entity\User;
use App\Request\Trip\GetTripsRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Trip|null find($id, $lockMode = null, $lockVersion = null)
 * @method Trip|null findOneBy(array $criteria, array $orderBy = null)
 * @method Trip[]    findAll()
 * @method Trip[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Trip::class);
    }

    public function save(Trip $trip): void
    {
        $this->_em->persist($trip);
        $this->_em->flush();
    }

    public function findAllByFilters(GetTripsRequest $request, User $user): array
    {
        $queryBuilder = ($this->createQueryBuilder('t'));
        $this->addUserFilter($queryBuilder, $user);
        $this->addVisibilityFilter($queryBuilder, $request->isDeletedTrips());
        $this->addSearchFilter($queryBuilder, $request->getSearchText());
        $this->addDateFilter($queryBuilder, $request->getSearchDateStart(), $request->getSearchDateEnd());
        $this->addPagination($queryBuilder, $request->getStart(), $request->getLimit());

        return $queryBuilder
            ->orderBy('t.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    private function addVisibilityFilter(QueryBuilder $queryBuilder, bool $deleted): void
    {
        if (!$deleted) {
            $queryBuilder->andWhere('t.deletedAt IS NULL');
        }
    }

    private function addSearchFilter(QueryBuilder $queryBuilder, ?string $text): void
    {
        if ($text) {
            $queryBuilder
                ->orWhere('t.name LIKE =:name')
                ->setParameter('name', '%'.$text.'%');
        }
    }

    private function addDateFilter(QueryBuilder $queryBuilder, ?\DateTime $fromDate, ?\DateTime $toDate): void
    {
        if ($fromDate) {
            $queryBuilder->andWhere('t.createdAt >= :fromDate')
                ->setParameter('fromDate', $fromDate);
        }

        if ($toDate) {
            $queryBuilder->andWhere('t.createdAt <= :toDate')
                ->setParameter('toDate', $toDate);
        }
    }

    private function addPagination(QueryBuilder $queryBuilder, ?int $start, ?int $limit): void
    {
        $queryBuilder
            ->setFirstResult($start ? $start : 0)
            ->setMaxResults($limit ? $limit : 15);
    }

    private function addUserFilter(QueryBuilder $queryBuilder, User $user): void
    {
        $queryBuilder
            ->andWhere('t.user = :user')
            ->setParameter('user', $user);
    }
}

<?php

namespace App\Repository;

use App\Entity\TripStop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TripStop|null find($id, $lockMode = null, $lockVersion = null)
 * @method TripStop|null findOneBy(array $criteria, array $orderBy = null)
 * @method TripStop[]    findAll()
 * @method TripStop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TripStopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TripStop::class);
    }

    public function save(TripStop $tripStop): void
    {
        $this->_em->persist($tripStop);
        $this->_em->flush();
    }
}

<?php

namespace App\Repository;

use App\Entity\User;
use App\Request\User\GetUsersRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function save(User $user): void
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findAllByFilters(GetUsersRequest $request): array
    {
        $queryBuilder = ($this->createQueryBuilder('u'));
        $this->addVisibilityFilter($queryBuilder, $request->isDeletedUsers());
        $this->addSearchFilter($queryBuilder, $request->getSearchText());
        $this->addDateFilter($queryBuilder, $request->getSearchDateStart(), $request->getSearchDateEnd());
        $this->addPagination($queryBuilder, $request->getStart(), $request->getLimit());

        return $queryBuilder
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    private function addVisibilityFilter(QueryBuilder $queryBuilder, bool $deleted): void
    {
        if (!$deleted) {
            $queryBuilder->andWhere('u.deletedAt IS NULL');
        }
    }

    private function addSearchFilter(QueryBuilder $queryBuilder, ?string $text): void
    {
        if ($text) {
            $queryBuilder
                ->orWhere('u.firstName LIKE =:firstNameText')
                ->setParameter('firstNameText', '%'.$text.'%')
                ->orWhere('u.lastName LIKE =:lastNameText')
                ->setParameter('lastNameText', '%'.$text.'%')
                ->orWhere('u.email LIKE =:emailText')
                ->setParameter('emailText', '%'.$text.'%');
        }
    }

    private function addDateFilter(QueryBuilder $queryBuilder, ?\DateTime $fromDate, ?\DateTime $toDate): void
    {
        if ($fromDate) {
            $queryBuilder->andWhere('u.createdAt >= :fromDate')
                ->setParameter('fromDate', $fromDate);
        }

        if ($toDate) {
            $queryBuilder->andWhere('u.createdAt <= :toDate')
                ->setParameter('toDate', $toDate);
        }
    }

    private function addPagination(QueryBuilder $queryBuilder, ?int $start, ?int $limit): void
    {
        $queryBuilder
            ->setFirstResult($start ? $start : 0)
            ->setMaxResults($limit ? $limit : 15);
    }
}

<?php

namespace App\Repository;

use App\Entity\Country;
use App\Request\Article\GetArticlesRequest;
use App\Request\Article\GetCountriesRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Country|null find($id, $lockMode = null, $lockVersion = null)
 * @method Country|null findOneBy(array $criteria, array $orderBy = null)
 * @method Country[]    findAll()
 * @method Country[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Country::class);
    }

    public function save(Country $country): void
    {
        $this->_em->persist($country);
        $this->_em->flush();
    }

    public function findAllByFilters(GetCountriesRequest $request): array
    {
        $queryBuilder = ($this->createQueryBuilder('a'));
        $this->addVisibilityFilter($queryBuilder, $request->isDeletedCountries());
        $this->addSearchFilter($queryBuilder, $request->getSearchText());
        $this->addDateFilter($queryBuilder, $request->getSearchDateStart(), $request->getSearchDateEnd());
        $this->addPagination($queryBuilder, $request->getStart(), $request->getLimit());

        return $queryBuilder
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    private function addVisibilityFilter(QueryBuilder $queryBuilder, bool $deleted): void
    {
        if ($deleted) {
            $queryBuilder->andWhere('a.deletedAt IS NOT NULL');
        } else{
            $queryBuilder->andWhere('a.deletedAt IS NULL');
        }
    }

    private function addSearchFilter(QueryBuilder $queryBuilder, ?string $text): void
    {
        if ($text) {
            $queryBuilder
                ->orWhere('a.name LIKE =:name')
                ->setParameter('name', '%'.$text.'%');
        }
    }

    private function addDateFilter(QueryBuilder $queryBuilder, ?\DateTime $fromDate, ?\DateTime $toDate): void
    {
        if ($fromDate) {
            $queryBuilder->andWhere('a.createdAt >= :fromDate')
                ->setParameter('fromDate', $fromDate);
        }

        if ($toDate) {
            $queryBuilder->andWhere('a.createdAt <= :toDate')
                ->setParameter('toDate', $toDate);
        }
    }

    private function addPagination(QueryBuilder $queryBuilder, ?int $start, ?int $limit): void
    {
        $queryBuilder
            ->setFirstResult($start ? $start : 0)
            ->setMaxResults($limit ? $limit : 15);
    }
}

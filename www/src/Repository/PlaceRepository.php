<?php

namespace App\Repository;

use App\Entity\Place;
use App\Request\Place\GetPlacesRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Base;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Place|null find($id, $lockMode = null, $lockVersion = null)
 * @method Place|null findOneBy(array $criteria, array $orderBy = null)
 * @method Place[]    findAll()
 * @method Place[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Place::class);
    }

    public function save(Place $place): void
    {
        $this->_em->persist($place);
        $this->_em->flush();
    }

    public function findAllByFilters(GetPlacesRequest $request): array
    {
        $queryBuilder = ($this->createQueryBuilder('p'));
        $this->addStatusFilter($queryBuilder, $request->getStatuses());
        $this->addVisibilityFilter($queryBuilder, $request->isDeletedPlaces());
        $this->addSearchFilter($queryBuilder, $request->getSearchText());
        $this->addDateFilter($queryBuilder, $request->getSearchDateStart(), $request->getSearchDateEnd());
        $this->addLocationsFilter($queryBuilder, $request->getLocationFilters());
        $this->addActivitiesFilter($queryBuilder, $request->getActivityFilters());
        $this->addPagination($queryBuilder, $request->getStart(), $request->getLimit());

        return $queryBuilder
            ->orderBy('p.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    private function addStatusFilter(QueryBuilder $queryBuilder, array $statuses): void
    {
        if (!empty($statuses)) {
            $queryBuilder->andWhere('p.status IN (:statuses)');
            $queryBuilder->setParameter('statuses', $statuses);
        }
    }

    private function addVisibilityFilter(QueryBuilder $queryBuilder, bool $deleted): void
    {
        if (!$deleted) {
            $queryBuilder->andWhere('p.deletedAt IS NULL');
        }
    }

    private function addSearchFilter(QueryBuilder $queryBuilder, ?string $text): void
    {
        if ($text) {
            $queryBuilder
                ->andWhere('p.name LIKE :name')
                ->setParameter('name', '%' . $text . '%');
        }
    }

    private function addDateFilter(QueryBuilder $queryBuilder, ?\DateTime $fromDate, ?\DateTime $toDate): void
    {
        if ($fromDate) {
            $queryBuilder->andWhere('p.createdAt >= :fromDate')
                ->setParameter('fromDate', $fromDate);
        }

        if ($toDate) {
            $queryBuilder->andWhere('p.createdAt <= :toDate')
                ->setParameter('toDate', $toDate);
        }
    }

    private function addLocationsFilter(QueryBuilder $queryBuilder, array $locations): void
    {
        $orx = new Orx();
        foreach ($locations as $key => $location) {
            $orx->add($queryBuilder->expr()->like('p.address', ':location'.$key));
            $queryBuilder->setParameter('location'.$key, '%' . $location . '%');;
        }

        $queryBuilder->andWhere($orx);
    }

    private function addActivitiesFilter(QueryBuilder $queryBuilder, array $activities): void
    {
        $orx = new Orx();
        foreach ($activities as $key => $activity) {
            $orx->add($queryBuilder->expr()->like('p.tags', ':activity'.$key));
            $queryBuilder->setParameter('activity'.$key, '%' . $activity . '%');;
        }

        $queryBuilder->andWhere($orx);
    }

    private function addPagination(QueryBuilder $queryBuilder, ?int $start, ?int $limit): void
    {
        $queryBuilder
            ->setFirstResult($start ? $start : 0)
            ->setMaxResults($limit ? $limit : 15);
    }
}

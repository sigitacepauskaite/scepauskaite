<?php

namespace App\Repository;

use App\Entity\ArticlePhoto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ArticlePhoto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticlePhoto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticlePhoto[]    findAll()
 * @method ArticlePhoto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticlePhotoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ArticlePhoto::class);
    }

    public function save(ArticlePhoto $articlePhoto): void
    {
        $this->_em->persist($articlePhoto);
        $this->_em->flush();
    }

    public function persist(ArticlePhoto $articlePhoto): void
    {
        $this->_em->persist($articlePhoto);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }
}

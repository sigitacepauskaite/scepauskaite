<?php

namespace App\Repository;

use App\Entity\PlaceVisit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlaceVisit|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlaceVisit|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlaceVisit[]    findAll()
 * @method PlaceVisit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlaceVisitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlaceVisit::class);
    }

    public function save(PlaceVisit $placeVisit): void
    {
        $this->_em->persist($placeVisit);
        $this->_em->flush();
    }
}

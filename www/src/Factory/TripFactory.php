<?php

namespace App\Factory;

use App\Entity\Place;
use App\Entity\PlacePhoto;
use App\Entity\Trip;
use App\Entity\TripStop;
use App\Entity\User;
use App\Request\Trip\CreateTripRequest;
use App\Request\Trip\UpdateTripsRequest;
use App\Response\Country\CountryResponse;
use App\Response\Photo\PhotoResponse;
use App\Response\Place\PlaceResponse;
use App\Response\Trip\CreateTripResponse;
use App\Response\Trip\GetTripResponse;
use App\Response\Trip\UpdateTripResponse;
use App\Response\TripStop\TripStopResponse;

class TripFactory
{
    private TripStopFactory $tripStopFactory;

    public function __construct(TripStopFactory $tripStopFactory)
    {
        $this->tripStopFactory = $tripStopFactory;
    }

    public function create(CreateTripRequest $request, User $user): Trip
    {
        return (new Trip())
            ->setName($request->getName())
            ->setDistance($request->getDistance())
            ->setDuration($request->getDuration())
            ->setUser($user)
            ->setTripDate($request->getTripDate());
    }

    public function update(UpdateTripsRequest $request, Trip $trip): Trip
    {
        return $trip
            ->setUpdatedAt(new \DateTime())
            ->setName($request->getName())
            ->setDistance($request->getDistance())
            ->setDuration($request->getDuration())
            ->setTripDate($request->getTripDate());
    }

    public function createResponse(Trip $trip): CreateTripResponse
    {
        return (new CreateTripResponse())
            ->setId($trip->getId())
            ->setName($trip->getName())
            ->setDistance($trip->getDistance())
            ->setDuration($trip->getDuration())
            ->setTripDate($trip->getTripDate())
            ->setTripStops($this->getTripStops($trip->getTripStops()))
            ;
    }

    public function updateResponse(Trip $trip): UpdateTripResponse
    {
        return (new UpdateTripResponse())
            ->setId($trip->getId())
            ->setName($trip->getName())
            ->setDistance($trip->getDistance())
            ->setDuration($trip->getDuration())
            ->setTripStops($this->getTripStops($trip->getTripStops()))
            ->setTripDate($trip->getTripDate());
    }

    public function getResponse(Trip $trip): GetTripResponse
    {
        return (new GetTripResponse())
            ->setId($trip->getId())
            ->setName($trip->getName())
            ->setDistance($trip->getDistance())
            ->setDuration($trip->getDuration())
            ->setTripStops($this->getTripStops($trip->getTripStops()))
            ->setTripDate($trip->getTripDate());
    }

    public function getListResponse(array $trips): array
    {
        $response = [];
        foreach ($trips as $trip) {
            $response[] = (new GetTripResponse())
                ->setId($trip->getId())
                ->setName($trip->getName())
                ->setDistance($trip->getDistance())
                ->setDuration($trip->getDuration())
                ->setTripStops($this->getTripStops($trip->getTripStops()))
                ->setTripDate($trip->getTripDate());
        }

        return $response;
    }

    private function getTripStops(array $tripStops): array
    {
        $tripStopsResponse = [];

        /** @var TripStop $tripStop */
        foreach ($tripStops as $tripStop) {
            if ($tripStop->getDeletedAt() === null) {
                $tripStopResponse = (new TripStopResponse())
                    ->setId($tripStop->getId())
                    ->setNote($tripStop->getNote())
                    ->setPlace($this->getPlaceResponse($tripStop->getPlace()));
                $tripStopsResponse [] = $tripStopResponse;
            }
        }

        return $tripStopsResponse;
    }

    private function getPlaceResponse(Place $place): PlaceResponse
    {
        return (new PlaceResponse())->setId($place->getId())
            ->setTags($place->getTags())
            ->setName($place->getName())
            ->setPlaceVisits([])
            ->setPhotos($this->getPhotos($place->getPlacePhotos()))
            ->setAddress($place->getAddress())
            ->setLongitude($place->getLongitude())
            ->setLatitude($place->getLatitude())
            ->setCountry(
                (new CountryResponse())->setName($place->getCountry()->getName())->setCode(
                    $place->getCountry()->getCode()
                )->setId($place->getCountry()->getId())
            )
            ->setDescription($place->getDescription())
            ->setId($place->getId());
    }

    private function getPhotos(array $photos): array
    {
        $response = [];
        /** @var PlacePhoto $photo */
        foreach ($photos as $photo) {
            if ($photo->getDeletedAt() === null){
                $object = (new PhotoResponse())
                    ->setId($photo->getId())
                    ->setUrl($photo->getUrl());
                $response[] = $object;
            }
        }

        return $response;
    }
}

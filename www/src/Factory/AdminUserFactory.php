<?php

namespace App\Factory;

use App\Entity\AdminUser;
use App\Entity\Role;
use App\Request\AdminUser\CreateAdminUserRequest;
use App\Request\AdminUser\UpdateAdminUserRequest;
use App\Response\AdminUser\CreateAdminUserResponse;
use App\Response\AdminUser\GetAdminUserResponse;
use App\Response\AdminUser\UpdateAdminUserResponse;
use App\Response\User\GetUserResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUserFactory
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function create(CreateAdminUserRequest $request, Role $role): AdminUser
    {
        $user = (new AdminUser())
            ->setEmail($request->getEmail())
            ->setFirstName($request->getFirstName())
            ->addRole($role)
            ->setLastName($request->getLastName());
        $user->setPassword($this->generatePassword($user, $request->getPassword()));

        return $user;
    }

    public function createResponse(AdminUser $user): CreateAdminUserResponse
    {
        return (new CreateAdminUserResponse())
            ->setId($user->getId())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setRoles($user->getRoles());
    }

    public function update(UpdateAdminUserRequest $request, AdminUser $user): AdminUser
    {
        $user
            ->setEmail($request->getEmail())
            ->setFirstName($request->getFirstName())
            ->setPassword($request->getPassword())
            ->setUpdatedAt(new \DateTime())
            ->setLastName($request->getLastName());

        return $user;
    }

    public function updateResponse(AdminUser $user): UpdateAdminUserResponse
    {
        return (new UpdateAdminUserResponse())
            ->setId($user->getId())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setRoles($user->getRoles());
    }

    public function getResponse(AdminUser $user): GetAdminUserResponse
    {
        return (new GetAdminUserResponse())
            ->setId($user->getId())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setRoles($user->getRoles());
    }

    public function getListResponse(array $users): array
    {
        $response = [];
        foreach ($users as $user) {
            $response[] = (new GetAdminUserResponse())
                ->setId($user->getId())
                ->setFirstName($user->getFirstName())
                ->setLastName($user->getLastName())
                ->setEmail($user->getEmail())
                ->setRoles($user->getRoles());
        }

        return $response;
    }

    private function generatePassword(AdminUser $user, string $password): string
    {
        return $this->encoder->encodePassword($user, $password);
    }
}

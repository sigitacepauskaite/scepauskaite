<?php

namespace App\Factory;

use App\Entity\Place;
use App\Entity\PlaceVisit;
use App\Entity\User;
use App\Enumerator\UserType;
use App\Request\PlaceVisit\AddPlaceVisitRequest;
use App\Request\PlaceVisit\RatePlaceVisitRequest;
use App\Response\PlaceVisit\AddPlaceVisitResponse;
use App\Response\PlaceVisit\RatePlaceVisitResponse;

class PlaceVisitFactory
{
    public function create(AddPlaceVisitRequest $addPlaceVisitRequest, Place $place, ?User $user): PlaceVisit
    {
        $placeVisit = (new PlaceVisit())
            ->setNote($addPlaceVisitRequest->getNote())
            ->setRate($addPlaceVisitRequest->getRate())
            ->setPlace($place);

        if ($user && in_array(UserType::ROLE_USER, $user->getRoles())) {
            $placeVisit->setUser($user);
        } else{
            $placeVisit->setUser(null);
        }

        return $placeVisit;
    }

    public function createResponse(PlaceVisit $placeVisit): AddPlaceVisitResponse
    {
        return (new AddPlaceVisitResponse())
            ->setId($placeVisit->getId())
            ->setRate($placeVisit->getRate())
            ->setNote($placeVisit->getNote())
            ->setUserId($placeVisit->getUser() ? $placeVisit->getUser()->getId() : null)
            ->setUserLastName($placeVisit->getUser() ? $placeVisit->getUser()->getLastName() : null)
            ->setUserFirstName($placeVisit->getUser() ? $placeVisit->getUser()->getFirstName() : null);
    }

    public function rateVisitPlace(RatePlaceVisitRequest $request, PlaceVisit $placeVisit, ?User $user): PlaceVisit
    {
        $placeVisit
            ->setUpdatedAt(new \DateTime())
            ->setRate($request->getRate())
            ->setNote($request->getNote());

        if ($user && in_array(UserType::ROLE_USER, $user->getRoles())) {
            $placeVisit->setUser($user);
        } else{
            $placeVisit->setUser(null);
        }

        return $placeVisit;
    }

    public function rateVisitPlaceResponse(PlaceVisit $placeVisit): RatePlaceVisitResponse
    {
        return (new RatePlaceVisitResponse())
            ->setId($placeVisit->getId())
            ->setRate($placeVisit->getRate())
            ->setNote($placeVisit->getNote())
            ->setUserFirstName($placeVisit->getUser() ? $placeVisit->getUser()->getFirstName(): null)
            ->setUserLastName($placeVisit->getUser() ? $placeVisit->getUser()->getLastName(): null)
            ->setUserId($placeVisit->getUser() ? $placeVisit->getUser()->getId() : null);
    }
}

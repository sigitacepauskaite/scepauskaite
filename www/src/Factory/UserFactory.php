<?php

namespace App\Factory;

use App\Entity\Country;
use App\Entity\User;
use App\Request\User\CreateUserRequest;
use App\Request\User\UpdateUserRequest;
use App\Response\User\CreateUserResponse;
use App\Response\User\GetUserResponse;
use App\Response\User\UpdateUserResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactory
{
    private UserPasswordEncoderInterface $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function create(CreateUserRequest $request, Country $country): User
    {
        $user = (new User())
            ->setFirstName($request->getFirstName())
            ->setLastName($request->getLastName())
            ->setEmail($request->getEmail())
            ->setCountry($country)
            ->setUsername($request->getEmail());

        $user->setPassword($this->generatePassword($user, $request->getPassword()));

        return $user;
    }

    public function update(UpdateUserRequest $request, User $user): User
    {
        $user = ($user)
            ->setUpdatedAt(new \DateTime())
            ->setFirstName($request->getFirstName())
            ->setLastName($request->getLastName())
            ->setUsername($request->getEmail())
            ->setEmail($request->getEmail());

        if ($request->getPassword() && strlen($request->getPassword()) > 0) {
            $user->setPassword($this->generatePassword($user, $request->getPassword()));
        }

        return $user;
    }

    public function createResponse(User $user): CreateUserResponse
    {
        return (new CreateUserResponse())
            ->setId($user->getId())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setRoles($user->getRoles());
    }

    public function updateResponse(User $user): UpdateUserResponse
    {
        return (new UpdateUserResponse())
            ->setId($user->getId())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setRoles($user->getRoles());
    }

    public function getResponse(User $user): GetUserResponse
    {
        return (new GetUserResponse())
            ->setId($user->getId())
            ->setFirstName($user->getFirstName())
            ->setLastName($user->getLastName())
            ->setEmail($user->getEmail())
            ->setRoles($user->getRoles());
    }

    public function getListResponse(array $users): array
    {
        $response = [];
        foreach ($users as $user) {
            $response[] = (new GetUserResponse())
                ->setId($user->getId())
                ->setFirstName($user->getFirstName())
                ->setLastName($user->getLastName())
                ->setEmail($user->getEmail())
                ->setRoles($user->getRoles());
        }

        return $response;
    }

    private function generatePassword(User $user, string $password): string
    {
        return $this->encoder->encodePassword($user, $password);
    }
}

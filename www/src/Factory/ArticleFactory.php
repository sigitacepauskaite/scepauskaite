<?php

namespace App\Factory;

use App\Entity\Article;
use App\Entity\ArticlePhoto;
use App\Entity\User;
use App\Request\Article\CreateArticleRequest;
use App\Request\Article\UpdateArticleRequest;
use App\Response\Article\CreateArticleResponse;
use App\Response\Article\GetArticleResponse;
use App\Response\Article\UpdateArticleResponse;
use App\Response\Photo\PhotoResponse;

class ArticleFactory
{
    public function create(CreateArticleRequest $request, ?User $user): Article
    {
        return (new Article())
            ->setUser($user)
            ->setContent($request->getContent())
            ->setTitle($request->getTitle());
    }

    public function update(UpdateArticleRequest $request, Article $article): Article
    {
        return $article
            ->setUpdatedAt(new \DateTime())
            ->setContent($request->getContent())
            ->setTitle($request->getTitle());
    }

    public function createResponse(Article $article): CreateArticleResponse
    {
        return (new CreateArticleResponse())
            ->setId($article->getId())
            ->setPhotos($this->getPhotos($article->getArticlePhotos()))
            ->setContent($article->getContent())
            ->setTitle($article->getTitle());
    }

    public function updateResponse(Article $article): UpdateArticleResponse
    {
        return (new UpdateArticleResponse())
            ->setId($article->getId())
            ->setPhotos($this->getPhotos($article->getArticlePhotos()))
            ->setContent($article->getContent())
            ->setTitle($article->getTitle());
    }

    public function getListResponse(array $articles): array
    {
        $response = [];
        foreach ($articles as $article) {
            $response[] = (new GetArticleResponse())
                ->setId($article->getId())
                ->setPhotos($this->getPhotos($article->getArticlePhotos()))
                ->setContent($article->getContent())
                ->setTitle($article->getTitle());
        }

        return $response;
    }

    public function getResponse(Article $article): GetArticleResponse
    {
        return (new GetArticleResponse())
            ->setId($article->getId())
            ->setContent($article->getContent())
            ->setPhotos($this->getPhotos($article->getArticlePhotos()))
            ->setTitle($article->getTitle());
    }

    private function getPhotos(array $photos): array
    {
        $response = [];
        /** @var ArticlePhoto $photo */
        foreach ($photos as $photo) {
            if ($photo->getDeletedAt() === null){
               $object = (new PhotoResponse())
                   ->setId($photo->getId())
                   ->setUrl($photo->getUrl());
                $response[] = $object;
            }
        }

        return $response;
    }
}

<?php

namespace App\Factory;

use App\Entity\Article;
use App\Entity\ArticlePhoto;

class ArticlePhotoFactory
{
    public function create(array $photoInfo, Article $article): ArticlePhoto
    {
        return (new ArticlePhoto())
            ->setName($photoInfo['fileName'])
            ->setCreatedAt(new \DateTime())
            ->setArticle($article)
            ->setUrl($photoInfo['filePath']);
    }
}

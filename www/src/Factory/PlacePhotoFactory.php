<?php

namespace App\Factory;

use App\Entity\Place;
use App\Entity\PlacePhoto;

class PlacePhotoFactory
{
    public function create(array $photoInfo, Place $place): PlacePhoto
    {
        return (new PlacePhoto())
            ->setPlace($place)
            ->setName($photoInfo['fileName'])
            ->setCreatedAt(new \DateTime())
            ->setUrl($photoInfo['filePath']);
    }
}

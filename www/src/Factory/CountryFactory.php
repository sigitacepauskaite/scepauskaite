<?php

namespace App\Factory;

use App\Entity\Country;
use App\Request\Article\CreateCountryRequest;
use App\Request\Article\UpdateCountryRequest;
use App\Response\Country\CreateCountryResponse;
use App\Response\Country\GetCountryResponse;
use App\Response\Country\UpdateCountryResponse;

class CountryFactory
{
    public function create(CreateCountryRequest $request): Country
    {
        return (new Country())
            ->setName($request->getName())
            ->setCode($request->getCode());
    }

    public function update(UpdateCountryRequest $request, Country $country): Country
    {
        return $country
            ->setUpdatedAt(new \DateTime())
            ->setCode($request->getCode())
            ->setName($request->getName());
    }

    public function createResponse(Country $country): CreateCountryResponse
    {
        return (new CreateCountryResponse())
            ->setId($country->getId())
            ->setName($country->getName())
            ->setCode($country->getCode());
    }

    public function updateResponse(Country $country): UpdateCountryResponse
    {
        return (new UpdateCountryResponse())
            ->setId($country->getId())
            ->setName($country->getName())
            ->setCode($country->getCode());
    }

    public function getListResponse(array $countries): array
    {
        $response = [];
        foreach ($countries as $country) {
            $response[] = (new GetCountryResponse())
                ->setId($country->getId())
                ->setName($country->getName())
                ->setCode($country->getCode());
        }

        return $response;
    }

    public function getResponse(Country $country): GetCountryResponse
    {
        return (new GetCountryResponse())
            ->setId($country->getId())
            ->setName($country->getName())
            ->setCode($country->getCode());
    }
}

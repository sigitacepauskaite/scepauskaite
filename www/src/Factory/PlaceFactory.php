<?php

namespace App\Factory;

use App\Entity\ArticlePhoto;
use App\Entity\Country;
use App\Entity\Place;
use App\Entity\PlacePhoto;
use App\Entity\PlaceVisit;
use App\Request\Place\CreatePlaceRequest;
use App\Request\Place\UpdatePlaceRequest;
use App\Response\Country\CountryResponse;
use App\Response\Photo\PhotoResponse;
use App\Response\Place\CreatePlaceResponse;
use App\Response\Place\GetPlaceResponse;
use App\Response\Place\UpdatePlaceResponse;
use App\Response\PlaceVisit\PlaceVisitResponse;

class PlaceFactory
{
    public function create(CreatePlaceRequest $request, Country $country): Place
    {
        return (new Place())
            ->setName($request->getName())
            ->setAddress($request->getAddress())
            ->setDescription($request->getDescription())
            ->setLatitude($request->getLatitude())
            ->setTags($request->getTags())
            ->setCountry($country)
            ->setLongitude($request->getLongitude());
    }

    public function update(UpdatePlaceRequest $request, Place $place): Place
    {
        return $place
            ->setUpdatedAt(new \DateTime())
            ->setName($request->getName())
            ->setAddress($request->getAddress())
            ->setLatitude($request->getLatitude())
            ->setTags($request->getTags())
            ->setDescription($request->getDescription())
            ->setLongitude($request->getLongitude());
    }

    public function createResponse(Place $place): CreatePlaceResponse
    {
        return (new CreatePlaceResponse())
            ->setId($place->getId())
            ->setAddress($place->getAddress())
            ->setPhotos($this->getPhotos($place->getPlacePhotos()))
            ->setPlaceVisits($this->getPlaceVisits($place->getPlaceVisits()))
            ->setDescription($place->getDescription())
            ->setName($place->getName())
            ->setTags($place->getTags())
            ->setStatus($place->getStatus())
            ->setCountry($this->getCountryResponse($place->getCountry()))
            ->setLatitude($place->getLatitude())
            ->setLongitude($place->getLongitude());
    }

    public function updateResponse(Place $place): UpdatePlaceResponse
    {
        return (new UpdatePlaceResponse())
            ->setAddress($place->getAddress())
            ->setName($place->getName())
            ->setLongitude($place->getLongitude())
            ->setLatitude($place->getLatitude())
            ->setPhotos($this->getPhotos($place->getPlacePhotos()))
            ->setPlaceVisits($this->getPlaceVisits($place->getPlaceVisits()))
            ->setCountry($this->getCountryResponse($place->getCountry()))
            ->setTags($place->getTags())
            ->setStatus($place->getStatus())
            ->setId($place->getId())
            ->setDescription($place->getDescription());
    }

    public function getResponse(Place $place): GetPlaceResponse
    {
        return (new GetPlaceResponse())
            ->setAddress($place->getAddress())
            ->setName($place->getName())
            ->setLongitude($place->getLongitude())
            ->setLatitude($place->getLatitude())
            ->setPhotos($this->getPhotos($place->getPlacePhotos()))
            ->setTags($place->getTags())
            ->setCountry($this->getCountryResponse($place->getCountry()))
            ->setStatus($place->getStatus())
            ->setPlaceVisits($this->getPlaceVisits($place->getPlaceVisits()))
            ->setId($place->getId())
            ->setDescription($place->getDescription());
    }

    public function getListResponse(array $places): array
    {
        $response = [];
        /** @var Place $place */
        foreach ($places as $place) {
            $response[] = (new GetPlaceResponse())
                ->setAddress($place->getAddress())
                ->setName($place->getName())
                ->setLongitude($place->getLongitude())
                ->setLatitude($place->getLatitude())
                ->setPhotos($this->getPhotos($place->getPlacePhotos()))
                ->setCountry($this->getCountryResponse($place->getCountry()))
                ->setStatus($place->getStatus())
                ->setPlaceVisits($this->getPlaceVisits($place->getPlaceVisits()))
                ->setTags($place->getTags())
                ->setId($place->getId())
                ->setDescription($place->getDescription());
        }

        return $response;
    }

    private function getPlaceVisits(array $placeVisits): array
    {
        $placeVisitsResponse = [];
        /** @var PlaceVisit $placeVisit */
        foreach ($placeVisits as $placeVisit) {
            $placeVisitResponse = (new PlaceVisitResponse())
                ->setId($placeVisit->getId())
                ->setNote($placeVisit->getNote())
                ->setRate($placeVisit->getRate())
                ->setUserId($placeVisit->getUser() ? $placeVisit->getUser()->getId() : null)
                ->setUserFirstName($placeVisit->getUser() ? $placeVisit->getUser()->getFirstName() : null)
                ->setUserLastName($placeVisit->getUser() ? $placeVisit->getUser()->getLastName() : null);

            $placeVisitsResponse [] = $placeVisitResponse;
        }

        return $placeVisitsResponse;
    }

    private function getCountryResponse(Country $country): CountryResponse
    {
        return (new CountryResponse())->setName($country->getName())
            ->setId($country->getId())
            ->setCode($country->getCode());
    }

    private function getPhotos(array $photos): array
    {
        $response = [];
        /** @var PlacePhoto $photo */
        foreach ($photos as $photo) {
            if ($photo->getDeletedAt() === null){
                $object = (new PhotoResponse())
                    ->setId($photo->getId())
                    ->setUrl($photo->getUrl());
                $response[] = $object;
            }
        }

        return $response;
    }
}

<?php

namespace App\Factory;

use App\Entity\Place;
use App\Entity\Trip;
use App\Entity\TripStop;
use App\Request\Trip\AddTripStopRequest;
use App\Response\Country\CountryResponse;
use App\Response\Place\PlaceResponse;
use App\Response\TripStop\AddTripStopResponse;

class TripStopFactory
{
    public function create(AddTripStopRequest $addTripStopRequest, Trip $trip, Place $place): TripStop
    {
        return (new TripStop())
            ->setNote($addTripStopRequest->getNote())
            ->setPlace($place)
            ->setTrip($trip);
    }

    public function createResponse(TripStop $tripStop): AddTripStopResponse
    {
        return (new AddTripStopResponse())
            ->setId($tripStop->getId())
            ->setPlace($this->getPlaceResponse($tripStop->getPlace()))
            ->setNote($tripStop->getNote());
    }

    private function getPlaceResponse(Place $place): PlaceResponse
    {
        return (new PlaceResponse())->setId($place->getId())
            ->setTags($place->getTags())
            ->setName($place->getName())
            ->setPlaceVisits([])
            ->setDescription($place->getDescription())
            ->setPhotos($place->getPlacePhotos())
            ->setAddress($place->getAddress())
            ->setStatus($place->getStatus())
            ->setLongitude($place->getLongitude())
            ->setLatitude($place->getLatitude())
            ->setCountry(
                (new CountryResponse())->setName($place->getCountry()->getName())->setCode(
                    $place->getCountry()->getCode()
                )->setId($place->getCountry()->getId())
            )
            ->setId($place->getId());
    }
}

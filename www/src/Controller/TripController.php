<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;
use App\Manager\TripManager;
use App\Request\Trip\AddTripStopRequest;
use App\Request\Trip\CreateTripRequest;
use App\Request\Trip\GetTripsRequest;
use App\Request\Trip\UpdateTripsRequest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class TripController extends AbstractController
{
    private TripManager $manager;

    private SerializerInterface $serializer;

    public function __construct(TripManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/trips/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getTripAction(int $id): View
    {
        return View::create($this->manager->getTrip($id, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/trips", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getTripsAction(Request $request): View
    {
        $filtersJson = json_encode($request->query->all());
        $getTripsRequest = $this->serializer->deserialize($filtersJson, GetTripsRequest::class, 'json');

        return View::create($this->manager->getTrips($getTripsRequest, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/trips", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createTripRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createTripAction(CreateTripRequest $createTripRequest): View
    {
        return View::create($this->manager->createTrip($createTripRequest, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/trips/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updateTripsRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updateTripAction(UpdateTripsRequest $updateTripsRequest, int $id): View
    {
        return View::create($this->manager->updateTrip($updateTripsRequest, $id, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/trips/{id}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deleteTripAction(int $id): View
    {
        $this->manager->deleteTrip($id, $this->getLoggedInUser());

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/trips/{id}/places/{placeId}/tripStops", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "addTripStopRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function addTripStopAction(AddTripStopRequest $addTripStopRequest, int $id, int $placeId): View
    {
        return View::create($this->manager->addTripStop($addTripStopRequest, $id, $placeId, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/trips/tripStops/{stopId}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function removeTripStopAction(int $stopId): View
    {
        $this->manager->removeTripStop($stopId, $this->getLoggedInUser());

        return View::create([], Response::HTTP_OK);
    }

    private function getLoggedInUser(): User
    {
        /** @var User|null $user */
        $user = $this->getUser();

        if (!$user) {
            throw new BadRequestException('User was not found', ExceptionCode::USER_NOT_FOUND_CODE);
        }

        return $user;
    }
}

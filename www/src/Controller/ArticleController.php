<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;
use App\Manager\ArticleManager;
use App\Request\Article\CreateArticleRequest;
use App\Request\Article\GetArticlesRequest;
use App\Request\Article\UpdateArticleRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class ArticleController extends AbstractController
{
    private ArticleManager $manager;

    private SerializerInterface $serializer;

    public function __construct(ArticleManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/public/articles/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getArticleAction(int $id): View
    {
        return View::create($this->manager->getArticle($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/articles", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getArticlesAction(Request $request): View
    {
        $filtersJson = json_encode($request->query->all());
        $getArticlesRequest = $this->serializer->deserialize($filtersJson, GetArticlesRequest::class, 'json');

        return View::create($this->manager->getArticles($getArticlesRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/articles", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createArticleRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createArticleAction(CreateArticleRequest $createArticleRequest): View
    {
        return View::create($this->manager->createArticle($createArticleRequest, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/articles/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updateArticleRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updateArticleAction(UpdateArticleRequest $updateArticleRequest, int $id): View
    {
        return View::create($this->manager->updateArticle($updateArticleRequest, $id, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/articles/{id}/photos", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function uploadPhotosAction(Request $request, int $id): View
    {
        $this->manager->uploadArticlePhotos($request, $id, $this->getLoggedInUser());

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/articles/{id}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deleteArticleAction(int $id): View
    {
        $this->manager->deleteArticle($id, $this->getLoggedInUser());

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/articles/{id}/photos/{photoId}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deletePhotoAction(int $id, int $photoId): View
    {
        $this->manager->deletePhoto($id, $photoId, $this->getLoggedInUser());

        return View::create([], Response::HTTP_OK);
    }

    private function getLoggedInUser(): User
    {
        /** @var User|null $user */
        $user = $this->getUser();

        if (!$user) {
            throw new BadRequestException('User was not found', ExceptionCode::USER_NOT_FOUND_CODE);
        }

        return $user;
    }
}

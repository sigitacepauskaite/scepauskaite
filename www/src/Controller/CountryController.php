<?php

namespace App\Controller;

use App\Manager\CountryManager;
use App\Request\Article\GetCountriesRequest;
use App\Request\Place\GetPlacesRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class CountryController extends AbstractController
{
    private CountryManager $manager;

    private SerializerInterface $serializer;

    public function __construct(CountryManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/public/countries/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getCountryAction(int $id): View
    {
        return View::create($this->manager->getCountry($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/countries", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getCountriesAction(Request $request): View
    {
        $getCountriesRequest = $this->serializer->deserialize($request->getContent(), GetCountriesRequest::class, 'json');

        return View::create($this->manager->getCountries($getCountriesRequest), Response::HTTP_OK);
    }
}

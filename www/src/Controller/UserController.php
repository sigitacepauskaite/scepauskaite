<?php

namespace App\Controller;

use App\Entity\User;
use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;
use App\Manager\UserManager;
use App\Request\User\CreateUserRequest;
use App\Request\User\GetUsersRequest;
use App\Request\User\UpdateUserRequest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

class UserController extends AbstractController
{
    private UserManager $manager;

    private SerializerInterface $serializer;

    public function __construct(UserManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/users/current", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getUserByTokenAction(): View
    {
        $id = $this->getLoggedInUser()->getId();

        return View::create($this->manager->getUser($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/users/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getUserAction(int $id): View
    {
        return View::create($this->manager->getUser($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/users", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getUsersAction(Request $request): View
    {
        $getUsersRequest = $this->serializer->deserialize($request->getContent(), GetUsersRequest::class, 'json');

        return View::create($this->manager->getUsers($getUsersRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/users/countries/{countryCode}", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createUserRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createUserAction(CreateUserRequest $createUserRequest, string $countryCode): View
    {
        $user = $this->manager->createUser($createUserRequest, $countryCode);

        return View::create($user, Response::HTTP_OK);
    }

    /**
     * @Route("/api/users/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updateUserRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updateUserAction(UpdateUserRequest $updateUserRequest, int $id): View
    {
        return View::create($this->manager->updateUser($updateUserRequest, $id, $this->getLoggedInUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/users/{id}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deleteUserAction(int $id): View
    {
        $this->manager->deleteUser($id, $this->getLoggedInUser());

        return View::create([], Response::HTTP_OK);
    }

    private function getLoggedInUser(): UserInterface
    {
        /** @var User|null $user */
        $user = $this->getUser();

        if (!$user) {
            throw new BadRequestException('User was not found', ExceptionCode::USER_NOT_FOUND_CODE);
        }

        return $user;
    }
}

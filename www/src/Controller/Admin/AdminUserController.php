<?php

namespace App\Controller\Admin;

use App\Entity\AdminUser;
use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;
use App\Manager\AdminUserManager;
use App\Request\AdminUser\CreateAdminUserRequest;
use App\Request\AdminUser\GetAdminUsersRequest;
use App\Request\AdminUser\UpdateAdminUserRequest;
use App\Request\User\GetUsersRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

class AdminUserController extends AbstractController
{
    private AdminUserManager $manager;

    private SerializerInterface $serializer;

    public function __construct(AdminUserManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/admin/admin-users/current", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getUserByTokenAction(): View
    {
        $id = $this->getLoggedInUser()->getId();

        return View::create($this->manager->getAdminUser($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/admin-users/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getAdminUserAction(int $id): View
    {
        return View::create($this->manager->getAdminUser($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/admin-users", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getAdminUsers(Request $request): View
    {
        $getAdminUsersRequest = $this->serializer->deserialize($request->getContent(), GetAdminUsersRequest::class, 'json');

        return View::create($this->manager->getAdminUsers($getAdminUsersRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/admin-users", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createAdminUserRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createAdminUserAction(CreateAdminUserRequest $createAdminUserRequest): View
    {
        return View::create($this->manager->createAdminUser($createAdminUserRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/admin-users/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updateAdminUserRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updateAdminUserAction(UpdateAdminUserRequest $updateAdminUserRequest, int $id): View
    {
        return View::create($this->manager->updateAdminUser($updateAdminUserRequest, $id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/admin-users/{id}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deleteAdminUserAction(int $id): View
    {
        $this->manager->deleteAdminUser($id);

        return View::create([], Response::HTTP_OK);
    }

    private function getLoggedInUser(): UserInterface
    {
        /** @var AdminUser|null $user */
        $user = $this->getUser();

        if (!$user) {
            throw new BadRequestException('Administratorius su tokiu prisijungimo vardu nerastas', ExceptionCode::USER_NOT_FOUND_CODE);
        }

        return $user;
    }
}

<?php

namespace App\Controller\Admin;

use App\Manager\CountryManager;
use App\Request\Article\CreateCountryRequest;
use App\Request\Article\GetCountriesRequest;
use App\Request\Article\UpdateCountryRequest;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class CountryController extends AbstractController
{
    private CountryManager $manager;

    private SerializerInterface $serializer;

    public function __construct(CountryManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/admin/countries/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getCountryAction(int $id): View
    {
        return View::create($this->manager->getCountry($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/countries", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getCountriesAction(Request $request): View
    {
        $filtersJson = json_encode($request->query->all());
        $getCountriesRequest = $this->serializer->deserialize($filtersJson, GetCountriesRequest::class, 'json');

        return View::create($this->manager->getCountries($getCountriesRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/countries", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createCountryRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createCountryAction(CreateCountryRequest $createCountryRequest): View
    {
        return View::create($this->manager->createCountry($createCountryRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/countries/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updateCountryRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updateCountryAction(UpdateCountryRequest $updateCountryRequest, int $id): View
    {
        return View::create($this->manager->updateCountry($updateCountryRequest, $id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/countries/{id}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deleteCountryAction(int $id): View
    {
        $this->manager->deleteCountry($id);

        return View::create([], Response::HTTP_OK);
    }
}

<?php

namespace App\Controller\Admin;

use App\Manager\PlaceManager;
use App\Request\Place\CreatePlaceRequest;
use App\Request\Place\GetPlacesRequest;
use App\Request\Place\UpdatePlaceRequest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class PlaceController extends AbstractController
{
    private PlaceManager $manager;

    private SerializerInterface $serializer;

    public function __construct(PlaceManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("api/admin/places/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getPlaceAction(int $id): View
    {
        return View::create($this->manager->getPlace($id), Response::HTTP_OK);
    }

    /**
     * @Route("api/admin/places", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getPlacesAction(Request $request): View
    {
        $filtersJson = json_encode($request->query->all());
        $getPlacesRequest = $this->serializer->deserialize($filtersJson, GetPlacesRequest::class, 'json');

        return View::create($this->manager->getPlaces($getPlacesRequest), Response::HTTP_OK);
    }

    /**
     * @Route("api/admin/places/countries/{countryCode}", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createPlaceRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createArticleAction(CreatePlaceRequest $createPlaceRequest, string $countryCode): View
    {
        return View::create($this->manager->createPlace($createPlaceRequest, $countryCode), Response::HTTP_OK);
    }

    /**
     * @Route("api/admin/places/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updatePlaceRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updatePlaceAction(UpdatePlaceRequest $updatePlaceRequest, int $id): View
    {
        return View::create($this->manager->updatePlace($updatePlaceRequest, $id), Response::HTTP_OK);
    }

    /**
     * @Route("api/admin/places/{id}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deletePlaceAction(int $id): View
    {
        $this->manager->deletePlace($id);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/places/{id}/photos", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function uploadPhotosAction(Request $request, int $id): View
    {
        $this->manager->uploadPhotos($request, $id);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/places/{id}/photos/{photoId}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deletePhotoAction(int $id, int $photoId): View
    {
        $this->manager->deletePhoto($id, $photoId);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/places/{id}/confirmation", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function confirmPlaceAction(int $id): View
    {
        $this->manager->confirmPlace($id);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/places/{id}/cancellation", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function cancelPlaceAction(int $id): View
    {
        $this->manager->cancelPlace($id);

        return View::create([], Response::HTTP_OK);
    }
}

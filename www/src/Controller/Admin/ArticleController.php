<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Manager\ArticleManager;
use App\Request\Article\CreateArticleRequest;
use App\Request\Article\GetArticlesRequest;
use App\Request\Article\UpdateArticleRequest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class ArticleController extends AbstractController
{
    private ArticleManager $manager;

    private SerializerInterface $serializer;

    public function __construct(ArticleManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/admin/articles/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getArticleAction(int $id): View
    {
        return View::create($this->manager->getArticle($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/articles", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getArticlesAction(Request $request): View
    {
        $filtersJson = json_encode($request->query->all());
        $getArticlesRequest = $this->serializer->deserialize($filtersJson, GetArticlesRequest::class, 'json');

        return View::create($this->manager->getArticles($getArticlesRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/articles", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createArticleRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createArticleAction(CreateArticleRequest $createArticleRequest): View
    {
        return View::create($this->manager->createArticle($createArticleRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/articles/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updateArticleRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updateArticleAction(UpdateArticleRequest $updateArticleRequest, int $id): View
    {
        return View::create($this->manager->updateArticle($updateArticleRequest, $id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/articles/{id}/users/{user}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deleteArticleAction(int $id): View
    {
        $this->manager->deleteArticle($id);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/articles/{id}/photos", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function uploadPhotosAction(Request $request, int $id): View
    {
        $this->manager->uploadArticlePhotos($request, $id);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/admin/articles/{id}/photos/{photoId}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deletePhotoAction(int $id, int $photoId): View
    {
        $this->manager->deletePhoto($id, $photoId);

        return View::create([], Response::HTTP_OK);
    }
}

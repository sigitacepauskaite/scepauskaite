<?php

namespace App\Controller;

use App\Manager\PlaceManager;
use App\Request\PlaceVisit\AddPlaceVisitRequest;
use App\Request\Place\CreatePlaceRequest;
use App\Request\Place\GetPlacesRequest;
use App\Request\PlaceVisit\RatePlaceVisitRequest;
use App\Request\Place\UpdatePlaceRequest;
use FOS\RestBundle\View\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;

class PlaceController extends AbstractController
{
    private PlaceManager $manager;

    private SerializerInterface $serializer;

    public function __construct(PlaceManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/public/places/{id}", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getPlaceAction(int $id): View
    {
        return View::create($this->manager->getPlace($id), Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/places", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getPlacesAction(Request $request): View
    {
        $queryParams = $request->query->all();
        $filtersContent = json_encode($queryParams);
        $getPlacesRequest = $this->serializer->deserialize($filtersContent, GetPlacesRequest::class, 'json');

        return View::create($this->manager->getPlaces($getPlacesRequest), Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/places/countries/{countryCode}", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "createPlaceRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function createPlaceAction(CreatePlaceRequest $createPlaceRequest, string $countryCode): View
    {
        return View::create($this->manager->createPlace($createPlaceRequest, $countryCode), Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/places/{id}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "updatePlaceRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function updatePlaceAction(UpdatePlaceRequest $updatePlaceRequest, int $id): View
    {
        return View::create($this->manager->updatePlace($updatePlaceRequest, $id), Response::HTTP_OK);
    }


    /**
     * @Route("/api/public/places/{id}/photos", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function uploadPhotosAction(Request $request, int $id): View
    {
//        var_dump($request->files->all());die;
        $this->manager->uploadPhotos($request, $id);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/places/{id}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deletePlaceAction(int $id): View
    {
        $this->manager->deletePlace($id);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/places/{id}/photos/{photoId}", methods={"DELETE"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function deletePhotoAction(int $id, int $photoId): View
    {
        $this->manager->deletePhoto($id, $photoId);

        return View::create([], Response::HTTP_OK);
    }

    /**
     * @Route("/api/places/visited", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getUserPlaceVisitsAction(): View
    {
        $user = $this->getUser();

        return View::create($this->manager->getVisitedPlaces($user), Response::HTTP_OK);
    }

    /**
     * @Route("/api/places/{id}/visits", methods={"POST"})
     * @Rest\View(serializerGroups={"Default"})
     * @ParamConverter(
     *     "addPlaceVisitRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function addPlaceVisitAction(Request $request, AddPlaceVisitRequest $addPlaceVisitRequest, int $id): View
    {
        return View::create($this->manager->addPlaceVisit($addPlaceVisitRequest, $id, $this->getUser()), Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/places/{id}/visits/{placeVisitId}", methods={"PUT"})
     * @Rest\View(serializerGroups={"Default", "Rate"})
     * @ParamConverter(
     *     "ratePlaceVisitRequest",
     *     converter="fos_rest.request_body"
     * )
     */
    public function ratePlaceVisitAction(RatePlaceVisitRequest $ratePlaceVisitRequest, int $id, int $placeVisitId): View
    {
        return View::create($this->manager->ratePlaceVisit($ratePlaceVisitRequest, $id, $placeVisitId, $this->getUser()), Response::HTTP_OK);
    }
}

<?php

namespace App\Controller;

use App\Manager\UserManager;
use FOS\RestBundle\View\View;
use SKAgarwal\GoogleApi\PlacesApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class GoogleController extends AbstractController
{
    private UserManager $manager;

    private SerializerInterface $serializer;

    public function __construct(UserManager $manager, SerializerInterface $serializer)
    {
        $this->manager = $manager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/public/google/places-autocomplete", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getPlaces(Request $request): View
    {
        $term = $request->query->get('term');
        $googlePlaces = new PlacesApi('AIzaSyCNoChP44UlZB1vd-x4iD8a1FHhzfaSxu4');
        $results = $googlePlaces->placeAutocomplete($term)->toArray();

        $responseResults = [];
        foreach ($results['predictions'] as $result) {
            $responseResults[] = [
                'name' => $result['description'],
                'placeId' => $result['place_id'],
            ];
        }

        return View::create($responseResults, Response::HTTP_OK);
    }

    /**
     * @Route("/api/public/google/places-details", methods={"GET"})
     * @Rest\View(serializerGroups={"Default"})
     */
    public function getPlaceDetails(Request $request): View
    {
        $placeId = $request->query->get('placeId');
        $googlePlaces = new PlacesApi('AIzaSyCNoChP44UlZB1vd-x4iD8a1FHhzfaSxu4');
        $result = $googlePlaces->placeDetails($placeId)->toArray();
        $result = $result['result'];

        $responseResults = [
            'name' => $result['formatted_address'],
            'icon' => $result['icon'],
            'placeId' => $result['place_id'],
            'latitude' => $result['geometry']['location']['lat'],
            'longitude' => $result['geometry']['location']['lng'],
        ];

        return View::create($responseResults, Response::HTTP_OK);
    }
}

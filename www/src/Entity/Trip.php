<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @Entity
 */
class Trip
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $duration;

    /**
     * @ORM\Column(type="string")
     */
    private string $distance;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $tripDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \Datetime $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $updatedAt;

    /**
     * @ManyToOne(targetEntity="User", inversedBy="trips")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     */
    private User $user;

    /**
     * @OneToMany(targetEntity="TripStop", mappedBy="trip")
     */
    private Collection $tripStops;

    public function __construct()
    {
        $this->tripStops = new ArrayCollection();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDuration(): string
    {
        return $this->duration;
    }

    public function setDuration(string $duration): self
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDistance(): string
    {
        return $this->distance;
    }

    public function setDistance(string $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getTripDate(): \DateTime
    {
        return $this->tripDate;
    }

    public function setTripDate(\DateTime $tripDate): self
    {
        $this->tripDate = $tripDate;

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getTripStopsCollection(): Collection
    {
        return $this->tripStops;
    }

    public function getTripStops(): array
    {
        return $this->getTripStopsCollection()->toArray();
    }

    public function setTripStops(array $tripStops): self
    {
        $this->tripStops = new ArrayCollection($tripStops);

        return $this;
    }

    public function addTripStop(TripStop $tripStop): self
    {
        if (!$this->tripStops->contains($tripStop)) {
            $this->tripStops[] = $tripStop;
        }

        return $this;
    }
}

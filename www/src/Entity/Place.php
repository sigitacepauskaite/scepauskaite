<?php

namespace App\Entity;

use App\Enumerator\PlaceStatusType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * @Entity
 */
class Place
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $address;

    /**
     * @ORM\Column(type="float")
     */
    private string $latitude;

    /**
     * @ORM\Column(type="float")
     */
    private string $longitude;

    /**
     * @ORM\Column(type="string", length=1024)
     */
    private string $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \Datetime $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $updatedAt;

    /**
     * @OneToMany(targetEntity="PlacePhoto", mappedBy="place")
     */
    private Collection $placePhotos;

    /**
     * @OneToMany(targetEntity="PlaceVisit", mappedBy="place")
     */
    private Collection $placeVisits;

    /**
     * @ORM\Column(type="array")
     */
    private array $tags;

    /**
     * @OneToMany(targetEntity="TripStop", mappedBy="place")
     */
    private Collection $tripsStops;

    /**
     * @ManyToOne(targetEntity="Country")
     * @JoinColumn(name="country_id", referencedColumnName="id")
     */
    private Country $country;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $status;

    public function __construct()
    {
        $this->placePhotos = new ArrayCollection();
        $this->placeVisits = new ArrayCollection();
        $this->tripsStops = new ArrayCollection();
        $this->tags = [];
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->status = PlaceStatusType::CREATED;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPlacePhotos(): array
    {
        return $this->getPlacePhotosCollection()->toArray();
    }

    public function getPlacePhotosCollection(): Collection
    {
        return $this->placePhotos;
    }

    public function setPlacePhotos(array $placePhotos): self
    {
        $this->placePhotos = new ArrayCollection($placePhotos);

        return $this;
    }

    public function addPlacePhoto(PlacePhoto $placePhoto): self
    {
        if (!$this->placePhotos->contains($placePhoto)) {
            $this->placePhotos[] = $placePhoto;
        }

        return $this;
    }

    public function getPlaceVisitsCollection(): Collection
    {
        return $this->placeVisits;
    }

    public function getPlaceVisits(): array
    {
        return $this->getPlaceVisitsCollection()->toArray();
    }

    public function setPlaceVisits(array $placeVisits): self
    {
        $this->placeVisits = new ArrayCollection($placeVisits);

        return $this;
    }

    public function addPlaceVisits(PlaceVisit $placeVisit): self
    {
        if (!$this->placeVisits->contains($placeVisit)) {
            $this->placeVisits[] = $placeVisit;
        }

        return $this;
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    public function getTripsStopsCollection(): Collection
    {
        return $this->tripsStops;
    }

    public function getTripsStops(): array
    {
        $tripsStops = [];
        /** @var TripStop $tripStop */
        foreach ($this->getTripsStopsCollection() as $tripStop) {
            $tripsStops[] = $tripStop->getId();
        }

        return $tripsStops;
    }

    public function setTripsStops(array $tripsStops): self
    {
        $this->tripsStops = new ArrayCollection($tripsStops);

        return $this;
    }

    public function addTripStop(TripStop $tripStop): self
    {
        if (!$this->tripsStops->contains($tripStop)) {
            $this->tripsStops[] = $tripStop;
        }

        return $this;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): Place
    {
        $this->country = $country;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): Place
    {
        $this->status = $status;

        return $this;
    }
}


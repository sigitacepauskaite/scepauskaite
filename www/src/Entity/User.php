<?php

namespace App\Entity;

use App\Enumerator\UserType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;
use Doctrine\ORM\Mapping\OneToMany;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Entity
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $email;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $password;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $firstName;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private string $lastName;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private string $salt;

    /**
     * @var array
     */
    private array $roles = [UserType::ROLE_USER];

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTime $deletedAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \Datetime $createdAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private \DateTime $updatedAt;

    /**
     * @ORM\Column(type="string", length=35, unique=true)
     */
    private string $username;

    /**
     * @OneToMany(targetEntity="PlaceVisit", mappedBy="user")
     */
    private Collection $placeVisits;

    /**
     * @OneToMany(targetEntity="Trip", mappedBy="user")
     */
    private Collection $trips;

    /**
     * @ManyToOne(targetEntity="Country", inversedBy="users")
     * @JoinColumn(name="country_id", referencedColumnName="id")
     */
    private Country $country;

    public function __construct()
    {
        $this->salt = md5(uniqid('', true));
        $this->createdAt = new \DateTime();
        $this->placeVisits = new ArrayCollection();
        $this->trips = new ArrayCollection();
        $this->updatedAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): User
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): User
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): User
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $username): User
    {
        $this->username = $username;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function getSalt(): string
    {
        return $this->salt;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function eraseCredentials(): void
    {
    }

    public function getDeletedAt(): ?\DateTime
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTime $deletedAt): User
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTime $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getPlaceVisitsCollection(): Collection
    {
        return $this->placeVisits;
    }

    public function getPlaceVisits(): array
    {
        $placeVisits = [];
        /** @var PlaceVisit $placeVisit */
        foreach ($this->getPlaceVisitsCollection() as $placeVisit) {
            $placeVisits[] = $placeVisit->getId();
        }

        return $placeVisits;
    }

    public function getVisitedPlaces(): array
    {
        $visitedPlaces = [];
        /** @var PlaceVisit $placeVisit */
        foreach ($this->getPlaceVisitsCollection() as $placeVisit) {
            $visitedPlaces[] = $placeVisit->getPlace();
        }

        return $visitedPlaces;
    }

    public function setPlaceVisits(array $placeVisits): self
    {
        $this->placeVisits = new ArrayCollection($placeVisits);

        return $this;
    }

    public function addPlaceVisits(PlaceVisit $placeVisit): self
    {
        if (!$this->placeVisits->contains($placeVisit)) {
            $this->placeVisits[] = $placeVisit;
        }

        return $this;
    }

    public function getTripsCollection(): Collection
    {
        return $this->placeVisits;
    }

    public function getTrips(): array
    {
        $trips = [];
        /** @var Trip $trip */
        foreach ($this->getTripsCollection() as $trip) {
            $trips[] = $trip->getId();
        }

        return $trips;
    }

    public function setTrips(array $trips): self
    {
        $this->trips = new ArrayCollection($trips);

        return $this;
    }

    public function addTrip(Trip $trip): self
    {
        if (!$this->trips->contains($trip)) {
            $this->$trip[] = $trip;
        }

        return $this;
    }

    public function getCountry(): Country
    {
        return $this->country;
    }

    public function setCountry(Country $country): User
    {
        $this->country = $country;

        return $this;
    }
}

<?php

namespace App\Validator;

use App\Entity\ArticlePhoto;
use App\Entity\PlacePhoto;
use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;

class PlacePhotoRequestValidator
{
    public function validateUploadRequest(PlacePhoto $photo): void
    {
        if ($photo->getDeletedAt() !== null) {
            throw new BadRequestException('Place image is already deleted', ExceptionCode::PLACE_IMAGE_DELETED_CODE);
        }
    }
}

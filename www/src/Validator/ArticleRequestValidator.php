<?php

namespace App\Validator;

use App\Request\Article\CreateArticleRequest;
use App\Request\Article\GetArticlesRequest;
use App\Request\Article\UpdateArticleRequest;
use App\Request\User\GetUsersRequest;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ArticleRequestValidator extends AbstractValidator
{
    public function validateCreateRequest(CreateArticleRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateUpdateRequest(UpdateArticleRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateListRequest(GetArticlesRequest $request): void
    {
        $this->validateRequestObject($request);
    }
}

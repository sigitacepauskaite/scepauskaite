<?php

namespace App\Validator;

use App\Entity\ArticlePhoto;
use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;

class ArticlePhotoRequestValidator extends AbstractValidator
{
    public function validateDeleteRequest(ArticlePhoto $photo): void
    {
        if ($photo->getDeletedAt() !== null) {
            throw new BadRequestException('Article image is already deleted', ExceptionCode::ARTICLE_IMAGE_DELETED_CODE);
        }
    }

}

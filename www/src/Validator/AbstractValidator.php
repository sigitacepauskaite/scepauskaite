<?php

namespace App\Validator;

use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;
use App\Request\RequestInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractValidator
{
    private ValidatorInterface $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function validateRequestObject(RequestInterface $request, bool $throwException = true): ConstraintViolationListInterface
    {
        $errors = $this->validator->validate($request);
        if ($throwException && count($errors) > 0) {
            throw new BadRequestException('Nurodyti netinkami duomenys', ExceptionCode::REQUEST_INVALID_DATA_CODE);
        }

        return $errors;
    }
}

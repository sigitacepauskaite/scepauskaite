<?php

namespace App\Validator;

use App\Enumerator\UserType;
use App\Exception\BadRequestException;
use App\Exception\ExceptionCode;
use App\Exception\NotUniqueException;
use App\Repository\UserRepository;
use App\Request\User\CreateUserRequest;
use App\Request\User\GetUsersRequest;
use App\Request\User\UpdateUserRequest;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserRequestValidator extends AbstractValidator
{
    private UserRepository $userRepository;

    public function __construct(ValidatorInterface $validator, UserRepository $userRepository)
    {
        parent::__construct($validator);

        $this->userRepository = $userRepository;
    }

    public function validateListRequest(GetUsersRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateCreateRequest(CreateUserRequest $request): void
    {
        $this->validateRequestObject($request);
        $this->validateUniqueEmailOnCreate($request->getEmail());
    }

    public function validateUpdateRequest(UpdateUserRequest $request, int $id): void
    {
        $this->validateRequestObject($request);
        $this->validateUniqueEmailOnUpdate($id, $request->getEmail());
    }

    private function validateUniqueEmailOnCreate(string $email): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if ($user) {
            throw new NotUniqueException('Vartotojas su tokiu el. pašto adresu jau egzistuoja', ExceptionCode::USER_EMAIL_NOT_UNIQUE_CODE);
        }
    }

    private function validateUniqueEmailOnUpdate(int $id, string $email): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if ($user && $id !== $user->getId()) {
            throw new NotUniqueException('Provided email is not unique', ExceptionCode::USER_EMAIL_NOT_UNIQUE_CODE);
        }
    }
}

<?php

namespace App\Validator;

use App\Entity\Place;
use App\Exception\ExceptionCode;
use App\Exception\NotUniqueException;
use App\Repository\PlaceRepository;
use App\Request\Place\CreatePlaceRequest;
use App\Request\Place\GetPlacesRequest;
use App\Request\Place\UpdatePlaceRequest;
use App\Request\PlaceVisit\AddPlaceVisitRequest;
use App\Request\PlaceVisit\RatePlaceVisitRequest;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class PlaceRequestValidator extends AbstractValidator
{
    private PlaceRepository $placeRepository;

    public function __construct(ValidatorInterface $validator, PlaceRepository  $placeRepository)
    {
        $this->placeRepository = $placeRepository;

        parent::__construct($validator);
    }

    public function validateCreateRequest(CreatePlaceRequest $request): void
    {
        $this->validatePlaceExistOnCreate($request);
        $this->validateRequestObject($request);
    }

    public function validateUpdateRequest(UpdatePlaceRequest $request, int $id): void
    {
        $this->validatePlaceExistOnUpdate($request, $id);
        $this->validateRequestObject($request);
    }

    public function validateListRequest(GetPlacesRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateAddPlaceVisitRequest(AddPlaceVisitRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateRatePlaceRequest(RatePlaceVisitRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    private function validatePlaceExistOnCreate(CreatePlaceRequest $request): void
    {
        $place = $this->placeRepository->findOneBy(
            ['latitude' => $request->getLatitude(), 'longitude' => $request->getLongitude()]
        );

        if ($place) {
            throw new NotUniqueException('Place exist', ExceptionCode::PLACE_EXIST_CODE);
        }
    }

    private function validatePlaceExistOnUpdate(UpdatePlaceRequest $request, int $id): void
    {
        $place = $this->placeRepository->findOneBy(
            ['latitude' => $request->getLatitude(), 'longitude' => $request->getLongitude()]
        );

        if ($place && $place->getId() !== $id) {
            throw new NotUniqueException('Place exist', ExceptionCode::PLACE_EXIST_CODE);
        }
    }
}

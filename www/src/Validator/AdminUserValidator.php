<?php

namespace App\Validator;

use App\Exception\ExceptionCode;
use App\Exception\NotUniqueException;
use App\Repository\AdminUserRepository;
use App\Request\AdminUser\CreateAdminUserRequest;
use App\Request\AdminUser\GetAdminUsersRequest;
use App\Request\AdminUser\UpdateAdminUserRequest;
use App\Request\User\GetUsersRequest;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AdminUserValidator extends AbstractValidator
{
    private AdminUserRepository $repository;

    public function __construct(ValidatorInterface $validator, AdminUserRepository $repository)
    {
        parent::__construct($validator);

        $this->repository = $repository;
    }

    public function validateListRequest(GetAdminUsersRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateCreateRequest(CreateAdminUserRequest $request): void
    {
        $this->validateRequestObject($request);
        $this->validateUniqueEmailOnCreate($request->getEmail());
    }

    public function validateUpdateRequest(UpdateAdminUserRequest $request, int $id): void
    {
        $this->validateRequestObject($request);
        $this->validateUniqueEmailOnUpdate($id, $request->getEmail(), );
    }

    private function validateUniqueEmailOnCreate(string $email): void
    {
        $user = $this->repository->findOneBy(['email' => $email]);

        if ($user) {
            throw new NotUniqueException('Vartotojas su tokiu el. pašto adresu jau egzistuoja', ExceptionCode::ADMIN_USER_EMAIL_NOT_UNIQUE_CODE);
        }
    }

    private function validateUniqueEmailOnUpdate(int $id, string $email): void
    {
        $user = $this->repository->findOneBy(['email' => $email]);

        if ($user && $id !== $user->getId()) {
            throw new NotUniqueException('Vartotojas su tokiu el. pašto adresu jau egzistuoja', ExceptionCode::ADMIN_USER_EMAIL_NOT_UNIQUE_CODE);
        }
    }
}

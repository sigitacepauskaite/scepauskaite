<?php

namespace App\Validator;

use App\Request\Place\CreatePlaceRequest;
use App\Request\Place\GetPlacesRequest;
use App\Request\Place\UpdatePlaceRequest;
use App\Request\Trip\AddTripStopRequest;
use App\Request\Trip\CreateTripRequest;
use App\Request\Trip\GetTripsRequest;
use App\Request\Trip\UpdateTripsRequest;

class TripRequestValidator extends AbstractValidator
{
    public function validateCreateRequest(CreateTripRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateUpdateRequest(UpdateTripsRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateListRequest(GetTripsRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateAddTripStopRequest(AddTripStopRequest $request): void
    {
        $this->validateRequestObject($request);
    }
}

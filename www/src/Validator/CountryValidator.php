<?php

namespace App\Validator;

use App\Request\Article\CreateCountryRequest;
use App\Request\Article\GetCountriesRequest;
use App\Request\Article\UpdateCountryRequest;

class CountryValidator  extends AbstractValidator
{
    public function validateListRequest(GetCountriesRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateCreateRequest(CreateCountryRequest $request): void
    {
        $this->validateRequestObject($request);
    }

    public function validateUpdateRequest(UpdateCountryRequest $request): void
    {
        $this->validateRequestObject($request);
    }
}

<?php

namespace App\Request\Article;

use App\Request\RequestInterface;
use App\Request\User\GetUsersRequest;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GetArticlesRequest implements RequestInterface
{
    /**
     * @SerializedName("userId")
     */
    private ?string $userId = null;

    /**
     * @SerializedName("deletedArticles")
     */
    private bool $deletedArticles = false;

    /**
     * @SerializedName("searchText")
     */
    protected ?string $searchText = null;

    /**
     * @SerializedName("searchDateStart")
     */
    protected ?\DateTime $searchDateStart = null;

    /**
     * @var \DateTime|null
     *
     * @SerializedName("searchDateEnd")
     */
    protected ?\DateTime $searchDateEnd = null;

    /**
     * @SerializedName("start")
     */
    protected ?string $start = null;

    /**
     * @SerializedName("limit")
     */
    protected ?string $limit = null;

    public function isDeletedArticles(): bool
    {
        return $this->deletedArticles;
    }

    public function setDeletedArticles(bool $deletedArticles): GetArticlesRequest
    {
        $this->deletedArticles = $deletedArticles;

        return $this;
    }

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): GetArticlesRequest
    {
        $this->searchText = $searchText;

        return $this;
    }

    public function getSearchDateStart(): ?\DateTime
    {
        return $this->searchDateStart;
    }

    public function setSearchDateStart(?\DateTime $searchDateStart): GetArticlesRequest
    {
        $this->searchDateStart = $searchDateStart;

        return $this;
    }

    public function getSearchDateEnd(): ?\DateTime
    {
        return $this->searchDateEnd;
    }

    public function setSearchDateEnd(?\DateTime $searchDateEnd): GetArticlesRequest
    {
        $this->searchDateEnd = $searchDateEnd;

        return $this;
    }

    public function getStart(): ?string
    {
        return $this->start;
    }

    public function setStart(?string $start): GetArticlesRequest
    {
        $this->start = $start;

        return $this;
    }

    public function getLimit(): ?string
    {
        return $this->limit;
    }

    public function setLimit(?string $limit): GetArticlesRequest
    {
        $this->limit = $limit;

        return $this;
    }

    public function getUserId(): ?string
    {
        return $this->userId;
    }

    public function setUserId(?string $userId): GetArticlesRequest
    {
        $this->userId = $userId;

        return $this;
    }
}

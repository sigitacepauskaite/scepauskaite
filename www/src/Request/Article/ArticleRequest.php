<?php

namespace App\Request\Article;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

abstract class ArticleRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank()
     * @SerializedName("name")
     */
    protected ?string $title;

    /**
     * @Assert\NotBlank()
     * @SerializedName("content")
     */
    protected ?string $content = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): ArticleRequest
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): ArticleRequest
    {
        $this->content = $content;

        return $this;
    }
}

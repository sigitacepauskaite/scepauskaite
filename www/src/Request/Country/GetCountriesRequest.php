<?php

namespace App\Request\Country;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GetCountriesRequest implements RequestInterface
{
    /**
     * @SerializedName("deletedArticles")
     */
    private bool $deletedCountries = false;

    /**
     * @SerializedName("searchText")
     */
    protected ?string $searchText = null;

    /**
     * @SerializedName("searchDateStart")
     */
    protected ?\DateTime $searchDateStart = null;

    /**
     * @var \DateTime|null
     *
     * @SerializedName("searchDateEnd")
     */
    protected ?\DateTime $searchDateEnd = null;

    /**
     * @SerializedName("start")
     */
    protected ?string $start = null;

    /**
     * @SerializedName("limit")
     */
    protected ?string $limit = null;

    public function isDeletedCountries(): bool
    {
        return $this->deletedCountries;
    }

    public function setDeletedCountries(bool $deletedCountries): GetCountriesRequest
    {
        $this->deletedCountries = $deletedCountries;

        return $this;
    }

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): GetCountriesRequest
    {
        $this->searchText = $searchText;

        return $this;
    }

    public function getSearchDateStart(): ?\DateTime
    {
        return $this->searchDateStart;
    }

    public function setSearchDateStart(?\DateTime $searchDateStart): GetCountriesRequest
    {
        $this->searchDateStart = $searchDateStart;

        return $this;
    }

    public function getSearchDateEnd(): ?\DateTime
    {
        return $this->searchDateEnd;
    }

    public function setSearchDateEnd(?\DateTime $searchDateEnd): GetCountriesRequest
    {
        $this->searchDateEnd = $searchDateEnd;

        return $this;
    }

    public function getStart(): ?string
    {
        return $this->start;
    }

    public function setStart(?string $start): GetCountriesRequest
    {
        $this->start = $start;

        return $this;
    }

    public function getLimit(): ?string
    {
        return $this->limit;
    }

    public function setLimit(?string $limit): GetCountriesRequest
    {
        $this->limit = $limit;

        return $this;
    }
}

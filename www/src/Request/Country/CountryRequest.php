<?php

namespace App\Request\Country;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

abstract class CountryRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank()
     * @SerializedName("name")
     */
    protected string $name;

    /**
     * @Assert\NotBlank()
     * @SerializedName("code")
     */
    protected string $code;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }
}

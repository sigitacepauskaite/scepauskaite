<?php

namespace App\Request\Place;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

abstract class PlaceRequest implements RequestInterface
{
    /**
     * @SerializedName("name")
     * @Assert\NotBlank()
     */
    protected string $name;

    /**
     * @SerializedName("address")
     * @Assert\NotBlank()
     */
    protected string $address;

    /**
     * @SerializedName("latitude")
     * @Assert\NotBlank()
     */
    protected $latitude;

    /**
     * @SerializedName("longitude")
     * @Assert\NotBlank()
     */
    protected $longitude;

    /**
     * @SerializedName("description")
     * @Assert\NotBlank()
     */
    protected string $description;

    /**
     * @SerializedName("tags")
     * @Assert\NotBlank()
     */
    protected array $tags;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): PlaceRequest
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): PlaceRequest
    {
        $this->address = $address;

        return $this;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function setLatitude($latitude): PlaceRequest
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function setLongitude($longitude): PlaceRequest
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }
}

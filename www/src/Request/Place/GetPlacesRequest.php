<?php

namespace App\Request\Place;

use App\Enumerator\PlaceStatusType;
use App\Enumerator\UserType;
use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GetPlacesRequest  implements RequestInterface
{
    /**
     * @SerializedName("deletePlaces")
     */
    private bool $deletedPlaces = false;

    private array $statuses = [];

    /**
     * @SerializedName("searchText")
     */
    protected ?string $searchText = null;

    /**
     * @SerializedName("searchDateStart")
     */
    protected ?\DateTime $searchDateStart = null;

    /**
     * @var \DateTime|null
     *
     * @SerializedName("searchDateEnd")
     */
    protected ?\DateTime $searchDateEnd = null;

    /**
     * @SerializedName("start")
     */
    protected ?string $start = null;

    /**
     * @SerializedName("limit")
     */
    protected ?string $limit = null;

    /**
     * @SerializedName("locationFilters")
     */
    private array $locationFilters = [];

    /**
     * @SerializedName("activityFilters")
     */
    private array $activityFilters = [];

    public function isDeletedPlaces(): bool
    {
        return $this->deletedPlaces;
    }

    public function setDeletedPlaces(bool $deletedPlaces): GetPlacesRequest
    {
        $this->deletedPlaces = $deletedPlaces;

        return $this;
    }

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): GetPlacesRequest
    {
        $this->searchText = $searchText;

        return $this;
    }

    public function getSearchDateStart(): ?\DateTime
    {
        return $this->searchDateStart;
    }

    public function setSearchDateStart(?\DateTime $searchDateStart): GetPlacesRequest
    {
        $this->searchDateStart = $searchDateStart;

        return $this;
    }

    public function getSearchDateEnd(): ?\DateTime
    {
        return $this->searchDateEnd;
    }

    public function setSearchDateEnd(?\DateTime $searchDateEnd): GetPlacesRequest
    {
        $this->searchDateEnd = $searchDateEnd;

        return $this;
    }

    public function getStart(): ?string
    {
        return $this->start;
    }

    public function setStart(?string $start): GetPlacesRequest
    {
        $this->start = $start;

        return $this;
    }

    public function getLimit(): ?string
    {
        return $this->limit;
    }

    public function setLimit(?string $limit): GetPlacesRequest
    {
        $this->limit = $limit;

        return $this;
    }

    public function getStatuses(): array
    {
        return $this->statuses;
    }

    public function setStatuses(array $statuses): GetPlacesRequest
    {
        $this->statuses = $statuses;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getLocationFilters(): array
    {
        return $this->locationFilters;
    }

    /**
     * @param string[] $locationFilters
     */
    public function setLocationFilters(array $locationFilters): void
    {
        $this->locationFilters = $locationFilters;
    }

    /**
     * @return string[]
     */
    public function getActivityFilters(): array
    {
        return $this->activityFilters;
    }

    /**
     * @param string[] $activityFilters
     */
    public function setActivityFilters(array $activityFilters): void
    {
        $this->activityFilters = $activityFilters;
    }
}

<?php

namespace App\Request\PlaceVisit;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class AddPlaceVisitRequest implements RequestInterface
{
    /**
     * @SerializedName("note")
     */
    protected ?string $note;

    /**
     * @SerializedName("rate")
     */
    protected ?string $rate;

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getRate(): ?string
    {
        return $this->rate;
    }

    public function setRate(?string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}

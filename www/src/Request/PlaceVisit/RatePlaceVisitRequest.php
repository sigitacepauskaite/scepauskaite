<?php

namespace App\Request\PlaceVisit;

use App\Entity\Place;
use App\Entity\PlaceVisit;
use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

class RatePlaceVisitRequest implements RequestInterface
{
    /**
     * @SerializedName("note")
     * @Assert\NotBlank()
     */
    protected string $note;

    /**
     * @SerializedName("rate")
     * @Assert\NotBlank()
     */
    protected float $rate;

    public function getNote(): string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}

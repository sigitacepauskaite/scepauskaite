<?php

namespace App\Request\AdminUser;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GetAdminUsersRequest implements RequestInterface
{
    /**
     * @SerializedName("deletedUsers")
     */
    private bool $deletedUsers = false;

    /**
     * @SerializedName("searchText")
     */
    protected ?string $searchText = null;

    /**
     * @SerializedName("searchDateStart")
     */
    protected ?\DateTime $searchDateStart = null;

    /**
     * @var \DateTime|null
     *
     * @SerializedName("searchDateEnd")
     */
    protected ?\DateTime $searchDateEnd = null;

    /**
     * @SerializedName("start")
     */
    protected ?int $start = null;

    /**
     * @SerializedName("limit")
     */
    protected ?int $limit = null;

    public function isDeletedUsers(): bool
    {
        return $this->deletedUsers;
    }

    public function setDeletedUsers(bool $deletedUsers): self
    {
        $this->deletedUsers = $deletedUsers;

        return $this;
    }

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): self
    {
        $this->searchText = $searchText;

        return $this;
    }

    public function getSearchDateStart(): ?\DateTime
    {
        return $this->searchDateStart;
    }

    public function setSearchDateStart(?\DateTime $searchDateStart): self
    {
        $this->searchDateStart = $searchDateStart;

        return $this;
    }

    public function getSearchDateEnd(): ?\DateTime
    {
        return $this->searchDateEnd;
    }

    public function setSearchDateEnd(?\DateTime $searchDateEnd): self
    {
        $this->searchDateEnd = $searchDateEnd;

        return $this;
    }

    public function getStart(): ?int
    {
        return $this->start;
    }

    public function setStart(?int $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getLimit(): ?int
    {
        return $this->limit;
    }

    public function setLimit(?int $limit): self
    {
        $this->limit = $limit;

        return $this;
    }
}

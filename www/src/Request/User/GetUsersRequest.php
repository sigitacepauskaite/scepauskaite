<?php

namespace App\Request\User;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GetUsersRequest implements RequestInterface
{
    /**
     * @SerializedName("deletedUsers")
     */
    private bool $deletedUsers = false;

    /**
     * @SerializedName("searchText")
     */
    protected ?string $searchText = null;

    /**
     * @SerializedName("searchDateStart")
     */
    protected ?\DateTime $searchDateStart = null;

    /**
     * @var \DateTime|null
     *
     * @SerializedName("searchDateEnd")
     */
    protected ?\DateTime $searchDateEnd = null;

    /**
     * @SerializedName("start")
     */
    protected ?string $start = null;

    /**
     * @SerializedName("limit")
     */
    protected ?string $limit = null;

    public function isDeletedUsers(): bool
    {
        return $this->deletedUsers;
    }

    public function setDeletedUsers(bool $deletedUsers): GetUsersRequest
    {
        $this->deletedUsers = $deletedUsers;

        return $this;
    }

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): GetUsersRequest
    {
        $this->searchText = $searchText;

        return $this;
    }

    public function getSearchDateStart(): ?\DateTime
    {
        return $this->searchDateStart;
    }

    public function setSearchDateStart(?\DateTime $searchDateStart): GetUsersRequest
    {
        $this->searchDateStart = $searchDateStart;

        return $this;
    }

    public function getSearchDateEnd(): ?\DateTime
    {
        return $this->searchDateEnd;
    }

    public function setSearchDateEnd(?\DateTime $searchDateEnd): GetUsersRequest
    {
        $this->searchDateEnd = $searchDateEnd;

        return $this;
    }

    public function getStart(): ?string
    {
        return $this->start;
    }

    public function setStart(?string $start): GetUsersRequest
    {
        $this->start = $start;

        return $this;
    }

    public function getLimit(): ?string
    {
        return $this->limit;
    }

    public function setLimit(?string $limit): GetUsersRequest
    {
        $this->limit = $limit;

        return $this;
    }
}

<?php

namespace App\Request\User;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

abstract class UserRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @SerializedName("email")
     */
    protected ?string $email;

    /**
     * @SerializedName("password")
     */
    protected ?string $password = null;

    /**
     * @Assert\NotBlank()
     * @SerializedName("firstName")
     */
    protected ?string $firstName = null;

    /**
     * @Assert\NotBlank()
     * @SerializedName("lastName")
     */
    protected ?string $lastName;

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }
}

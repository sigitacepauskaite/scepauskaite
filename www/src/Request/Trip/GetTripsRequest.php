<?php

namespace App\Request\Trip;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class GetTripsRequest implements RequestInterface
{
    /**
     * @SerializedName("deletedTrips")
     */
    private bool $deletedTrips = false;

    /**
     * @SerializedName("searchText")
     */
    protected ?string $searchText = null;

    /**
     * @SerializedName("searchDateStart")
     */
    protected ?\DateTime $searchDateStart = null;

    /**
     * @var \DateTime|null
     *
     * @SerializedName("searchDateEnd")
     */
    protected ?\DateTime $searchDateEnd = null;

    /**
     * @SerializedName("start")
     */
    protected ?string $start = null;

    /**
     * @SerializedName("limit")
     */
    protected ?string $limit = null;

    public function isDeletedTrips(): bool
    {
        return $this->deletedTrips;
    }

    public function setDeletedTrips(bool $deletedTrips): GetTripsRequest
    {
        $this->deletedTrips = $deletedTrips;

        return $this;
    }

    public function getSearchText(): ?string
    {
        return $this->searchText;
    }

    public function setSearchText(?string $searchText): GetTripsRequest
    {
        $this->searchText = $searchText;

        return $this;
    }

    public function getSearchDateStart(): ?\DateTime
    {
        return $this->searchDateStart;
    }

    public function setSearchDateStart(?\DateTime $searchDateStart): GetTripsRequest
    {
        $this->searchDateStart = $searchDateStart;

        return $this;
    }

    public function getSearchDateEnd(): ?\DateTime
    {
        return $this->searchDateEnd;
    }

    public function setSearchDateEnd(?\DateTime $searchDateEnd): GetTripsRequest
    {
        $this->searchDateEnd = $searchDateEnd;

        return $this;
    }

    public function getStart(): ?string
    {
        return $this->start;
    }

    public function setStart(?string $start): GetTripsRequest
    {
        $this->start = $start;

        return $this;
    }

    public function getLimit(): ?string
    {
        return $this->limit;
    }

    public function setLimit(?string $limit): GetTripsRequest
    {
        $this->limit = $limit;

        return $this;
    }
}

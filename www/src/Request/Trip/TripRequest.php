<?php

namespace App\Request\Trip;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

abstract class TripRequest implements RequestInterface
{
    /**
     * @Assert\NotBlank()
     * @SerializedName("name")
     */
    protected ?string $name;

    /**
     * @SerializedName("duration")
     */
    protected ?string $duration;

    /**
     * @SerializedName("distance")
     */
    protected string $distance = '0';

    /**
     * @Assert\NotBlank()
     * @SerializedName("tripDate")
     */
    protected \DateTime $tripDate;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): TripRequest
    {
        $this->name = $name;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): TripRequest
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDistance(): string
    {
        return $this->distance;
    }

    public function setDistance(string $distance): TripRequest
    {
        $this->distance = $distance;

        return $this;
    }

    public function getTripDate(): \DateTime
    {
        return $this->tripDate;
    }

    public function setTripDate(\DateTime $tripDate): TripRequest
    {
        $this->tripDate = $tripDate;

        return $this;
    }
}

<?php

namespace App\Request\Trip;

use App\Request\RequestInterface;
use Symfony\Component\Serializer\Annotation\SerializedName;

class AddTripStopRequest implements RequestInterface
{
    /**
     * @SerializedName("note")
     */
    private $note;

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): AddTripStopRequest
    {
        $this->note = $note;

        return $this;
    }
}

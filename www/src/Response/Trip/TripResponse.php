<?php

namespace App\Response\Trip;

use App\Response\TripStop\TripStopResponse;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

abstract class TripResponse
{
    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    protected ?int $id;

    /**
     * @SerializedName("name")
     * @Groups("Default")
     */
    protected ?string $name;

    /**
     * @SerializedName("duration")
     * @Groups("Default")
     */
    protected ?string $duration;

    /**
     * @SerializedName("distance")
     * @Groups("Default")
     */
    protected string $distance;

    /**
     * @SerializedName("tripDate")
     * @Groups("Default")
     */
    protected \DateTime $tripDate;

    /**
     * @SerializedName("tripStops")
     * @Groups("Default")
     * @var TripStopResponse[] $tripStops
     */
    protected array $tripStops;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): TripResponse
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): TripResponse
    {
        $this->name = $name;

        return $this;
    }

    public function getDuration(): ?string
    {
        return $this->duration;
    }

    public function setDuration(?string $duration): TripResponse
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDistance(): string
    {
        return $this->distance;
    }

    public function setDistance(string $distance): TripResponse
    {
        $this->distance = $distance;

        return $this;
    }

    public function getTripDate(): \DateTime
    {
        return $this->tripDate;
    }

    public function setTripDate(\DateTime $tripDate): TripResponse
    {
        $this->tripDate = $tripDate;

        return $this;
    }

    public function getTripStops(): array
    {
        return $this->tripStops;
    }

    public function setTripStops(array $tripStops): TripResponse
    {
        $this->tripStops = $tripStops;

        return $this;
    }
}

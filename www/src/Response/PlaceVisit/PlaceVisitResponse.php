<?php

namespace App\Response\PlaceVisit;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class PlaceVisitResponse
{

    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    protected int $id;

    /**
     * @SerializedName("userId")
     * @Groups("Default")
     */
    protected ?int $userId;

    /**
     * @SerializedName("userFirstName")
     * @Groups("Default")
     */
    protected ?string $userFirstName;

    /**
     * @SerializedName("userFirstName")
     * @Groups("Default")
     */
    protected ?string $userLastName;

    /**
     * @SerializedName("note")
     * @Groups("Default")
     */
    protected ?string $note;

    /**
     * @SerializedName("rate")
     * @Groups("Default")
     */
    protected ?float $rate;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }

    public function setUserId(?int $userId): self
    {
        $this->userId = $userId;

        return $this;
    }

    public function getUserFirstName(): ?string
    {
        return $this->userFirstName;
    }

    public function setUserFirstName(?string $userFirstName): self
    {
        $this->userFirstName = $userFirstName;

        return $this;
    }

    public function getUserLastName(): ?string
    {
        return $this->userLastName;
    }

    public function setUserLastName(?string $userLastName): self
    {
        $this->userLastName = $userLastName;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getRate(): ?float
    {
        return $this->rate;
    }

    public function setRate(?float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}

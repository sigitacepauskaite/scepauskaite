<?php

namespace App\Response\Place;

use App\Entity\Country;
use App\Response\Country\CountryResponse;
use App\Response\Photo\PhotoResponse;
use App\Response\PlaceVisit\PlaceVisitResponse;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class PlaceResponse
{
    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    protected ?int $id;

    /**
     * @SerializedName("name")
     * @Groups("Default")
     */
    protected string $name;

    /**
     * @SerializedName("address")
     * @Groups("Default")
     */
    protected string $address;

    /**
     * @SerializedName("latitude")
     * @Groups("Default")
     */
    protected string $latitude;

    /**
     * @SerializedName("longitude")
     * @Groups("Default")
     */
    protected string $longitude;

    /**
     * @SerializedName("description")
     * @Groups("Default")
     */
    protected string $description;

    /**
     * @SerializedName("photos")
     * @Groups("Default")
     * @var PhotoResponse[] $photos;
     */
    protected array $photos;

    /**
     * @SerializedName("placeVisits")
     * @Groups("Default")
     * @var PlaceVisitResponse[] $placeVisits
     */
    protected array $placeVisits;

    /**
     * @SerializedName("country")
     * @Groups("Default")
     */
    protected CountryResponse $country;

    /**
     * @SerializedName("tags")
     * @Groups("Default")
     * @var string[] $tags
     */
    protected array $tags;

    /**
     * @SerializedName("status")
     * @Groups("Default")
     * @var string $status
     */
    protected $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): PlaceResponse
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): PlaceResponse
    {
        $this->name = $name;

        return $this;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): PlaceResponse
    {
        $this->address = $address;

        return $this;
    }

    public function getLatitude(): string
    {
        return $this->latitude;
    }

    public function setLatitude(string $latitude): PlaceResponse
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): string
    {
        return $this->longitude;
    }

    public function setLongitude(string $longitude): PlaceResponse
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): PlaceResponse
    {
        $this->description = $description;

        return $this;
    }

    public function getPhotos(): array
    {
        return $this->photos;
    }

    public function setPhotos(array $photos): PlaceResponse
    {
        $this->photos = $photos;

        return $this;
    }

    public function getPlaceVisits(): array
    {
        return $this->placeVisits;
    }

    public function setPlaceVisits(array $placeVisits): PlaceResponse
    {
        $this->placeVisits = $placeVisits;

        return $this;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): PlaceResponse
    {
        $this->tags = $tags;

        return $this;
    }

    public function getCountry(): CountryResponse
    {
        return $this->country;
    }

    public function setCountry(CountryResponse $country): PlaceResponse
    {
        $this->country = $country;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): PlaceResponse
    {
        $this->status = $status;

        return $this;
    }
}

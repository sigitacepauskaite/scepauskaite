<?php

namespace App\Response\User;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

abstract class UserResponse
{
    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    protected int $id;

    /**
     * @SerializedName("email")
     * @Groups("Default")
     */
    protected ?string $email;

    /**
     * @SerializedName("firstName")
     * @Groups("Default")
     */
    protected ?string $firstName = null;

    /**
     * @SerializedName("lastName")
     * @Groups("Default")
     */
    protected ?string $lastName;

    /**
     * @SerializedName("roles")
     * @Groups("Default")
     */
    protected ?array $roles;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getRoles(): ?array
    {
        return $this->roles;
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}

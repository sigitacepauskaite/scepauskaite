<?php

namespace App\Response\Photo;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class PhotoResponse
{
    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    protected int $id;

    /**
     * @SerializedName("url")
     * @Groups("Default")
     */
    protected string $url;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): PhotoResponse
    {
        $this->id = $id;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): PhotoResponse
    {
        $this->url = $url;

        return $this;
    }
}

<?php

namespace App\Response\TripStop;

use App\Response\Place\PlaceResponse;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class TripStopResponse
{
    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    private int $id;

    /**
     * @SerializedName("note")
     * @Groups("Default")
     */
    private ?string $note;

    /**
     * @SerializedName("place")
     * @Groups("Default")
     */
    private PlaceResponse $place;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): TripStopResponse
    {
        $this->id = $id;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): TripStopResponse
    {
        $this->note = $note;

        return $this;
    }

    public function getPlace(): PlaceResponse
    {
        return $this->place;
    }

    public function setPlace(PlaceResponse $place): TripStopResponse
    {
        $this->place = $place;

        return $this;
    }
}

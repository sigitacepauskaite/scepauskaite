<?php

namespace App\Response\TripStop;

use Symfony\Component\Serializer\Annotation\SerializedName;

class AddTripStopResponse extends TripStopResponse
{
    /**
     * @SerializedName("note")
     */
    private ?string $note;

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): AddTripStopResponse
    {
        $this->note = $note;

        return $this;
    }
}

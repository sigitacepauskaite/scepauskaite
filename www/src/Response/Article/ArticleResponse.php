<?php

namespace App\Response\Article;

use App\Request\Article\ArticleRequest;
use App\Response\Photo\PhotoResponse;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

abstract class ArticleResponse
{
    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    protected ?int $id;

    /**
     * @SerializedName("name")
     * @Groups("Default")
     */
    protected ?string $title;

    /**
     * @SerializedName("content")
     * @Groups("Default")
     */
    protected ?string $content = null;

    /**
     * @SerializedName("photos")
     * @Groups("Default")
     * @var PhotoResponse[] $photos
     */
    private array $photos;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): ArticleResponse
    {
        $this->id = $id;

        return $this;
    }


    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): ArticleResponse
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): ArticleResponse
    {
        $this->content = $content;

        return $this;
    }

    public function getPhotos(): array
    {
        return $this->photos;
    }

    public function setPhotos(array $photos): self
    {
        $this->photos = $photos;

        return $this;
    }
}

<?php

namespace App\Response\Country;

use App\Response\PlaceVisit\PlaceVisitResponse;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

class CountryResponse
{
    /**
     * @SerializedName("id")
     * @Groups("Default")
     */
    protected ?int $id;

    /**
     * @SerializedName("name")
     * @Groups("Default")
     */
    protected string $name;

    /**
     * @SerializedName("code")
     * @Groups("Default")
     */
    protected string $code;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): CountryResponse
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): CountryResponse
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): CountryResponse
    {
        $this->code = $code;

        return $this;
    }
}

<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $response = $this->createErrorResponse($event->getThrowable());

        $event->allowCustomResponseCode();
        $event->setResponse($response);
    }

    private function createErrorResponse(\Throwable $exception): Response
    {
        if ($exception instanceof HttpExceptionInterface) {
            return new JsonResponse(
                $this->buildErrorResponse($exception),
                $exception->getStatusCode()
            );
        }

        return new JsonResponse('Unexpected error occurred.', Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    private function buildErrorResponse(\Throwable $exception): array
    {
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

        if ($exception instanceof HttpExceptionInterface) {
            $statusCode = $exception->getStatusCode();
        }

        return [
            'error' => [
                'status' => $statusCode,
                'message' => $exception->getMessage(),
                'code' => $exception->getCode(),
            ],
        ];
    }
}

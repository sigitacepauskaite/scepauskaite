<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class BadRequestException extends AbstractException implements HttpExceptionInterface
{
    protected $statusCode = Response::HTTP_BAD_REQUEST;

    public function __construct(string $message = 'Invalid request', int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

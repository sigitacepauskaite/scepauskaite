<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class NotUniqueException extends AbstractException implements HttpExceptionInterface
{
    protected $statusCode = Response::HTTP_CONFLICT;

    public function __construct(string $message = 'Provided data are not unique', int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

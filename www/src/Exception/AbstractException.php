<?php

namespace App\Exception;

class AbstractException extends \Exception
{
    protected $statusCode = 0;

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getHeaders(): array
    {
        return [];
    }
}

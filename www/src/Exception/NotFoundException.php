<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class NotFoundException extends AbstractException implements HttpExceptionInterface
{
    protected $statusCode = Response::HTTP_NOT_FOUND;

    public function __construct(string $message = 'Not found', int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

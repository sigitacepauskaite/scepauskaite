<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ApiException extends AbstractException implements HttpExceptionInterface
{
    protected $statusCode = Response::HTTP_BAD_GATEWAY;

    public function __construct(string $message = 'Failed', int $code = 0, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}

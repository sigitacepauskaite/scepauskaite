<?php

namespace App\Service;

use App\Exception\ApiException;
use App\Exception\ExceptionCode;
use http\Exception\BadConversionException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private SluggerInterface $slugger;

    private string $kernelRootDir;

    private string $url;

    public function __construct(SluggerInterface $slugger, string $kernelRootDir, string $url)
    {
        $this->slugger = $slugger;
        $this->kernelRootDir = $kernelRootDir;
        $this->url = $url;
    }

    public function upload(UploadedFile $file): array
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            throw new ApiException($e->getMessage(),ExceptionCode::FILE_UPLOAD_FAILED_CODE);
        }

        return ['fileName' => $fileName, 'filePath' => $this->url.'/uploads/'. $fileName];
    }

    private function getTargetDirectory(): string
    {
        return $this->kernelRootDir.'/public/uploads/';
    }
}

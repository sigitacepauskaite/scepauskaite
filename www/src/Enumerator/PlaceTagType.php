<?php

namespace App\Enumerator;

class PlaceTagType
{
    public const SIGHTSEEING = 'SIGHTSEEING';
    public const NATURE = 'NATURE';
    public const ABANDONED_BUILDINGS = 'ABANDONED_BUILDINGS';
    public const TASTY_FOOD = 'TASTY_FOOD';
}

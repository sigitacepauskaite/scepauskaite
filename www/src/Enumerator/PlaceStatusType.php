<?php

namespace App\Enumerator;

class PlaceStatusType
{
    public const CONFIRMED = 'confirmed';
    public const CREATED = 'created';
    public const CANCELLED = 'cancelled';
}

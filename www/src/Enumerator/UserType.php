<?php

namespace App\Enumerator;

class UserType
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_USER = 'ROLE_USER';
}

<?php

namespace App\Manager;

use App\Entity\Article;
use App\Entity\ArticlePhoto;
use App\Entity\User;
use App\Exception\ExceptionCode;
use App\Exception\NotFoundException;
use App\Factory\ArticleFactory;
use App\Factory\ArticlePhotoFactory;
use App\Repository\ArticlePhotoRepository;
use App\Repository\ArticleRepository;
use App\Request\Article\CreateArticleRequest;
use App\Request\Article\GetArticlesRequest;
use App\Request\Article\UpdateArticleRequest;
use App\Response\Article\CreateArticleResponse;
use App\Response\Article\GetArticleResponse;
use App\Response\Article\UpdateArticleResponse;
use App\Service\FileUploader;
use App\Validator\ArticlePhotoRequestValidator;
use App\Validator\ArticleRequestValidator;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\Request;

class ArticleManager
{
    private ArticleRepository $articleRepository;

    private ArticlePhotoRepository $articlePhotoRepository;

    private ArticleRequestValidator $validator;

    private ArticleFactory $factory;

    private string $kernelRootDir;

    private ArticlePhotoRequestValidator $photoRequestValidator;

    private ArticlePhotoFactory $articlePhotoFactory;

    private FileUploader $fileUploader;

    public function __construct(
        ArticleRepository $articleRepository,
        ArticlePhotoRepository $articlePhotoRepository,
        ArticleRequestValidator $validator,
        ArticleFactory $factory,
        string $kernelRootDir,
        ArticlePhotoRequestValidator $photoRequestValidator,
        ArticlePhotoFactory $articlePhotoFactory,
        FileUploader $fileUploader
    ) {
        $this->articleRepository = $articleRepository;
        $this->articlePhotoRepository = $articlePhotoRepository;
        $this->validator = $validator;
        $this->factory = $factory;
        $this->kernelRootDir = $kernelRootDir;
        $this->photoRequestValidator = $photoRequestValidator;
        $this->articlePhotoFactory = $articlePhotoFactory;
        $this->fileUploader = $fileUploader;
    }

    public function getArticle(int $id): GetArticleResponse
    {
        return $this->factory->getResponse($this->getArticleObject($id));
    }

    public function getArticles(GetArticlesRequest $request): array
    {
        $this->validator->validateListRequest($request);
        $articles = $this->articleRepository->findAllByFilters($request);

        return $this->factory->getListResponse($articles);
    }

    public function createArticle(CreateArticleRequest $request, User $user = null): CreateArticleResponse
    {
        $this->validator->validateCreateRequest($request);

        $article = $this->factory->create($request, $user);
        $this->articleRepository->save($article);

        return $this->factory->createResponse($article);
    }

    public function updateArticle(UpdateArticleRequest $request, int $id, User $user = null): UpdateArticleResponse
    {
        $this->validator->validateUpdateRequest($request);

        $article = $this->getArticleObject($id, $user);
        $article = $this->factory->update($request, $article);
        $this->articleRepository->save($article);

        return $this->factory->updateResponse($article);
    }

    public function uploadArticlePhotos(Request $request, int $id, User $user = null): void
    {
        $files = $request->files->get('images');

        if (empty($files)) {
            throw new NotFoundException('Image was not found', ExceptionCode::ARTICLE_IMAGE_NOT_FOUND_CODE);
        }

        $article = $this->getArticleObject($id, $user);

        foreach ($files as $file) {
            $photoInfo = $this->fileUploader->upload($file);
            $photo = $this->articlePhotoFactory->create($photoInfo, $article);
            $this->articlePhotoRepository->save($photo);
        }
    }

    public function deleteArticle(int $id, User $user = null): void
    {
        $article = $this->getArticleObject($id, $user);
        $article->setDeletedAt(new \DateTime());
        $this->deleteArticlePhotos($article->getArticlePhotosCollection());

        $this->articleRepository->save($article);
    }

    public function deletePhoto(int $id, int $photoId, User $user = null): void
    {
        $photo = $this->getArticlePhotoObject($id, $photoId, $user);
        $this->photoRequestValidator->validateDeleteRequest($photo);

        $photo->setDeletedAt(new \DateTime());
        $this->articlePhotoRepository->save($photo);
    }

    private function deleteArticlePhotos(Collection $articlePhotos): void
    {
        /** @var ArticlePhoto $articlePhoto */
        foreach ($articlePhotos as $articlePhoto) {
            $articlePhoto->setDeletedAt(new \DateTime());
            $this->articlePhotoRepository->persist($articlePhoto);
        }

        $this->articlePhotoRepository->flush();
    }

    private function getArticleObject(int $id, ?User $user = null): Article
    {
        if (!$user) {
            $article = $this->articleRepository->findOneBy(['id' => $id]);
        } else {
            $article = $this->articleRepository->findOneBy(['id' => $id, 'user' => $user]);
        }

        if (!$article) {
            throw new NotFoundException('Article was not found', ExceptionCode::ARTICLE_NOT_FOUND_CODE);
        }

        return $article;
    }

    private function getArticlePhotoObject(int $articleId, int $articlePhotoId, ?User $user = null): ArticlePhoto
    {
        $photo = $this->articlePhotoRepository->findOneBy(['article' => $articleId, 'id' => $articlePhotoId]);

        if ($user) {
            if ($photo && $photo->getArticle()->getUser() !== $user) {
                $photo = null;
            }
        }

        if (!$photo) {
            throw new NotFoundException('Image was not found', ExceptionCode::ARTICLE_IMAGE_NOT_FOUND_CODE);
        }

        return $photo;
    }
}

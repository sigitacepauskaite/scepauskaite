<?php

namespace App\Manager;

use App\Entity\Country;
use App\Exception\ExceptionCode;
use App\Exception\NotFoundException;
use App\Factory\CountryFactory;
use App\Repository\CountryRepository;
use App\Request\Article\CreateCountryRequest;
use App\Request\Article\GetCountriesRequest;
use App\Request\Article\UpdateCountryRequest;
use App\Response\Country\CreateCountryResponse;
use App\Response\Country\GetCountryResponse;
use App\Response\Country\UpdateCountryResponse;
use App\Validator\CountryValidator;

class CountryManager
{
    private CountryRepository $countryRepository;

    private CountryFactory $factory;

    private CountryValidator $validator;

    public function __construct(
        CountryRepository $countryRepository,
        CountryFactory $countryFactory,
        CountryValidator $validator
    ) {
        $this->countryRepository = $countryRepository;
        $this->factory = $countryFactory;
        $this->validator = $validator;
    }

    public function getCountry(int $id): GetCountryResponse
    {
        return $this->factory->getResponse($this->getObject($id));
    }

    public function getCountries(GetCountriesRequest $request): array
    {
        $this->validator->validateListRequest($request);
        $countries = $this->countryRepository->findAllByFilters($request);

        return $this->factory->getListResponse($countries);
    }

    public function createCountry(CreateCountryRequest $request): CreateCountryResponse
    {
        $this->validator->validateCreateRequest($request);

        $country = $this->factory->create($request);
        $this->countryRepository->save($country);

        return $this->factory->createResponse($country);
    }

    public function updateCountry(UpdateCountryRequest $request, int $id): UpdateCountryResponse
    {
        $this->validator->validateUpdateRequest($request);

        $country = $this->getObject($id);
        $country= $this->factory->update($request, $country);
        $this->countryRepository->save($country);

        return $this->factory->updateResponse($country);
    }

    public function deleteCountry(int $id): void
    {
        $country = $this->getObject($id);
        $country->setDeletedAt(new \DateTime());
        $this->countryRepository->save($country);
    }

    private function getObject(int $id): Country
    {
        $country = $this->countryRepository->findOneBy(['id' => $id]);

        if (!$country) {
            throw new NotFoundException('Country was not found', ExceptionCode::COUNTRY_NOT_FOUND_CODE);
        }

        return $country;
    }
}

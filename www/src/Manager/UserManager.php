<?php

namespace App\Manager;

use App\Entity\Country;
use App\Entity\User;
use App\Enumerator\UserType;
use App\Exception\ExceptionCode;
use App\Exception\NotFoundException;
use App\Factory\UserFactory;
use App\Repository\CountryRepository;
use App\Repository\RoleRepository;
use App\Repository\UserRepository;
use App\Request\User\CreateUserRequest;
use App\Request\User\GetUsersRequest;
use App\Request\User\UpdateUserRequest;
use App\Response\User\CreateUserResponse;
use App\Response\User\GetUserResponse;
use App\Response\User\UpdateUserResponse;
use App\Validator\UserRequestValidator;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\User\UserInterface;

class UserManager
{
    private UserRepository $userRepository;

    private UserFactory $factory;

    private RoleRepository $roleRepository;

    private UserRequestValidator $validator;

    private CountryRepository $countryRepository;

    public function __construct(
        UserRepository $userRepository,
        UserFactory $factory,
        RoleRepository $roleRepository,
        UserRequestValidator $validator,
        CountryRepository $countryRepository
    ) {
        $this->userRepository = $userRepository;
        $this->factory = $factory;
        $this->roleRepository = $roleRepository;
        $this->validator = $validator;
        $this->countryRepository = $countryRepository;
    }

    public function getUser(int $id): GetUserResponse
    {
        return $this->factory->getResponse($this->getUserObject($id));
    }

    public function getUsers(GetUsersRequest $request): array
    {
        $this->validator->validateListRequest($request);
        $users = $this->userRepository->findAllByFilters($request);

        return $this->factory->getListResponse($users);
    }

    public function createUser(CreateUserRequest $request, string $countryCode): CreateUserResponse
    {
        $this->validator->validateCreateRequest($request);

        $country = $this->getCountryObject($countryCode);

        $user = $this->factory->create($request, $country);
        $this->userRepository->save($user);

        return $this->factory->createResponse($user);
    }

    public function updateUser(UpdateUserRequest $request, int $id, UserInterface $loggedInUser): UpdateUserResponse
    {
        $user = $this->getUserObject($id);

        if ($user !== $loggedInUser && !in_array(UserType::ROLE_ADMIN, $loggedInUser->getRoles())) {
            throw new AccessDeniedException('Access denied');
        }

        $this->validator->validateUpdateRequest($request, $id);

        $user = $this->factory->update($request, $user);
        $this->userRepository->save($user);

        return $this->factory->updateResponse($user);
    }

    public function deleteUser(int $id, UserInterface $loggedInUser): void
    {
        $user = $this->getUserObject($id);

        if ($user !== $loggedInUser && !in_array(UserType::ROLE_ADMIN, $loggedInUser->getRoles())) {
            throw new AccessDeniedException('Access denied');
        }

        $user->setDeletedAt(new \DateTime());
        $this->userRepository->save($user);
    }

    private function getUserObject(int $id): User
    {
        $user = $this->userRepository->findOneBy(['id' => $id]);

        if (!$user) {
            throw new NotFoundException('User was not found', ExceptionCode::USER_NOT_FOUND_CODE);
        }

        return $user;
    }

    private function getCountryObject(string $countryCode): Country
    {
        $country = $this->countryRepository->findOneBy(['code' => $countryCode]);

        if (!$country) {
            throw new NotFoundException('Country was not found', ExceptionCode::COUNTRY_NOT_FOUND_CODE);
        }

        return $country;
    }
}

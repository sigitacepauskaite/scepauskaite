<?php

namespace App\Manager;

use App\Entity\Country;
use App\Entity\Place;
use App\Entity\PlacePhoto;
use App\Entity\User;
use App\Enumerator\PlaceStatusType;
use App\Exception\ExceptionCode;
use App\Exception\NotFoundException;
use App\Factory\PlaceFactory;
use App\Factory\PlacePhotoFactory;
use App\Factory\PlaceVisitFactory;
use App\Repository\CountryRepository;
use App\Repository\PlacePhotoRepository;
use App\Repository\PlaceRepository;
use App\Repository\PlaceVisitRepository;
use App\Request\Place\CreatePlaceRequest;
use App\Request\Place\GetPlacesRequest;
use App\Request\Place\UpdatePlaceRequest;
use App\Request\PlaceVisit\AddPlaceVisitRequest;
use App\Request\PlaceVisit\RatePlaceVisitRequest;
use App\Response\Place\CreatePlaceResponse;
use App\Response\Place\GetPlaceResponse;
use App\Response\Place\UpdatePlaceResponse;
use App\Response\PlaceVisit\AddPlaceVisitResponse;
use App\Response\PlaceVisit\RatePlaceVisitResponse;
use App\Service\FileUploader;
use App\Validator\PlacePhotoRequestValidator;
use App\Validator\PlaceRequestValidator;
use Symfony\Component\HttpFoundation\Request;

class PlaceManager
{
    private PlaceVisitRepository $placeVisitRepository;

    private PlaceRepository $placeRepository;

    private PlacePhotoRepository $placePhotoRepository;

    private PlaceRequestValidator $validator;

    private PlaceFactory $factory;

    private PlacePhotoRequestValidator $photoRequestValidator;

    private string $kernelRootDir;

    private PlacePhotoFactory $placePhotoFactory;

    private PlaceVisitFactory $placeVisitFactory;

    private FileUploader $fileUploader;

    private CountryRepository $countryRepository;

    public function __construct(
        PlaceVisitRepository $placeVisitRepository,
        PlaceRepository $placeRepository,
        PlacePhotoRepository $placePhotoRepository,
        PlaceRequestValidator $validator,
        PlaceFactory $factory,
        string $kernelRootDir,
        PlacePhotoRequestValidator $photoRequestValidator,
        PlacePhotoFactory $placePhotoFactory,
        PlaceVisitFactory $placeVisitFactory,
        FileUploader $fileUploader,
        CountryRepository $countryRepository
    ) {
        $this->placePhotoRepository = $placePhotoRepository;
        $this->placeVisitRepository = $placeVisitRepository;
        $this->placeRepository = $placeRepository;
        $this->validator = $validator;
        $this->factory = $factory;
        $this->photoRequestValidator = $photoRequestValidator;
        $this->kernelRootDir = $kernelRootDir;
        $this->placePhotoFactory = $placePhotoFactory;
        $this->placeVisitFactory = $placeVisitFactory;
        $this->fileUploader = $fileUploader;
        $this->countryRepository = $countryRepository;
    }

    public function getPlace(int $id): GetPlaceResponse
    {
        return $this->factory->getResponse($this->getPlaceObject($id));
    }

    public function getPlaces(GetPlacesRequest $request): array
    {
        $this->validator->validateListRequest($request);
        $articles = $this->placeRepository->findAllByFilters($request);

        return $this->factory->getListResponse($articles);
    }

    public function getVisitedPlaces(User $user): array
    {
        return $this->factory->getListResponse($user->getVisitedPlaces());
    }

    public function createPlace(CreatePlaceRequest $request, string $countryCode): CreatePlaceResponse
    {
        $this->validator->validateCreateRequest($request);
        $country = $this->getCountryObject($countryCode);
        $place = $this->factory->create($request, $country);
        $this->placeRepository->save($place);

        return $this->factory->createResponse($place);
    }

    public function updatePlace(UpdatePlaceRequest $request, int $id): UpdatePlaceResponse
    {
        $this->validator->validateUpdateRequest($request, $id);

        $place = $this->getPlaceObject($id);
        $place = $this->factory->update($request, $place);
        $this->placeRepository->save($place);

        return $this->factory->updateResponse($place);
    }

    public function uploadPhotos(Request $request, int $id): void
    {
        $files = $request->files->get('images');

        if (empty($files)) {
            throw new NotFoundException('Image was not found', ExceptionCode::PLACE_IMAGE_NOT_FOUND_CODE);
        }

        $place = $this->getPlaceObject($id);

        foreach ($files as $file) {
            $photoInfo = $this->fileUploader->upload($file);
            $photo = $this->placePhotoFactory->create($photoInfo, $place);
            $this->placePhotoRepository->save($photo);
        }
    }

    public function deletePlace(int $id): void
    {
        $place = $this->getPlaceObject($id);
        $place->setDeletedAt(new \DateTime());
        $this->placeRepository->save($place);
    }

    public function deletePhoto(int $id, int $photoId): void
    {
        $photo = $this->getPlacePhotoObject($id, $photoId);

        $this->photoRequestValidator->validateUploadRequest($photo);

        $photo->setDeletedAt(new \DateTime());
        $this->placePhotoRepository->save($photo);
    }

    public function confirmPlace(int $id): void
    {
        $place = $this->getPlaceObject($id);
        $place->setStatus(PlaceStatusType::CONFIRMED);
        $this->placeRepository->save($place);
    }

    public function cancelPlace(int $id): void
    {
        $place = $this->getPlaceObject($id);
        $place->setStatus(PlaceStatusType::CANCELLED);
        $this->placeRepository->save($place);
    }

    public function addPlaceVisit(AddPlaceVisitRequest $request, int $id, ?User $user): AddPlaceVisitResponse
    {
        $this->validator->validateAddPlaceVisitRequest($request);

        $place = $this->getPlaceObject($id);
        $placeVisit = $this->placeVisitFactory->create($request, $place, $user);

        $this->placeVisitRepository->save($placeVisit);

        return $this->placeVisitFactory->createResponse($placeVisit);
    }

    public function ratePlaceVisit(RatePlaceVisitRequest $request, int $id, int $placeVisitId, ?User $user): RatePlaceVisitResponse
    {
        $this->validator->validateRatePlaceRequest($request);

        $placeVisit = $this->placeVisitRepository->findOneBy(['id' => $placeVisitId, 'place' => $id]);

        if (!$placeVisit) {
            throw new NotFoundException('Place was not found', ExceptionCode::PLACE_VISIT_NOT_FOUND_CODE);
        }

        $placeVisit = $this->placeVisitFactory->rateVisitPlace($request, $placeVisit, $user);

        $this->placeVisitRepository->save($placeVisit);

        return $this->placeVisitFactory->rateVisitPlaceResponse($placeVisit);
    }

    private function getPlaceObject(int $id): Place
    {
        $place = $this->placeRepository->findOneBy(['id' => $id]);

        if (!$place) {
            throw new NotFoundException('Place was not found', ExceptionCode::PLACE_NOT_FOUND_CODE);
        }

        return $place;
    }

    private function getPlacePhotoObject(int $placeId, int $placePhotoId): PlacePhoto
    {
        $photo = $this->placePhotoRepository->findOneBy(['place' => $placeId, 'id' => $placePhotoId]);

        if (!$photo) {
            throw new NotFoundException('Image was not found', ExceptionCode::PLACE_IMAGE_NOT_FOUND_CODE);
        }

        return $photo;
    }

    private function getCountryObject(string $countryCode): Country
    {
        $country = $this->countryRepository->findOneBy(['code' => $countryCode]);

        if (!$country) {
            throw new NotFoundException('Country was not found', ExceptionCode::COUNTRY_NOT_FOUND_CODE);
        }

        return $country;
    }
}

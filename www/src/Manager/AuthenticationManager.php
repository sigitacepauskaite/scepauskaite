<?php

namespace App\Manager;

use App\Validator\AuthenticationRequestValidator;

class AuthenticationManager
{
    private AuthenticationRequestValidator $validator;

    public function __construct(AuthenticationRequestValidator $validator)
    {
        $this->validator = $validator;
    }
}

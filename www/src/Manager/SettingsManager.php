<?php

namespace App\Manager;

use App\Repository\CountryRepository;
use App\Validator\SettingsRequestValidator;

class SettingsManager
{
    private CountryRepository $countryRepository;

    private SettingsRequestValidator $validator;

    public function __construct(CountryRepository $countryRepository, SettingsRequestValidator $validator)
    {
        $this->countryRepository = $countryRepository;
        $this->validator = $validator;
    }
}

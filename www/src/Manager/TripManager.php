<?php

namespace App\Manager;

use App\Entity\Place;
use App\Entity\Trip;
use App\Entity\User;
use App\Exception\ExceptionCode;
use App\Exception\NotFoundException;
use App\Factory\TripFactory;
use App\Factory\TripStopFactory;
use App\Repository\PlaceRepository;
use App\Repository\TripRepository;
use App\Repository\TripStopRepository;
use App\Request\Trip\AddTripStopRequest;
use App\Request\Trip\CreateTripRequest;
use App\Request\Trip\GetTripsRequest;
use App\Request\Trip\UpdateTripsRequest;
use App\Response\Trip\CreateTripResponse;
use App\Response\Trip\GetTripResponse;
use App\Response\Trip\UpdateTripResponse;
use App\Response\TripStop\AddTripStopResponse;
use App\Validator\TripRequestValidator;

class TripManager
{
    private TripRepository $tripRepository;

    private TripStopRepository $tripStopRepository;

    private TripRequestValidator $validator;

    private TripFactory $factory;

    private TripStopFactory $tripStopFactory;

    private PlaceRepository $placeRepository;

    public function __construct(
        TripRepository $tripRepository,
        TripStopRepository $tripStopRepository,
        TripRequestValidator $validator,
        TripFactory $factory,
        TripStopFactory $tripStopFactory,
        PlaceRepository $placeRepository
    ) {
        $this->tripRepository = $tripRepository;
        $this->tripStopRepository = $tripStopRepository;
        $this->validator = $validator;
        $this->factory = $factory;
        $this->tripStopFactory = $tripStopFactory;
        $this->placeRepository = $placeRepository;
    }

    public function getTrip(int $id, User $user): GetTripResponse
    {
        return $this->factory->getResponse($this->getTripObject($id, $user));
    }

    public function getTrips(GetTripsRequest $request, User $user): array
    {
        $this->validator->validateListRequest($request);
        $trips = $this->tripRepository->findAllByFilters($request, $user);

        return $this->factory->getListResponse($trips);
    }

    public function createTrip(CreateTripRequest $request, User $user): CreateTripResponse
    {
        $this->validator->validateCreateRequest($request);
        $trip = $this->factory->create($request, $user);
        $this->tripRepository->save($trip);

        return $this->factory->createResponse($trip);
    }

    public function updateTrip(UpdateTripsRequest $request, int $id, User $user): UpdateTripResponse
    {
        $this->validator->validateUpdateRequest($request);

        $trip = $this->getTripObject($id, $user);
        $trip = $this->factory->update($request, $trip);
        $this->tripRepository->save($trip);

        return $this->factory->updateResponse($trip);
    }

    public function deleteTrip(int $id, User $user): void
    {
        $trip = $this->getTripObject($id, $user);
        $trip->setDeletedAt(new \DateTime());
        $this->tripRepository->save($trip);
    }

    public function addTripStop(AddTripStopRequest $request, int $id, int $placeId, User $user): AddTripStopResponse
    {
        $this->validator->validateAddTripStopRequest($request);

        $trip = $this->getTripObject($id, $user);
        $place = $this->getPlaceObject($placeId);

        $tripStop = $this->tripStopFactory->create($request, $trip, $place);

        $this->tripStopRepository->save($tripStop);

        return $this->tripStopFactory->createResponse($tripStop);
    }

    public function removeTripStop(int $stopId, User $user): void
    {
        $tripStop = $this->tripStopRepository->findOneBy(['id' => $stopId]);

        if (!$tripStop || $tripStop->getTrip()->getUser() !== $user) {
            throw new NotFoundException('Trip was not found', ExceptionCode::TRIP_STOP_NOT_FOUND_CODE);
        }

        $tripStop->setDeletedAt(new \DateTime());
        $this->tripStopRepository->save($tripStop);
    }

    private function getTripObject(int $id, User $user): Trip
    {
        $trip = $this->tripRepository->findOneBy(['id' => $id, 'user' => $user]);

        if (!$trip) {
            throw new NotFoundException('TripModel was not found', ExceptionCode::TRIP_NOT_FOUND_CODE);
        }

        return $trip;
    }

    private function getPlaceObject(int $placeId): Place
    {
        $place = $this->placeRepository->findOneBy(['id' => $placeId]);

        if (!$place) {
            throw new NotFoundException('TripModel was not found', ExceptionCode::PLACE_NOT_FOUND_CODE);
        }

        return $place;
    }
}

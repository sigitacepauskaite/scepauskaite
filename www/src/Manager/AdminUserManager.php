<?php

namespace App\Manager;

use App\Entity\AdminUser;
use App\Entity\Role;
use App\Enumerator\UserType;
use App\Exception\ApiException;
use App\Exception\ExceptionCode;
use App\Exception\NotFoundException;
use App\Factory\AdminUserFactory;
use App\Repository\AdminUserRepository;
use App\Repository\RoleRepository;
use App\Request\AdminUser\CreateAdminUserRequest;
use App\Request\AdminUser\GetAdminUsersRequest;
use App\Request\AdminUser\UpdateAdminUserRequest;
use App\Response\AdminUser\CreateAdminUserResponse;
use App\Response\AdminUser\GetAdminUserResponse;
use App\Response\AdminUser\UpdateAdminUserResponse;
use App\Validator\AdminUserValidator;

class AdminUserManager
{
    private AdminUserRepository $repository;

    private AdminUserFactory $factory;

    private AdminUserValidator $validator;

    private RoleRepository $roleRepository;

    public function __construct(
        AdminUserRepository $repository,
        AdminUserFactory $factory,
        AdminUserValidator $validator,
        RoleRepository $roleRepository
    ) {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->validator = $validator;
        $this->roleRepository = $roleRepository;
    }

    public function getAdminUser(int $id): GetAdminUserResponse
    {
        return $this->factory->getResponse($this->getAdminUserObject($id));
    }

    public function getAdminUsers(GetAdminUsersRequest $request): array
    {
        $this->validator->validateListRequest($request);
        $users = $this->repository->findAllByFilters($request);

        return $this->factory->getListResponse($users);
    }

    public function createAdminUser(CreateAdminUserRequest $request): CreateAdminUserResponse
    {
        $this->validator->validateCreateRequest($request);

        $user = $this->factory->create($request, $this->getAdminUserRole());
        $this->repository->save($user);

        return $this->factory->createResponse($user);
    }

    public function updateAdminUser(UpdateAdminUserRequest $request, int $id): UpdateAdminUserResponse
    {
        $this->validator->validateUpdateRequest($request, $id);

        $user = $this->getAdminUserObject($id);
        $user = $this->factory->update($request, $user);
        $this->repository->save($user);

        return $this->factory->updateResponse($user);
    }

    public function deleteAdminUser(int $id): void
    {
        $user = $this->getAdminUserObject($id);
        $user->setDeletedAt(new \DateTime());
        $this->repository->save($user);
    }

    private function getAdminUserObject(int $id): AdminUser
    {
        $user = $this->repository->findOneBy(['id' => $id]);

        if (!$user) {
            throw new NotFoundException('User was not found', ExceptionCode::ADMIN_USER_NOT_FOUND_CODE);
        }

        return $user;
    }

    private function getAdminUserRole(): Role
    {
        $adminUserRole = $this->roleRepository->findOneBy(['role' => UserType::ROLE_ADMIN]);

        if (!$adminUserRole) {
            $adminUserRole = (new Role())
                ->setRole(UserType::ROLE_ADMIN);

            $this->roleRepository->save($adminUserRole);
        }

        return $adminUserRole;
    }
}

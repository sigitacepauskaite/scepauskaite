<?php

namespace App\DataFixtures;

use App\Entity\AdminUser;
use App\Entity\Country;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUserFixtures extends Fixture implements DependentFixtureInterface
{
    private UserPasswordEncoderInterface $userPasswordEncoder;

    public function __construct(UserPasswordEncoderInterface  $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function getDependencies(): array
    {
        return [
            RolesFixtures::class,
        ];
    }

    public function load(ObjectManager $manager)
    {
        $adminUser = new AdminUser();
        $adminUser
            ->setEmail('si.cepauskaite@gmail.com')
            ->setFirstName('Sigita')
            ->setLastName('Čepauskaitė')
            ->setRoles([$this->getReference('ROLE_ADMIN')])
            ->setCreatedAt(new DateTime())
        ;
        $adminUser->setPassword($this->userPasswordEncoder->encodePassword($adminUser, 'test'));

        $manager->persist($adminUser);
        $manager->flush();
    }
}

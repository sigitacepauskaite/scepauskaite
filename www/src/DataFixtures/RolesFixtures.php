<?php

namespace App\DataFixtures;

use App\Entity\Country;
use App\Entity\Role;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RolesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $roleUser = new Role();
        $roleUser
            ->setRole('ROLE_USER')
            ->setCreatedAt(new DateTime())
        ;
        $manager->persist($roleUser);

        $roleAdmin = new Role();
        $roleAdmin
            ->setRole('ROLE_ADMIN')
            ->setCreatedAt(new DateTime())
        ;
        $manager->persist($roleAdmin);

        $manager->flush();

        $this->setReference('ROLE_USER', $roleUser);
        $this->setReference('ROLE_ADMIN', $roleAdmin);
    }
}

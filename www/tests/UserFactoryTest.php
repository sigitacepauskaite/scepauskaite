<?php

namespace Tests;

use App\Entity\Country;
use App\Entity\User;
use App\Enumerator\UserType;
use App\Factory\UserFactory;
use App\Request\User\CreateUserRequest;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $encoder = $this->createMock(UserPasswordEncoderInterface::class);
        $encoder->method('encodePassword')->willReturn('EncodedPassword');
        $factory = new UserFactory($encoder);

        $createRequest = (new CreateUserRequest())
            ->setFirstName('Test')
            ->setLastName('Test')
            ->setEmail('email@gmail.com')
            ->setPassword('Test123456789')
        ;

        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $user = $factory->create($createRequest, $country);
        self::assertEquals($user->getCountry(), $country);
        self::assertEquals($user->getRoles(), [UserType::ROLE_USER]);
        self::assertNotEquals($user->getPassword(), 'Test123456789');
        self::assertEquals($user->getEmail(), 'email@gmail.com');
        self::assertEquals($user->getUsername(), $user->getEmail());
        self::assertNotEquals($user->getSalt(), '');
    }

    public function testCreateResponse(): void
    {
        $encoder = $this->createMock(UserPasswordEncoderInterface::class);
        $factory = new UserFactory($encoder);

        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $user = (new User())
            ->setId(1)
            ->setCountry($country)
            ->setFirstName('Test')
            ->setLastName('Test')
            ->setEmail('email@gmail.com')
            ->setPassword('Test123456789')
        ;

        $response = $factory->createResponse($user);
        self::assertEquals($response->getLastName(), 'Test');
        self::assertEquals($response->getFirstName(), 'Test');
        self::assertEquals($response->getEmail(), 'email@gmail.com');
    }
}

<?php

namespace Tests;

use App\Entity\Article;
use App\Entity\Country;
use App\Entity\User;
use App\Factory\ArticleFactory;
use App\Request\Article\CreateArticleRequest;
use App\Request\Article\UpdateArticleRequest;
use PHPUnit\Framework\TestCase;

class ArticleFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $request = (new CreateArticleRequest())
            ->setContent('Test test test test')
            ->setTitle('Test name');

        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $user = (new User())
            ->setId(1)
            ->setCountry($country)
            ->setFirstName('Test')
            ->setLastName('Test')
            ->setEmail('email@gmail.com')
            ->setPassword('Test123456789')
        ;

        $factory = new ArticleFactory();
        $article = $factory->create($request, $user);
        self::assertEquals($article->getUser(), $user);
        self::assertEquals($article->getContent(), 'Test test test test');
        self::assertEquals($article->getTitle(), 'Test name');
    }

    public function testCreateResponse(): void
    {
        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $user = (new User())
            ->setId(1)
            ->setCountry($country)
            ->setFirstName('Test')
            ->setLastName('Test')
            ->setEmail('email@gmail.com')
            ->setPassword('Test123456789')
        ;

        $article = (new Article())
            ->setId(1)
            ->setUser($user)
            ->setContent('Test test test test')
            ->setTitle('Test name');

        $factory = new ArticleFactory();
        $response = $factory->createResponse($article);
        self::assertEquals($response->getId(), 1);
        self::assertEquals($response->getContent(), 'Test test test test');
        self::assertEquals($response->getTitle(), 'Test name');
        self::assertEquals($response->getPhotos(), []);
    }

    public function testUpdate(): void
    {
        $request = (new UpdateArticleRequest())
            ->setContent('test test')
            ->setTitle('Test name');

        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $user = (new User())
            ->setId(1)
            ->setCountry($country)
            ->setFirstName('Test')
            ->setLastName('Test')
            ->setEmail('email@gmail.com')
            ->setPassword('Test123456789')
        ;

        $article = (new Article())
            ->setId(1)
            ->setUser($user)
            ->setContent('Test test test test')
            ->setTitle('Test name');


        $factory = new ArticleFactory();
        $updatedArticle = $factory->update($request, $article);
        self::assertEquals($updatedArticle->getUser(), $user);
        self::assertEquals($updatedArticle->getContent(), 'test test');
        self::assertEquals($updatedArticle->getTitle(), 'Test name');
    }

    public function testUpdateResponse(): void
    {
        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $user = (new User())
            ->setId(1)
            ->setCountry($country)
            ->setFirstName('Test')
            ->setLastName('Test')
            ->setEmail('email@gmail.com')
            ->setPassword('Test123456789')
        ;

        $article = (new Article())
            ->setId(1)
            ->setUser($user)
            ->setContent('Test test test test')
            ->setTitle('Test name');


        $factory = new ArticleFactory();
        $response = $factory->updateResponse($article);
        self::assertEquals($response->getId(), 1);
        self::assertEquals($response->getContent(), 'Test test test test');
        self::assertEquals($response->getPhotos(), []);
        self::assertEquals($response->getTitle(), 'Test name');
    }
}

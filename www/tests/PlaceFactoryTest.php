<?php

namespace Tests;

use App\Entity\Country;
use App\Entity\Place;
use App\Factory\PlaceFactory;
use App\Request\Place\CreatePlaceRequest;
use App\Request\Place\UpdatePlaceRequest;
use PHPUnit\Framework\TestCase;

class PlaceFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $createRequest = (new CreatePlaceRequest())
            ->setName('test')
            ->setTags(['place'])
            ->setDescription('test')
            ->setLatitude(23.1)
            ->setLongitude(34.1)
            ->setAddress('Address');

        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $factory = new PlaceFactory();
        $place = $factory->create($createRequest, $country);

        self::assertEquals($place->getName(), 'test');
        self::assertEquals($place->getStatus(), 'created');
        self::assertEquals($place->getCountry(), $country);
        self::assertEquals($place->getLatitude(), 23.1);
        self::assertEquals($place->getLongitude(), 34.1);
        self::assertEquals($place->getAddress(), 'Address');
        self::assertEquals($place->getDescription(), 'test');
    }

    public function testCreateResponse(): void
    {
        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $place = (new Place())
            ->setId(1)
            ->setName('test')
            ->setTags(['place'])
            ->setCountry($country)
            ->setDescription('test')
            ->setLatitude(23.1)
            ->setLongitude(34.1)
            ->setAddress('Address');

        $factory = new PlaceFactory();
        $createResponse = $factory->createResponse($place);

        self::assertEquals($createResponse->getId(), 1);
        self::assertEquals($createResponse->getName(), 'test');
        self::assertEquals($createResponse->getStatus(), 'created');
        self::assertEquals($createResponse->getCountry()->getId(), 1);
        self::assertEquals($createResponse->getLatitude(), 23.1);
        self::assertEquals($createResponse->getLongitude(), 34.1);
        self::assertEquals($createResponse->getAddress(), 'Address');
        self::assertEquals($createResponse->getDescription(), 'test');
    }

    public function testUpdate(): void
    {
        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $updateRequest = (new UpdatePlaceRequest())
            ->setName('test 1')
            ->setTags(['place1', 'place'])
            ->setDescription('test')
            ->setLatitude(23.1)
            ->setLongitude(34.1)
            ->setAddress('Address');

        $place = (new Place())
            ->setId(1)
            ->setName('test')
            ->setTags(['place'])
            ->setCountry($country)
            ->setDescription('test')
            ->setLatitude(23.1)
            ->setLongitude(34.1)
            ->setAddress('Address');

        $factory = new PlaceFactory();
        $updatedPlace = $factory->update($updateRequest, $place);

        self::assertEquals($updatedPlace->getName(), 'test 1');
        self::assertEquals($updatedPlace->getTags(), ['place1', 'place']);
        self::assertEquals($updatedPlace->getStatus(), 'created');
        self::assertEquals($updatedPlace->getCountry(), $country);
        self::assertEquals($updatedPlace->getLatitude(), 23.1);
        self::assertEquals($updatedPlace->getLongitude(), 34.1);
        self::assertEquals($updatedPlace->getAddress(), 'Address');
        self::assertEquals($updatedPlace->getDescription(), 'test');
    }

    public function testUpdateResponse(): void
    {
        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $place = (new Place())
            ->setId(1)
            ->setName('test')
            ->setTags(['place'])
            ->setCountry($country)
            ->setDescription('test')
            ->setLatitude(23.1)
            ->setLongitude(34.1)
            ->setAddress('Address');

        $factory = new PlaceFactory();
        $response = $factory->updateResponse($place);

        self::assertEquals($response->getName(), 'test');
        self::assertEquals($response->getStatus(), 'created');
        self::assertEquals($response->getCountry()->getCode(), 'LT');
        self::assertEquals($response->getLatitude(), 23.1);
        self::assertEquals($response->getLongitude(), 34.1);
        self::assertEquals($response->getAddress(), 'Address');
        self::assertEquals($response->getDescription(), 'test');
    }
}

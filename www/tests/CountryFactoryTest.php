<?php

namespace Tests;

use App\Entity\Country;
use App\Factory\CountryFactory;
use App\Request\Article\CreateCountryRequest;
use App\Request\Article\UpdateCountryRequest;
use PHPUnit\Framework\TestCase;

class CountryFactoryTest extends TestCase
{
    public function testCreate(): void
    {
        $request = (new CreateCountryRequest())
            ->setName('Lithuania')
            ->setCode('LT');

        $factory = new CountryFactory();
        $country = $factory->create($request);

        self::assertEquals($country->getCode(), 'LT');
        self::assertEquals($country->getName(), 'Lithuania');
    }

    public function testCreateResponse(): void
    {
        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $factory = new CountryFactory();
        $response = $factory->createResponse($country);

        self::assertEquals($response->getId(), 1);
        self::assertEquals($response->getCode(), 'LT');
        self::assertEquals($response->getName(), 'Lithuania');
    }

    public function testUpdate(): void
    {
        $request = (new UpdateCountryRequest())
            ->setName('Lietuva')
            ->setCode('LT');

        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $factory = new CountryFactory();
        $updatedCountry = $factory->update($request, $country);

        self::assertEquals($updatedCountry->getCode(), 'LT');
        self::assertEquals($updatedCountry->getName(), 'Lietuva');
    }

    public function testUpdateResponse(): void
    {
        $country = (new Country())
            ->setId(1)
            ->setName('Lithuania')
            ->setCode('LT');

        $factory = new CountryFactory();
        $response = $factory->updateResponse($country);

        self::assertEquals($response->getId(), 1);
        self::assertEquals($response->getCode(), 'LT');
        self::assertEquals($response->getName(), 'Lithuania');
    }
}

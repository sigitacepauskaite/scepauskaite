-- -------------------------------------------------------------
-- TablePlus 3.12.8(368)
--
-- https://tableplus.com/
--
-- Database: project
-- Generation Time: 2021-05-17 23:50:47.4450
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `admin_user`;
CREATE TABLE `admin_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `admin_user_role`;
CREATE TABLE `admin_user_role` (
  `admin_user_id` int NOT NULL,
  `role_id` int NOT NULL,
  PRIMARY KEY (`admin_user_id`,`role_id`),
  KEY `IDX_C41FC3916352511C` (`admin_user_id`),
  KEY `IDX_C41FC391D60322AC` (`role_id`),
  CONSTRAINT `FK_C41FC3916352511C` FOREIGN KEY (`admin_user_id`) REFERENCES `admin_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_C41FC391D60322AC` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_23A0E66A76ED395` (`user_id`),
  CONSTRAINT `FK_23A0E66A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `article_photo`;
CREATE TABLE `article_photo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `article_id` int DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6300F6097294869C` (`article_id`),
  CONSTRAINT `FK_6300F6097294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` int NOT NULL AUTO_INCREMENT,
  `code` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `place`;
CREATE TABLE `place` (
  `id` int NOT NULL AUTO_INCREMENT,
  `country_id` int DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `description` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `tags` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `status` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_741D53CDF92F3E70` (`country_id`),
  CONSTRAINT `FK_741D53CDF92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `place_photo`;
CREATE TABLE `place_photo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `place_id` int DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BDC19F3BDA6A219` (`place_id`),
  CONSTRAINT `FK_BDC19F3BDA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `place_visit`;
CREATE TABLE `place_visit` (
  `id` int NOT NULL AUTO_INCREMENT,
  `place_id` int DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rate` double DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EA08F21ADA6A219` (`place_id`),
  KEY `IDX_EA08F21AA76ED395` (`user_id`),
  CONSTRAINT `FK_EA08F21AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_EA08F21ADA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `role` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_57698A6A57698A6A` (`role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `trip`;
CREATE TABLE `trip` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trip_date` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7656F53BA76ED395` (`user_id`),
  CONSTRAINT `FK_7656F53BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `trip_stop`;
CREATE TABLE `trip_stop` (
  `id` int NOT NULL AUTO_INCREMENT,
  `trip_id` int DEFAULT NULL,
  `place_id` int DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_926E85DDA5BC2E0E` (`trip_id`),
  KEY `IDX_926E85DDDA6A219` (`place_id`),
  CONSTRAINT `FK_926E85DDA5BC2E0E` FOREIGN KEY (`trip_id`) REFERENCES `trip` (`id`),
  CONSTRAINT `FK_926E85DDDA6A219` FOREIGN KEY (`place_id`) REFERENCES `place` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `country_id` int DEFAULT NULL,
  `email` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `username` varchar(35) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  KEY `IDX_8D93D649F92F3E70` (`country_id`),
  CONSTRAINT `FK_8D93D649F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `admin_user` (`id`, `email`, `password`, `first_name`, `last_name`, `salt`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'si.cepauskaite@gmail.com', '$2y$13$by/H7arRX5nE1SH9zmbGoOMAv743czLx3a7bnWXLHIPeZj7/jZWZS', 'Sigita', 'Čepauskaitė', '5a66969359929a764984235a3381f47d', NULL, '2021-05-17 22:55:40', '2021-05-17 22:55:40');

INSERT INTO `admin_user_role` (`admin_user_id`, `role_id`) VALUES
(1, 2);

INSERT INTO `country` (`id`, `code`, `name`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'LT', 'Lietuva', NULL, '2021-05-17 22:55:40', '2021-05-17 22:55:40');

INSERT INTO `place` (`id`, `country_id`, `name`, `address`, `latitude`, `longitude`, `description`, `deleted_at`, `created_at`, `updated_at`, `tags`, `status`) VALUES
(1, 1, 'Šiaulės apžvalgos bokštas', '82432, Lithuania', 55.6455316, 23.4195244, 'Šiaulės apžvalgos bokštas. Labai įdomus, kadangi laiptai sraigtiniai, yra du užlipimai ir susitinki viršuje :) ', NULL, '2021-05-17 19:03:01', '2021-05-17 19:09:09', 'a:3:{i:0;s:16:\"beautiful-nature\";i:1;s:18:\"observation-towers\";i:2;s:7:\"forests\";}', 'created'),
(2, 1, 'Diktariškių dvaras', 'Šaukoto sen., 82435, Lithuania', 55.6530604, 23.4458884, 'Diktariškių dvaras. Šiuo metu parduodamas. Gal kas nori įsigyti ?', NULL, '2021-05-17 19:12:55', '2021-05-17 19:12:55', 'a:2:{i:0;s:7:\"museums\";i:1;s:19:\"abandoned-buildings\";}', 'created'),
(3, 1, 'Žuvinto ežero gamtos takas', 'Žuvinto biosferos rezervatas, 64351, Lithuania', 54.457107, 23.639539, 'Nuostabus žuvinto ežero gamtos takas. Patartinta apsilankyti vakarais, saulei besileidžiant... ', NULL, '2021-05-17 19:19:36', '2021-05-17 19:23:44', 'a:3:{i:0;s:16:\"beautiful-nature\";i:1;s:5:\"lakes\";i:2;s:7:\"beaches\";}', 'created'),
(5, 1, 'Kačėniškės piliakalnis', 'Švenčionių 18101, Lithuania', 55.2208925, 26.2620935, 'Kačėniškės piliakalnis. Puiki vieta daryti iškylą su šeima ir pasimėgauti nuostabiu vaizdu.', NULL, '2021-05-17 19:34:56', '2021-05-17 19:34:56', 'a:2:{i:0;s:16:\"beautiful-nature\";i:1;s:6:\"picnic\";}', 'created'),
(6, 1, 'Viešvilės žuvitakis', '141 95, Viešvilė 74233, Lithuania', 55.0729254, 22.3860179, 'Viešvilės žuvitakis.', NULL, '2021-05-17 19:38:26', '2021-05-17 19:40:16', 'a:2:{i:0;s:16:\"beautiful-nature\";i:1;s:6:\"picnic\";}', 'created'),
(7, 1, 'Jiesios skardis', 'Sąnašos g. 53, Kaunas 46231, Lithuania', 54.8539298, 23.937379, 'Jiesios skardis Kaune. Kas dar neaplankėte, rekomenduoju aplankyti!', NULL, '2021-05-17 19:43:28', '2021-05-17 19:43:28', 'a:3:{i:0;s:9:\"mountains\";i:1;s:16:\"beautiful-nature\";i:2;s:7:\"forests\";}', 'created'),
(8, 1, 'Okupacijų ir laisvės kovų muziejus', 'Aukų g. 2A, Vilnius 01400, Lithuania', 54.6879651, 25.2705956, 'Okupacijų ir laisvės kovų muziejus Vilniuje', NULL, '2021-05-17 19:47:08', '2021-05-17 19:47:08', 'a:2:{i:0;s:7:\"museums\";i:1;s:19:\"abandoned-buildings\";}', 'created'),
(9, 1, 'Nemenčinės piliakalnis', 'Piliakalnis 15154, Lithuania', 54.8608209, 25.4736399, 'Nemenčinės pakraštyje esančio piliakalnio iki šiol nebuvau aplankęs, tačiau visai be reikalo. Šis atokus gamtos kampelis man paliko puikų įspūdį.', NULL, '2021-05-17 19:49:01', '2021-05-17 19:52:34', 'a:3:{i:0;s:16:\"beautiful-nature\";i:1;s:6:\"picnic\";i:2;s:7:\"forests\";}', 'created'),
(10, 1, 'Lyduvėnų tiltas', 'Lyduvėnai 60416, Lithuania', 55.5073262, 23.0823743, 'Lyduvėnų tiltas – ilgiausias ir aukščiausias Lietuvoje tiltas, esantis prie Lyduvėnų. Tiltas pastatytas Radviliškio-Pagėgių geležinkelio ruožo 50,7 km. Tiltas nutiestas per Dubysą ir jos slėnį', NULL, '2021-05-17 19:54:23', '2021-05-17 19:54:23', 'a:2:{i:0;s:16:\"beautiful-nature\";i:1;s:7:\"museums\";}', 'created');

INSERT INTO `place_photo` (`id`, `place_id`, `name`, `url`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'bokstas-60a2beea01fc7.jpg', 'http://api.onthego.local/uploads/bokstas-60a2beea01fc7.jpg', NULL, '2021-05-17 19:07:22', '2021-05-17 19:07:22'),
(2, 1, 'bokstas1-60a2bf59d72d9.jpg', 'http://api.onthego.local/uploads/bokstas1-60a2bf59d72d9.jpg', NULL, '2021-05-17 19:09:13', '2021-05-17 19:09:13'),
(3, 2, 'diktdvar-60a2c039e2ec1.jpg', 'http://api.onthego.local/uploads/diktdvar-60a2c039e2ec1.jpg', NULL, '2021-05-17 19:12:58', '2021-05-17 19:12:58'),
(4, 2, 'diktdavr2-60a2c03a135c8.jpg', 'http://api.onthego.local/uploads/diktdavr2-60a2c03a135c8.jpg', NULL, '2021-05-17 19:12:58', '2021-05-17 19:12:58'),
(5, 3, 'zuvint-60a2c2c3bf551.jpg', 'http://api.onthego.local/uploads/zuvint-60a2c2c3bf551.jpg', NULL, '2021-05-17 19:23:47', '2021-05-17 19:23:47'),
(7, 5, 'kancpil-60a2c5630fcbe.jpg', 'http://api.onthego.local/uploads/kancpil-60a2c5630fcbe.jpg', NULL, '2021-05-17 19:34:59', '2021-05-17 19:34:59'),
(8, 5, 'kancpil1-60a2c563259d8.jpg', 'http://api.onthego.local/uploads/kancpil1-60a2c563259d8.jpg', NULL, '2021-05-17 19:34:59', '2021-05-17 19:34:59'),
(9, 6, 'zuvitak-60a2c6a4117de.jpg', 'http://api.onthego.local/uploads/zuvitak-60a2c6a4117de.jpg', NULL, '2021-05-17 19:40:20', '2021-05-17 19:40:20'),
(10, 7, 'jiesskard-60a2c762b6a09.jpg', 'http://api.onthego.local/uploads/jiesskard-60a2c762b6a09.jpg', NULL, '2021-05-17 19:43:30', '2021-05-17 19:43:30'),
(11, 8, 'okup1-60a2c83ee9bc4.jpg', 'http://api.onthego.local/uploads/okup1-60a2c83ee9bc4.jpg', NULL, '2021-05-17 19:47:11', '2021-05-17 19:47:11'),
(12, 8, 'okup2-60a2c83f0a343.jpg', 'http://api.onthego.local/uploads/okup2-60a2c83f0a343.jpg', NULL, '2021-05-17 19:47:11', '2021-05-17 19:47:11'),
(13, 9, 'nemenc-60a2c9843f4e5.jpg', 'http://api.onthego.local/uploads/nemenc-60a2c9843f4e5.jpg', NULL, '2021-05-17 19:52:36', '2021-05-17 19:52:36'),
(14, 10, 'lyduven-60a2c9f40eb85.jpg', 'http://api.onthego.local/uploads/lyduven-60a2c9f40eb85.jpg', NULL, '2021-05-17 19:54:28', '2021-05-17 19:54:28');

INSERT INTO `role` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'ROLE_USER', '2021-05-17 22:55:40', '2021-05-17 22:55:40'),
(2, 'ROLE_ADMIN', '2021-05-17 22:55:40', '2021-05-17 22:55:40');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

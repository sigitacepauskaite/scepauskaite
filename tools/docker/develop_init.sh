#!/bin/bash

set -e
set -x

PROJECT_ROOT="$(dirname $(dirname $(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)))"

echo "PROJECT ROOT: ${PROJECT_ROOT}"
cd "${PROJECT_ROOT}"

function setPerms {
  mkdir -p $1
  sudo setfacl -R  -m m:rwx -m u:33:rwX -m u:1000:rwX $1
  sudo setfacl -dR -m m:rwx -m u:33:rwX -m u:1000:rwX $1
}

cd ${PROJECT_ROOT}/www/

composer install --no-interaction
php bin/console d:m:m -n
php bin/console d:f:l -n
php bin/console d:d:c -e test --if-not-exists

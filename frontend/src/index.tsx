import React from 'react';
import ReactDOM from 'react-dom';
import "./shared/config/axios.config";
import '@fortawesome/fontawesome-free/css/all.css';
import {Router, Switch, Route} from "react-router-dom";
import {history} from "./shared/config/history";
import App from './app/App';
import AdminApp from "./admin/AdminApp";
import UserApp from "./user/UserApp";

ReactDOM.render(
    <Router history={history}>
            <Switch>
                <Route path={'/admin'}>
                    <AdminApp/>
                </Route>
                <Route path={'/user'}>
                    <UserApp/>
                </Route>
                <Route path={'/'}>
                    <App/>
                </Route>
            </Switch>
    </Router>,
    document.getElementById('root')
);

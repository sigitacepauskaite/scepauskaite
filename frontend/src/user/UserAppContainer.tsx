import React from 'react';
import {Route, Switch, useRouteMatch} from "react-router";
import BlogUserPage from "./pages/Blog";
import TripsPage from "./pages/Trips";
import PlannerPage from "./pages/Planner";
import VisitedPlacesPage from "./pages/VisitedPlaces";
import ProfilePage from "./pages/Profile";
import Spinner from "../shared/components/Spinner";

function UserAppContainer() {
    let match = useRouteMatch();

    return (
        <>
            <Switch>
                <Route exact path={`${match.path}/trips`}>
                    <TripsPage/>
                </Route>
                <Route exact path={`${match.path}/trip-planner`}>
                    <PlannerPage/>
                </Route>
                <Route path={`${match.path}/trip-planner/trips/:id`}>
                    <PlannerPage/>
                </Route>
                <Route exact path={`${match.path}/visited-places`}>
                    <VisitedPlacesPage/>
                </Route>
                <Route exact path={`${match.path}/profile`}>
                    <ProfilePage/>
                </Route>
                <Route exact path={`${match.path}/blog`}>
                    <BlogUserPage/>
                </Route>
            </Switch>
            <Spinner/>
        </>
    );
}

export default UserAppContainer;

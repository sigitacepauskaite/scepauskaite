import React from 'react';
import UserAppContainer from "./UserAppContainer";
import "./assets/scss/index.scss";
import {getAuthenticatedUser} from "../shared/utils/AuthUtils";
import {history} from "../shared/config/history";
import {SpinnerContextProvider} from "../shared/context/SpinnerContext";

function UserApp() {
    const authenticatedUser = getAuthenticatedUser();

    if (!authenticatedUser) {
        history.push("/");
    }

    return (
        <SpinnerContextProvider>
            <UserAppContainer/>
        </SpinnerContextProvider>
    );
}

export default UserApp;

import React from 'react';
import Header from "../../components/Header";
import Navigation from "../../components/Navigation";
import TripForm from "../../components/TripForm";

const PlannerPage = () => {
    return (
        <>
            <Header/>
            <Navigation/>

            <div className="col-md-9 mx-auto">
                <div className="page-content">
                    <TripForm/>
                </div>
            </div>
        </>
    );
};

export default PlannerPage;

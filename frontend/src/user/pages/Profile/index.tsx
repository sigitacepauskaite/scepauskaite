import React, {useEffect, useState} from 'react';
import Header from "../../components/Header";
import Navigation from "../../components/Navigation";
import {
    UserRequest,
    UserRequestData,
} from "../../../shared/models/request/UserModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {getAuthenticatedUser} from "../../../shared/utils/AuthUtils";
import {removeTrip} from "../../../app/actions/TripActions";
import {getUserAction, updateUserAction} from "../../../admin/actions/UserActions";
import {getCurrentUser, updateUserAppAction} from "../../../app/actions/UserActions";
import {UserModel} from "../../../shared/models/response/User/UserModel";

const ProfilePage = () => {
    const [error, setError] = useState('');
    const [formData, setFormData] = useState(UserRequestData);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const authenticatedUser = getAuthenticatedUser();

    useEffect(() => {
        setFormData({
            ...formData,
            email: authenticatedUser ? authenticatedUser.email : '',
            firstName: authenticatedUser ? authenticatedUser.firstName : '',
            lastName: authenticatedUser ? authenticatedUser.lastName : '',
        })
    }, []);

    const onInputChange = (event: any) => {
        if (event.target.name === 'firstName') {
            setFormData({
                ...formData,
                firstName: event.target.value,
            })
        }

        if (event.target.name === 'lastName') {
            setFormData({
                ...formData,
                lastName: event.target.value,
            })
        }

        if (event.target.name === 'password') {
            setFormData({
                ...formData,
                password: event.target.value,
            })
        }

        if (event.target.name === 'repeatedPassword') {
            setFormData({
                ...formData,
                repeatedPassword: event.target.value,
            })
        }
    };

    const onProfileUpdateSubmit = (event: any) => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });
        if (!authenticatedUser) {
            return;
        }

        updateUserAppAction(authenticatedUser.id, formData)
            .then((response:any) => {
                getCurrentUser()
                    .then((response: any) => {
                        const responseData: UserModel = response.data;
                        localStorage.setItem('authorizedUser', JSON.stringify(responseData));

                        window.location.reload();
                    })
                    .catch((error) => {
                        setError(error.response.data.message);

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        })
                    });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
        ;
    };

    const onPasswordUpdateSubmit = (event: any) => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });
        // @ts-ignore
        updateUserAppAction(authenticatedUser.id, formData)
            .then((response:any) => {
                window.location.reload();

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
        ;
    };

    return (
        <>
            <Header/>
            <Navigation/>
            <div className="col-md-9 mx-auto mt-4">
                <div className="row">
                    <div className="col-md-8">
                        <div className="card">
                            <div className="card-body">
                                <h2 className="fs-18">Profilio informacijos atnaujinimas</h2>
                                <hr/>
                                <div className="content">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <label>Vartotojo vardas:</label>
                                                <input className="form-control" type="text" name="username" value={formData.email} disabled/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-6">
                                                <label>Vardas:</label>
                                                <input className="form-control" type="text" name="firstName" value={formData.firstName} onChange={onInputChange}/>
                                            </div>
                                            <div className="col-md-6">
                                                <label>Pavardė:</label>
                                                <input className="form-control" type="text" name="lastName"  value={formData.lastName} onChange={onInputChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-3">
                                                <button className="btn btn-primary btn-block mt-3" onClick={onProfileUpdateSubmit}>
                                                    Atnaujinti
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="card">
                            <div className="card-body">
                                <h2 className="fs-18">Slaptažodžio atnaujinimas</h2>
                                <hr/>
                                <div className="content">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <label>Slaptažodis:</label>
                                                <input className="form-control" type="password" name="password" onChange={onInputChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <label>Pakartokite slaptažodį:</label>
                                                <input className="form-control" type="password" name="repeatedPassword" onChange={onInputChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <button className="btn btn-primary btn-block mt-3" onClick={onPasswordUpdateSubmit}>
                                                    Atnaujinti
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default ProfilePage;

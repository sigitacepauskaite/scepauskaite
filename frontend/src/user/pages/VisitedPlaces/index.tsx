import React, {useEffect, useState} from 'react';
import Header from "../../components/Header";
import Navigation from "../../components/Navigation";
import Map from "../../../shared/components/Map/Map";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import SpinnerImg from "../../../shared/assets/img/spinner.svg";
import {Place, PlaceRate, placeRateData} from "../../../shared/assets/models/Place";
import {addPlaceVisitAppAction, getPlacesAppAction, getVisitedPlacesAppAction} from "../../../app/actions/PlaceActions";

const VisitedPlacesPage = () => {
        const [visitedPlaces, setVisitedPlaces] = useState([] as Place[]);
        const [error, setError] = useState('');
        const [placeRate, setPlaceRate] = useState(placeRateData);
        const [foundPlaces, setFoundPlaces] = useState([] as Place[]);
        const {spinnerData, setSpinnerData} = useSpinnerContext();
        const [addressSpinnerState, setAddressSpinnerState] = useState(false);

        useEffect(() => {
            setSpinnerData({
                ...spinnerData,
                active: true,
            });

            getVisitedPlacesAppAction()
                .then((response) => {
                    setVisitedPlaces(response.data);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    })
                })
                .catch((httpError) => {
                    setError(httpError.response.data.message);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    })
                })
        }, [])
        let typingTimer: any;
        const doneTypingInterval: number = 500;
        const onAddressInputKeyUp = (event: any) => {
            clearTimeout(typingTimer);
            typingTimer = setTimeout(onAddressInputChange, doneTypingInterval, event);
        };
        const onAddressInputKeyDown = (event: any) => {
            const value = event.target.value;

            if (value && value.length >= 3) {
                setAddressSpinnerState(true);
            }

            clearTimeout(typingTimer);
        };
        const onAddressInputChange = (event: any) => {
            const value = event.target.value;

            if (value && value.length > 3) {
                getPlacesAppAction(0, 20, [], [], value)
                    .then((response) => {
                        setFoundPlaces(response.data);
                        setAddressSpinnerState(false);
                    })
                    .catch((error) => {
                        setError(error.response.data.message);
                    })
            }
        };
        const onTyping = (event: any): any => {
            if (event.target.name === 'address') {
                setPlaceRate({
                    ...placeRate,
                    place: {
                        ...placeRate.place,
                        name: event.target.value,
                    }
                });
            }

            if (event.target.name === 'note') {
                setPlaceRate({
                    ...placeRate,
                    note: event.target.value,
                });
            }
        };
        const onAddressPlaceSelect = (place: Place): any => {
            setPlaceRate({
                ...placeRate,
                place : place
            });
            setFoundPlaces([]);
        };

        const onPlaceRate = (event: any) => {
            setPlaceRate({
                ...placeRate,
                rate: event.target.value
            });
        };

        const onPlaceVisitedSubmit = () => {
            if (!placeRate.place || !placeRate.rate || placeRate.rate === 0) {
                setError('Privaloma pasirinkti norimą vietą bei nurodyti jos įvertinimą');
            }

            addPlaceVisitAppAction(placeRate)
                .then((response) => {
                    setPlaceRate(placeRateData);

                    window.location.reload();

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });

                })
                .catch((error) => {
                    setError(error.response.data.message);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });

                    return;
                })
        };

        return (
            <>
                <Header/>
                <Navigation/>
                <div className="row">
                    <div className="col-md-10 mx-auto mt-4">
                        <div className="add-visited-place">
                            <div className="card">
                                <div className="card-body">
                                    {
                                        error.length > 0
                                            ? <div className="alert alert-danger">
                                                {error}
                                            </div>
                                            : null
                                    }
                                    <div className="row">
                                        <div className="col-md-7">
                                            <div className="address-search-input-block">
                                                <input type="text"
                                                       className="form-control"
                                                       name="address"
                                                       placeholder="Vietos pavadinimas"
                                                       autoComplete="off"
                                                       onChange={onTyping}
                                                       onKeyUp={onAddressInputKeyUp}
                                                       onKeyDown={onAddressInputKeyDown}
                                                       value={placeRate.place.name}
                                                />
                                                <div
                                                    className={"address-search-spinner " + (addressSpinnerState === true ? '' : 'd-none')}>
                                                    <img src={SpinnerImg}/>
                                                </div>
                                            </div>
                                            {
                                                foundPlaces.length > 0
                                                    ? <><div className="address-autocomplete-results">
                                                        <ul>
                                                            {
                                                                foundPlaces.map((element, key) => (
                                                                    <li key={key}
                                                                        onClick={onAddressPlaceSelect.bind(this, element)}>{element.name}</li>
                                                                ))
                                                            }
                                                        </ul>
                                                    </div><br/></>
                                                    : null
                                            }
                                            <br/>
                                            <textarea
                                                className="form-control"
                                                name="note"
                                                placeholder="Atsiliepimas"
                                                onChange={onTyping}
                                                value={placeRate.note}
                                                rows={5}
                                            />
                                            <button className="btn btn-primary mt-4 btn-round" onClick={onPlaceVisitedSubmit}>
                                                Žymėti kaip aplankytą
                                            </button>
                                        </div>
                                        <div className="col-md-5 vertical-center">
                                            <div className="rate">
                                                <input type="radio" id="star5" name="rate" onChange={onPlaceRate}
                                                       value="5"/>
                                                <label htmlFor="star5" title="text">5 stars</label>
                                                <input type="radio" id="star4" name="rate" onChange={onPlaceRate}
                                                       value="4"/>
                                                <label htmlFor="star4" title="text">4 stars</label>
                                                <input type="radio" id="star3" name="rate" onChange={onPlaceRate}
                                                       value="3"/>
                                                <label htmlFor="star3" title="text">3 stars</label>
                                                <input type="radio" id="star2" name="rate" onChange={onPlaceRate}
                                                       value="2"/>
                                                <label htmlFor="star2" title="text">2 stars</label>
                                                <input type="radio" id="star1" name="rate" onChange={onPlaceRate}
                                                       value="1"/>
                                                <label htmlFor="star1" title="text">1 star</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row mt-4">
                    <div className="col-md-10 mx-auto">
                        <div className="mt-1" style={{'height': '800px'}}>
                            <Map selectorId="visited-places-map" places={visitedPlaces} zoom={9}
                                 showPlaceInfoBox={true}></Map>
                        </div>
                    </div>
                </div>
            </>
        );
    }
;

export default VisitedPlacesPage;

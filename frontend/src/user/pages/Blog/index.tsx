import React, {useEffect, useState} from 'react';
import Header from "../../components/Header";
import Navigation from "../../components/Navigation";
import {ArticleModel, ArticleRequest, defaultArticleRequest,} from "../../../shared/models/request/ArticleModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {history} from "../../../shared/config/history";
import {getArticlesAppAction, removeArticle} from "../../../app/actions/ArticleActions";
import {getAuthenticatedUser} from "../../../shared/utils/AuthUtils";
import ArticleForm from "./ArticleForm";
import {removeTrip} from "../../../app/actions/TripActions";

const BlogUserPage = () => {
    const [formData, setFormData] = useState(defaultArticleRequest as ArticleRequest);
    const [articles, setArticles] = useState([] as ArticleModel[]);
    const [formVisible, setFormVisible] = useState(false);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const authenticatedUser = getAuthenticatedUser();

    if (authenticatedUser === null) {
        history.push("/");
    }

    useEffect(() => {

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        getArticlesAppAction(0, 30, authenticatedUser.id)
            .then((response) => {
                const responseItems = response.data;

                setArticles(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                })
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    const showForm = () => {
        setFormVisible(true);
    };

    const hideForm = () => {
        setFormVisible(false);
    };

    const editArticle = (article: ArticleModel) => {
        setFormData({
            ...formData,
            id: article.id,
            name: article.name,
            content: article.content,
            photos: article.photos,
        })
        showForm();
        window.scrollTo(0, 120);
    };

    const handleRemove = (id: number) => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        removeArticle(id)
            .then((response:any) => {
                window.location.reload();

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    };

    return (
        <>
            <Header/>
            <Navigation/>
            <div className="col-md-9 mx-auto mt-5">
                <div className={"toolbar mb-5" + (formVisible ? ' d-none' : '')}>
                    <button className="btn btn-primary col-md-2 float-right btn-round" onClick={showForm}>
                        Naujas
                    </button>
                    <div className="clearfix"/>
                </div>

                <ArticleForm formVisible={formVisible} hideForm={hideForm} formData={formData}
                             setFormData={setFormData}/>
                <div className="articles-list">
                    {
                        articles.map((article, key: any) => (
                            <div className="article-container" key={key}>
                                <div className="card">
                                    <div className="card-body">
                                        <div className="row" style={{minHeight: '260px'}}>
                                            <div className="col-md-4">
                                                <img className="img img-thumbnail img-responsive"
                                                     src={
                                                         article.photos.length > 0
                                                             ? article.photos[0].url
                                                             : 'https://medmarinetravel.com/wp-content/plugins/aheto/assets/images/placeholder.jpg'
                                                     }
                                                     alt={article.name}/>
                                            </div>
                                            <div className="col-md-8 position-relative">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h2 className="fs-20">{article.name}</h2>
                                                    </div>
                                                    <div className="col-md-6">
                                                        <button className="btn btn-sm btn-danger px-4 mx-1 float-right btn-round"
                                                                style={{'bottom': 0}}
                                                                onClick={handleRemove.bind(this, article.id)}
                                                        >
                                                            Pašalinti
                                                        </button>
                                                        <button className="btn btn-sm btn-primary px-4 mx-1 float-right btn-round"
                                                                style={{'bottom': 0}} onClick={editArticle.bind(this, article)}>
                                                            Redaguoti
                                                        </button>
                                                    </div>
                                                </div>

                                                <hr/>
                                                <p>
                                                    {article.content}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
            </div>
        </>
    );
};

export default BlogUserPage;

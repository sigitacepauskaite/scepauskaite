import React, {useEffect, useState} from 'react';
import {ArticleRequest, defaultArticleRequest} from "../../../shared/models/request/ArticleModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {useDropzone} from "react-dropzone";
import {
    createArticleAppAction,
    deleteArticlePhotosAppAction,
    updateArticleAppAction,
    uploadArticlePhotosAppAction
} from "../../../app/actions/ArticleActions";
import {PlacePhoto} from "../../../shared/assets/models/Place";

type Props = {
    formVisible: boolean,
    hideForm: any,
    formData: ArticleRequest,
    setFormData: any,
};

const ArticleForm = ({formVisible, hideForm, formData, setFormData}: Props) => {
        const [error, setError] = useState('');
        const {spinnerData, setSpinnerData} = useSpinnerContext();
        const [files, setFiles] = useState([] as File[]);
        const {getRootProps, getInputProps} = useDropzone({
            accept: 'image/*',
            onDrop: acceptedFiles => {
                // const uploadedFiles: DropZoneFile[] = acceptedFiles.map(file => Object.assign(file, {
                //     preview: URL.createObjectURL(file)
                // }));

                setFiles([...files, ...acceptedFiles]);
            }
        });

        const handlePhotoRemove = (photo: PlacePhoto) => {
            setSpinnerData({
                ...spinnerData,
                active: true,
            });

            deleteArticlePhotosAppAction(formData.id, photo.id)
                .then((response: any) => {
                    setSpinnerData({
                        ...spinnerData,
                        active: true,
                    })

                    window.location.reload();

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                })
                .catch((error) => {
                    setError(error.response.data.message);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });

                    return;
                });
            ;
        };

        const thumbs = files.map(file => (
            <div className="thumb" key={file.name}>
                <div className="thumb-inner">
                    <img
                        src={URL.createObjectURL(file)}
                    />
                </div>
            </div>
        ));

        let existingThumbs = null;
        if (formData && formData.photos) {
            existingThumbs = formData.photos.map(photo => (
                <div className="thumb" key={photo.id}>
                    <div className="thumb-inner">
                        {/*// @ts-ignore*/}
                        <div className="remove-photo" onClick={handlePhotoRemove.bind(this, photo)}>
                            Pašalinti
                        </div>
                        <img src={photo.url}/>
                    </div>
                </div>
            ));
        }

        useEffect(() => () => {
            files.forEach(file => URL.revokeObjectURL(URL.createObjectURL(file)));
        }, [files]);


        const onInputChange = (event: any) => {
            if (event.target.name === 'title') {
                setFormData({
                    ...formData,
                    name: event.target.value
                });
            }

            if (event.target.name === 'content') {
                setFormData({
                    ...formData,
                    content: event.target.value
                });
            }
        };
        const onArticleFormSubmit = (event: any) => {
            event.preventDefault();

            if (formData.name.length === 0) {
                setError('Privaloma nurodyti pavadinimą');

                return;
            }

            if (formData.content.length === 0) {
                setError('Privaloma nurodyti straipsnio turinį');

                return;
            }

            setSpinnerData({
                ...spinnerData,
                active: true,
            });

            if (formData.id) {
                updateArticleAppAction(formData.id, formData)
                    .then((response: any) => {
                        const responseData: any = response.data;
                        setFormData(defaultArticleRequest);
                        if (files.length > 0) {
                            uploadArticlePhotosAppAction(files, responseData.id)
                                .then((response) => {
                                    setFiles([]);
                                    hideForm();
                                });
                            window.location.reload();

                            setSpinnerData({
                                ...spinnerData,
                                active: false,
                            });
                        } else {
                            hideForm();
                            window.location.reload();

                            setSpinnerData({
                                ...spinnerData,
                                active: false,
                            });
                        }
                    })
                    .catch((error) => {
                        setError(error.response.data.message);

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });

                        return;
                    });
            } else {
                createArticleAppAction(formData)
                    .then((response: any) => {
                        const responseData: any = response.data;
                        setFormData(defaultArticleRequest);
                        uploadArticlePhotosAppAction(files, responseData.id)
                            .then((response) => {
                                setFiles([]);
                                hideForm();
                                window.location.reload();

                                setSpinnerData({
                                    ...spinnerData,
                                    active: false,
                                });
                            });
                    })
                    .catch((error) => {
                        setError(error.response.data.message);

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });

                        return;
                    });
            }

        };

        return (
            <>
                <div className={"add-article-form card p-5" + (formVisible ? '' : ' d-none')}>
                    {
                        error.length > 0
                            ? <>
                                <div className="alert alert-danger">
                                    {error}
                                </div>
                                <br/>
                            </>
                            : null
                    }
                    <form action="/" method="POST" onSubmit={onArticleFormSubmit}>
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <section className="dropzone-container">
                                                    <div {...getRootProps({className: 'dropzone vertical-center'})}
                                                         style={{'height': '240px'}}>
                                                        <input {...getInputProps()} />
                                                        <p>Tempkite nuotraukas į šį laukelį, arba paspauskite ir
                                                            pasirinkite
                                                            nuotraukas</p>
                                                    </div>
                                                    <aside>
                                                        {thumbs}
                                                        {existingThumbs}
                                                    </aside>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-9">
                                    <label>Pavadinimas</label>
                                    <input type="text" name="title" className="form-control" required
                                           value={formData.name}
                                           onChange={onInputChange}/>
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-12">
                                    <label>Turinys</label>
                                    <textarea name="content" className="form-control" required
                                              value={formData.content}
                                              onChange={onInputChange} rows={15}/>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div className="form-group">
                            <div className="row">
                                <div className="col-md-8 mx-auto">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <button type="submit" className="btn btn-primary btn-block btn-round">
                                                Išsaugoti
                                            </button>
                                        </div>
                                        <div className="col-md-6">
                                            <button className="btn btn-dark btn-block btn-round" onClick={hideForm}>
                                                Atšaukti
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </>
        );
    }
;

export default ArticleForm;

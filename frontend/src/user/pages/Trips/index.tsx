import React, {useEffect, useState} from 'react';
import Header from "../../components/Header";
import Navigation from "../../components/Navigation";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {getAuthenticatedUser} from "../../../shared/utils/AuthUtils";
import {history} from "../../../shared/config/history";
import {TripModel} from "../../../shared/models/main/Map";
import {getTrips, removeTrip, removeTripStop} from "../../../app/actions/TripActions";
import Map from "../../../shared/components/Map/Map";
import {Link} from 'react-router-dom';

const TripsPage = () => {
    const [trips, setTrips] = useState([] as TripModel[]);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const authenticatedUser = getAuthenticatedUser();

    if (authenticatedUser === null) {
        history.push("/");
    }

    useEffect(() => {

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        getTrips(0, 20)
            .then((response) => {
                const responseItems: TripModel[] = response.data;

                responseItems.forEach((trip) => {
                    trip.places = [];

                    trip.tripStops.forEach((tripStop) => {
                        trip.places = [...trip.places, tripStop.place]
                    });
                });

                setTrips(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                })
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    const handleRemoveTrip = (id: number) => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        removeTrip(id)
            .then((response:any) => {
                window.location.reload();

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
        ;
    };

    return (
        <>
            <Header/>
            <Navigation/>
            <div className="col-md-9 mx-auto mt-5">
                <div className="trips-list">
                    <div className="row">
                        {
                            trips.map((trip, key: any) => (
                                <div className="col-md-12 trip-container" key={key}>
                                    <div className="card">
                                        <div className="card-body">
                                            <div className="row" style={{minHeight: '160px'}}>
                                                <div className="col-md-12 position-relative">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <h2 className="fs-20">{trip.name}</h2>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <button className="btn btn-sm btn-danger px-4 mx-1 float-right btn-round"
                                                                    style={{'bottom': 0}}
                                                                    onClick={handleRemoveTrip.bind(this, trip.id)}
                                                            >
                                                                Pašalinti
                                                            </button>
                                                            <Link to={"/user/trip-planner/trips/" + trip.id}
                                                                  className="btn btn-sm btn-primary px-4 mx-1 float-right btn-round"
                                                                  style={{'bottom': 0}}>
                                                                Redaguoti
                                                            </Link>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div className="content">
                                                        <p>
                                                            <b>Kelionės data: </b>
                                                            {trip.tripDate.slice(0, 10)}
                                                        </p>
                                                        <p>
                                                            <b>Maršrutas: </b>
                                                            {
                                                                trip.places.map((place, key:any) => (
                                                                    <div key={key}>
                                                                        {place.name}
                                                                        <br/>
                                                                    </div>
                                                                ))
                                                            }
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    );
};

export default TripsPage;

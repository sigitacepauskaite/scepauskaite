import React from 'react';
import logo from '../../../shared/assets/img/logo.svg';
import {Link} from "react-router-dom";
import {history} from "../../../shared/config/history";
import "./index.scss";

const Header = () => {

    const handleLogout = () => {
        localStorage.removeItem('authorizationToken');
        localStorage.removeItem('authorizedUser');
        history.push("/");
    };

    return (
        <header className="user-app">
            <div className="col-md-9 mx-auto">
                <div className="row">
                    <div className="col-md-4">
                        <div className="logo">
                            <a href="/">
                                <img src={logo} alt=""/>
                            </a>
                        </div>
                    </div>
                    <div className="col-md-8">
                        <div className="header-buttons">
                            <div className="row h-100">
                                <div className="col-md-5 vertical-center">
                                    <Link className="btn btn-block btn-primary" to="/">
                                        Vietų paieška
                                    </Link>
                                </div>
                                <div className="col-md-7">
                                    <div className="row h-100">
                                        <div className="col-md-7 vertical-center">
                                            <Link className="btn btn-block btn-primary btn-round" to='/user/profile'>Atnaujinti profilį</Link>
                                        </div>
                                        <div className="col-md-5 vertical-center">
                                            <button className="btn btn-block btn-dark btn-round" onClick={handleLogout}>
                                                Atsijungti
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;

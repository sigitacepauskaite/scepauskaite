import React, {useEffect, useState} from 'react';
import Map from "../../../shared/components/Map/Map";
import "./index.scss";
import {TripModel, TripModelData, TripStopModel} from "../../../shared/models/main/Map";
import {Place} from "../../../shared/assets/models/Place";
import {getPlacesAppAction} from "../../../app/actions/PlaceActions";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {addTripStop, createTrip, getTrip, removeTripStop, updateTrip} from "../../../app/actions/TripActions";
import {history} from "../../../shared/config/history";
import {useParams} from 'react-router-dom';

const TripForm = () => {
    const [error, setError] = useState('');
    const [allowStopAdd, setAllowStopAdd] = useState(false);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const [activeStop, setActiveStop] = useState({} as TripStopModel);
    const [placesSearchResults, setPlacesSearchResults] = useState([] as Place[]);
    const [trip, setTrip] = useState(TripModelData as TripModel);
    const {id}: any = useParams();

    useEffect(() => {

        if (id) {
            setSpinnerData({
                ...spinnerData,
                active: true,
            });

            getTrip(id)
                .then((response) => {
                    const responseData : TripModel = response.data;

                    let index = 1;
                    responseData.tripStops.forEach((loopItem) => {
                       loopItem.orderNumber = index;

                        index++;
                    });

                    setTrip(responseData);
                    setAllowStopAdd(true);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    })
                })
                .catch(() => {
                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                })
        }
    }, []);

    const onTripInfoFill = (event: any) => {
        if (event.target.name === 'trip_title') {
            setTrip({
                ...trip,
                name: event.target.value
            });
        }

        if (event.target.name === 'trip_date') {
            setTrip({
                ...trip,
                tripDate: event.target.value
            });
        }
    };

    const onTripStopRemove = (trip: TripModel, tripStop: TripStopModel) => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        removeTripStop(tripStop.id)
            .then((response) => {
                window.location.reload();

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    };

    let typingTimer: any;
    const doneTypingInterval: number = 500;
    const onAddressInputKeyUp = (stop: TripStopModel, event: any) => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(onSearchInputChange, doneTypingInterval, stop, event);
    };
    const onAddressInputKeyDown = (stop: TripStopModel, event: any) => {
        const value = event.target.value;

        clearTimeout(typingTimer);
    };

    const onSearchInputChange = (stop: TripStopModel, event: any) => {
        const editingStop = trip.tripStops.findIndex((searchableTrip) => searchableTrip.orderNumber === stop.orderNumber);
        const searchValue: string = event.target.value;
        trip.tripStops[editingStop].place.name = searchValue;

        setTrip({
            ...trip,
            tripStops: trip.tripStops,
        });

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getPlacesAppAction(0, 20, [], [], searchValue)
            .then((response) => {
                setPlacesSearchResults(response.data);
                setActiveStop(stop);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    };

    const onAddressTyping = (stop: TripStopModel, event: any): any => {
        const editingStop = trip.tripStops.findIndex((searchableTrip) => searchableTrip.orderNumber === stop.orderNumber);
        const searchValue: string = event.target.value;
        trip.tripStops[editingStop].place.name = searchValue;

        setTrip({
            ...trip,
            tripStops: trip.tripStops,
        });
    };

    const onSearchResultClick = (place: Place, clickedStop: TripStopModel, event: any): any => {
        setPlacesSearchResults([]);

        const editingStop:number = trip.tripStops.findIndex((searchableTrip) => searchableTrip.orderNumber === clickedStop.orderNumber);
        trip.tripStops[editingStop].place.name = place.name ? place.name : clickedStop.place.name;
        trip.tripStops[editingStop].place = place;
        trip.tripStops[editingStop].placeId = place.id ? place.id : 0;

        setTrip({
            ...trip,
            tripStops: trip.tripStops,
        });

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        addTripStop(trip.id, place.id)
            .then((response) => {
                trip.tripStops[editingStop].id = response.data.id;

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    };

    const handleTripSave = () => {
        if (trip.name.length === 0) {
            setError('Pirmiausia įveskite kelionės pavadinimą');

            return;
        }

        if (trip.tripDate.length === 0) {
            setError('Pirmiausia nurodykite kelionės datą');

            return;
        }

        setError('');

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        if (id) {
            updateTrip(id, trip)
                .then((response) => {
                    setAllowStopAdd(true);

                    history.push('/user/trip-planner/trips/'+response.data.id);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                })
                .catch((error) => {
                    setError(error.response.data.message);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });

                    return;
                });
        } else {
            createTrip(trip)
                .then((response) => {
                    setAllowStopAdd(true);

                    history.push('/user/trip-planner/trips/'+response.data.id);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                })
                .catch((error) => {
                    setError(error.response.data.message);

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });

                    return;
                });
        }
    };

    const onTripStopAddClick = () => {
        const newStop: TripStopModel = {
            id: 0,
            placeId: 0,
            place: {
                latitude: '0',
                longitude: '0',
                address: '',
            },
            orderNumber: trip.tripStops.length + 1
        };

        if (!trip.id && id) {
            setSpinnerData({
                ...spinnerData,
                active: true,
            });

            getTrip(id)
                .then((response) => {
                    const responseData : TripModel = response.data;

                    setTrip({
                        ...trip,
                        id: responseData.id,
                        tripStops: [...trip.tripStops, newStop]
                    });

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                })
                .catch(() => {
                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                })
        } else {
            setTrip({
                ...trip,
                tripStops: [...trip.tripStops, newStop]
            });
        }
    };

    return (
        <div className="row">
            <div className="col-12">
                <div className="trip-form">
                    {
                        error.length > 0
                            ? <div className="row mt-3">
                                <div className="col-12">
                                    <div className="alert alert-danger">
                                        {error}
                                    </div>
                                </div>
                            </div>
                            : null
                    }
                    <div className="row mt-3">
                        <div className="col-12">
                            <div className="card">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <div className="form-group">
                                                <label>Kelionės pavadinimas</label>
                                                <input id="trip-name" type="text" name="trip_title"
                                                       placeholder="Pavadinimas"
                                                       className="form-control"
                                                       value={trip.name}
                                                       onChange={onTripInfoFill}/>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <label>Kelionės data</label>
                                            <input id="trip-date"
                                                   type="date"
                                                   name="trip_date"
                                                   className="form-control"
                                                   value={trip.tripDate}
                                                   onChange={onTripInfoFill}/>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-12">
                                            <button className="btn btn-primary col-md-3 mt-3 btn-round" onClick={handleTripSave}>
                                                Išsaugoti
                                            </button>
                                            <div className="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-12">
                            <div className="trip-stops">
                                <div className="list">
                                    {
                                        trip.tripStops.map((stop: TripStopModel, key: any) => (
                                                <div className="trip-stop" key={key}>
                                                    <div className="row h-100">
                                                        <div className="col-12 h-100">
                                                            <div className="card h-100">
                                                                <div className="card-body h-100">
                                                                    <div className="order-number">
                                                                        #{stop.orderNumber}
                                                                    </div>
                                                                    <button className="btn btn-danger btn-md position-absolute" style={{bottom: '35px', right: '30px', zIndex:20}} onClick={onTripStopRemove.bind(this, trip, stop)}>
                                                                        Pašalinti
                                                                    </button>
                                                                    <Map
                                                                        selectorId={"trip-form-map-" + (Math.random().toString(36).substr(2, 5))}
                                                                        places={[stop.place]}
                                                                        showPlaceInfoBox={true}
                                                                    >
                                                                        <div className="place-search-box">
                                                                            <input type="text"
                                                                                   name="search-place-name"
                                                                                   autoComplete="off"
                                                                                   placeholder="Ieškoti norimos aplankyti vietos"
                                                                                   value={stop.place.name}
                                                                                   onKeyUp={onAddressInputKeyUp.bind(this, stop)}
                                                                                   onKeyDown={onAddressInputKeyDown.bind(this, stop)}
                                                                                   onChange={onAddressTyping.bind(this, stop)}
                                                                                   disabled={stop.id ? true : false}
                                                                            />
                                                                            <div
                                                                                className={"search-place-results " + (stop.orderNumber === activeStop.orderNumber ? "" : "d-none")}>
                                                                                <ul>
                                                                                    {
                                                                                        placesSearchResults.map((place, key) => (
                                                                                                <li key={key}
                                                                                                    onClick={onSearchResultClick.bind(this, place, stop)}>
                                                                                                    {place.id} - {place.name}
                                                                                                </li>
                                                                                            )
                                                                                        )
                                                                                    }
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </Map>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        )
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                    {
                        allowStopAdd
                            ? <div className="row">
                                <div className="col-12 col-md-7 mx-auto">
                                    <button className="btn btn-block btn-add-stop"
                                            onClick={onTripStopAddClick}
                                    >
                                        Pridėti kelionės sustojimą
                                    </button>
                                </div>
                            </div>
                            : null
                    }
                </div>
            </div>
        </div>
    );
};

export default TripForm;

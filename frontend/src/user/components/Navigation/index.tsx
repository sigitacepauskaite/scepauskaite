import React, {useEffect} from 'react';
import "./index.scss";
import logo from '../../../shared/assets/img/logo.svg';
import {Link} from "react-router-dom";

const Navigation = () => {

    useEffect(() => {
        document.querySelectorAll('nav a').forEach((link) => {
            if (link.getAttribute('href') === window.location.pathname) {
                link.classList.add('active');
            }
        });
    }, []);

    return (
        <nav className="navigation">
            <div className="col-md-9 mx-auto">
                <ul>
                    <li>
                        <Link to="/user/trips" className="text-dark">
                            <i className="icon-bike"></i>
                            Suplanuotos kelionės
                        </Link>
                    </li>
                    <li>
                        <Link to="/user/trip-planner" className="text-dark">
                            <i className="icon-telescope"></i>
                            Kelionės planuotojas
                        </Link>
                    </li>
                    <li>
                        <Link to="/user/visited-places" className="text-dark">
                            <i className="icon-map"></i>
                            Aplankytos vietos
                        </Link>
                    </li>
                    <li>
                        <Link to="/user/blog" className="text-dark">
                            <i className="icon-browser"></i>
                            Kelionių tinklaraštis
                        </Link>
                    </li>
                </ul>
            </div>
        </nav>
    );
};

export default Navigation;

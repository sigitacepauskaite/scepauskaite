import React from 'react';
import AdminAppContainer from "./AdminAppContainer";
import "./assets/scss/index.scss";
import {SpinnerContextProvider} from "../shared/context/SpinnerContext";
import {getAuthenticatedAdminUser, getAuthenticatedUser} from "../shared/utils/AuthUtils";
import {history} from "../shared/config/history";

function AdminApp() {
    const authenticatedAdminUser = getAuthenticatedAdminUser();

    if (!authenticatedAdminUser) {
        history.push("/admin/login");
    }

    return (
        <SpinnerContextProvider>
            <AdminAppContainer/>
        </SpinnerContextProvider>
    );
}

export default AdminApp;

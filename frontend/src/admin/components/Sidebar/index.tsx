import React from 'react';
import "./index.scss";
import profileImage from "../../assets/img/avatar/1.jpg"
import {getAuthenticatedAdminUser} from "../../../shared/utils/AuthUtils";
import {history} from "../../../shared/config/history";
import {Link} from 'react-router-dom';

function Sidebar() {
    const authenticatedAdminUser = getAuthenticatedAdminUser();

    if (authenticatedAdminUser === null) {
        history.push("/");

        return null;
    }

    const handleLogout = () => {
        localStorage.removeItem('adminAuthorizationToken');
        localStorage.removeItem('authorizedAdminUser');
        history.push("/admin/login");
    };

    return (
        <aside className="sidebar sidebar-icons-right sidebar-icons-boxed sidebar-expand-lg">
            <nav className="sidebar-navigation ps-container ps-theme-default">
                <div className="sidebar-profile">
                    <div className="avatar-view">
                        <img className="img-responsive img-thumbnail" src={profileImage}/>
                    </div>
                    <div className="profile-info">
                        <h4>{authenticatedAdminUser.firstName} {authenticatedAdminUser.lastName}</h4>
                        <p>Administratorius</p>
                        <b onClick={handleLogout}>Atsijungti</b>
                    </div>
                </div>

                <ul className="menu menu-lg menu-bordery">
                    <li className={"menu-item "+(window.location.pathname.includes('/admin/places') ? 'active' : '')}>
                        <Link className="menu-link text-decoration-none" to="/admin/places">
                            <span className="icon">
                                <i className="icon-map fw-700 fs-20"/>
                            </span>
                            <span className="title">Vietovės</span>
                        </Link>
                    </li>


                    <li className={"menu-item "+(window.location.pathname.includes('/admin/users') ? 'active' : '')}>
                        <Link className="menu-link text-decoration-none" to="/admin/users">
                            <span className="icon">
                                <i className="icon-profile-male fw-700 fs-20"/>
                            </span>
                            <span className="title">Vartotojai</span>
                        </Link>
                    </li>
                    <li className={"menu-item "+(window.location.pathname.includes('/admin/articles') ? 'active' : '')}>
                        <Link className="menu-link text-decoration-none" to="/admin/articles">
                            <span className="icon">
                                <i className="icon-streetsign fw-700 fs-20"/>
                            </span>
                            <span className="title">Kelionių istorijos</span>
                        </Link>
                    </li>
                    <li className={"menu-item "+(window.location.pathname.includes('/admin/settings') ? 'active' : '')}>
                        <Link className="menu-link text-decoration-none" to="/admin/settings/countries">
                            <span className="icon">
                                <i className="icon-gears fw-700 fs-20"/>
                            </span>
                            <span className="title">Nustatymai</span>
                        </Link>
                    </li>
                </ul>
            </nav>
        </aside>
    );
}

export default Sidebar;

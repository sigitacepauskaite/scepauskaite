import React from 'react';
import "./index.scss";

type Props = {
    title: string,
    children: any,
};

function Card({title, children}:Props) {
    return (
        <div className="card ">
            <h4 className="card-title">
                <strong>{title}</strong>
            </h4>
            <div className="card-body">
                {children}
            </div>
        </div>
    );
}

export default Card;

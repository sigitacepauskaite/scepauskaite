import React from 'react';
import "./index.scss";
import {useRouteMatch} from "react-router";
import {Link} from 'react-router-dom';

type Props = {
    title: string,
};

function PageContentTop({title}: Props) {
    let match = useRouteMatch();

    return (
        <header className="header bg-ui-general">
            <div className="header-info">
                <div className="row">
                    <div className="col-12 col-md-10">
                        <h1 className="header-title">
                            <strong>{title}</strong>
                        </h1>
                    </div>
                    <div className="col-12 col-md-2">
                        <a className="btn btn-primary btn-admin btn-block" href={`${match.path}/new`}>
                            Naujas
                        </a>
                    </div>
                </div>
            </div>
            {
                match.path === '/admin/settings/countries'
                    ? <div className="header-action">
                        <nav className="nav">
                            <Link className="nav-link active" to="/admin/settings/countries">
                                Šalių nustatymai
                            </Link>
                        </nav>
                    </div>
                    : null
            }
        </header>
    );
}

export default PageContentTop;

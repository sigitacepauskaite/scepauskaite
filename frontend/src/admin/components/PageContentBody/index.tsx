import React from 'react';
import "./index.scss";

type Props = {
    children: any
}

function PageContentBody({children}:Props) {
    return (
        <div className="main-content">
            {children}
        </div>
    );
}

export default PageContentBody;

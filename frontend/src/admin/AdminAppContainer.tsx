import React from 'react';
import {Route, Switch, useRouteMatch} from "react-router";
import Sidebar from "./components/Sidebar";
import PlacesPage from "./pages/Places";
import BlogPage from "./pages/Articles";
import UsersPage from "./pages/Users";
import CountriesPage from "./pages/Countries";
import LoginPage from "./pages/Auth/LoginPage";
import Spinner from "../shared/components/Spinner";

function AdminAppContainer() {
    let match = useRouteMatch();

    return (
        <>
            <Switch>
                <Route exact path={`${match.path}/login`}>
                    <LoginPage/>
                </Route>
                <Route path={`${match.path}`}>
                    <Sidebar/>
                    <div className="main-container">
                        <Switch>
                            <Route path={`${match.path}/places`}>
                                <PlacesPage/>
                            </Route>
                            <Route path={`${match.path}/articles`}>
                                <BlogPage/>
                            </Route>
                            <Route path={`${match.path}/users`}>
                                <UsersPage/>
                            </Route>
                            <Route path={`${match.path}/settings`}>
                                <CountriesPage/>
                            </Route>
                        </Switch>
                    </div>
                </Route>
            </Switch>
            <Spinner/>
        </>
    );
}

export default AdminAppContainer;

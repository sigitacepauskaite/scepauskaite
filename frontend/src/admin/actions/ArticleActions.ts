import axios from "axios";
import {ArticleRequest} from "../../shared/models/request/ArticleModels";
import {adminLoginAction} from "./UserActions";
import {getAuthenticatedAdminUserToken} from "../../shared/utils/AuthUtils";

export const getArticleAction = (id: number) => {
    return axios.get(
        'api/admin/articles/'+id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const getArticlesAction = (from: number, count: number) => {
    return axios.get(
        'api/admin/articles',
        {
            params: {
                start: from,
                limit: count,
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const createArticleAction = (articleRequest: ArticleRequest) => {
    return axios.post(
        'api/admin/articles',
        articleRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const updateArticleAction = (id: number, articleRequest: ArticleRequest) => {
    return axios.put(
        'api/admin/articles/'+id,
        articleRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const deleteArticleAction = (id: number) => {
    return axios.delete(
        'api/admin/articles/'+id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const uploadArticlePhotosAction = (files: File[], placeId: number) => {
    const formData : FormData = new FormData();

    files.forEach((file) => {
        formData.append('images[]', file)
    });

    return axios.post(
        'api/admin/articles/'+placeId+'/photos',
        formData,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                'Content-type' : 'multipart/form-data',
                'authorization': 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const deleteArticlePhotosAction = (articleId: number, photoId: number) => {
    return axios.delete(
        'api/admin/articles/'+articleId+'/photos/'+photoId,
        {
            headers: {
                'Content-type' : 'multipart/form-data',
                'authorization': 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};




import axios from "axios";
import {AdminLoginFormData, UserRequest} from "../../shared/models/request/UserModels";
import {getAuthenticatedAdminUserToken} from "../../shared/utils/AuthUtils";

export const adminLoginAction = (adminLoginData: AdminLoginFormData) => {
    return axios.post('api/admin/login_check', adminLoginData);
};

export const getCurrentAdminUser = (token: string) => {
    return axios.get('/api/admin/admin-users/current', {
        headers: {
            authorization: 'Bearer ' + token,
        }
    });
};

export const getUserAction = (id: number) => {
    return axios.get(
        'api/admin/users/' + id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const getUsersAction = (from: number, count: number) => {
    return axios.get(
        'api/admin/users',
        {
            params: {
                start: from,
                limit: count,
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const createUserAction = (userRequest: UserRequest) => {
    return axios.post(
        'api/admin/users/countries/LT',
        userRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const updateUserAction = (id: number, userRequest: UserRequest) => {
    return axios.put(
        'api/admin/users/' + id,
        userRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const deleteUserAction = (id: number) => {
    return axios.delete(
        'api/admin/users/' + id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};




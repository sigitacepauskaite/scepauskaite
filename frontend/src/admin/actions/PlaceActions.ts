import axios from "axios";
import {PlaceRequest} from "../../shared/models/request/PlaceModels";
import {getAuthenticatedAdminUserToken} from "../../shared/utils/AuthUtils";

export const getPlaceAction = (id: number) => {
    return axios.get(
        'api/admin/places/'+id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const getPlacesAction = (from: number, count: number) => {
    return axios.get(
        'api/admin/places',
        {
            params: {
                start: from,
                limit: count,
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const createPlaceAction = (placeRequest: PlaceRequest) => {
    return axios.post(
        'api/admin/places/countries/LT',
        placeRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const updatePlaceAction = (id: number, placeRequest: PlaceRequest) => {
    return axios.put(
        'api/admin/places/'+id,
        placeRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const deletePlaceAction = (id: number) => {
    return axios.delete(
        'api/admin/places/'+id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const uploadPlacePhotosAction = (files: File[], placeId: number) => {
    const formData : FormData = new FormData();

    files.forEach((file) => {
        formData.append('images[]', file)
    });

    return axios.post(
        'api/admin/places/'+placeId+'/photos',
        formData,
        {
            headers: {
                'Content-type' : 'multipart/form-data',
                'authorization': 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const deletePlacePhotosAction = (placeId: number, photoId: number) => {
    return axios.delete(
        'api/admin/places/'+placeId+'/photos/'+photoId,
        {
            headers: {
                'Content-type' : 'multipart/form-data',
                'authorization': 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};


import axios from "axios";
import {ArticleRequest} from "../../shared/models/request/ArticleModels";
import {CountryRequest} from "../../shared/models/request/CountryModels";
import {adminLoginAction} from "./UserActions";
import {getAuthenticatedAdminUserToken} from "../../shared/utils/AuthUtils";

export const getCountryAction = (id: number) => {
    return axios.get(
        'api/admin/countries/'+id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const getCountriesAction = (from: number, count: number) => {
    return axios.get(
        'api/admin/countries',
        {
            params: {
                start: from,
                limit: count,
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const createCountryAction = (countryRequest: CountryRequest) => {
    return axios.post(
        'api/admin/countries',
        countryRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const updateCountryAction = (id: number, countryRequest: CountryRequest) => {
    return axios.put(
        'api/admin/countries/'+id,
        countryRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};

export const deleteCountryAction = (id: number) => {
    return axios.delete(
        'api/admin/countries/'+id,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedAdminUserToken(),
            }
        }
    );
};




import React, {useEffect, useState} from 'react';
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {UserModel} from "../../../shared/models/response/User/UserModel";
import {getUsersAction} from "../../actions/UserActions";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {Link} from 'react-router-dom';


function List() {
    const [users, setUsers] = useState([] as UserModel[]);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getUsersAction(0, 30)
            .then((response) => {
                const responseItems = response.data;

                setUsers(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    return (
        <>
            <PageContentTop title="Vartotojai"/>
            <PageContentBody>
                <Card title="Vartotojų sąrašas">
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Vartotojo vardas</th>
                                <th>Vardas/Pavardė</th>
                                <th>Veiksmai</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                users.map((user: UserModel) => (
                                    <tr>
                                        <td>{user.id}</td>
                                        <td>{user.email}</td>
                                        <td>{user.firstName} {user.lastName}</td>
                                        <td>
                                            <Link to={"/admin/users/"+user.id+"/edit"} className="pr-3">
                                                <i className="icon-pencil fw-700 fs-14 btn btn-sm btn-outline-info"></i>
                                            </Link>
                                            <Link to={"/admin/users/"+user.id+"/show"}>
                                                <i className="icon-search fw-700 fs-14 btn btn-sm btn-outline-success"></i>
                                            </Link>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default List;

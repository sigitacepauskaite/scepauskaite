import React, {useState} from 'react';
import {UserRequest, UserRequestData} from "../../../shared/models/request/UserModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {createUserAction} from "../../actions/UserActions";
import {history} from "../../../shared/config/history";

function Create() {
    const [formData, setFormData] = useState(UserRequestData as UserRequest);
    const [error, setError] = useState('');
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    const onInputChange = (event: any) => {
        if (event.target.name === 'email') {
            setFormData({
                ...formData,
                email: event.target.value
            });
        }

        if (event.target.name === 'firstName') {
            setFormData({
                ...formData,
                firstName: event.target.value
            });
        }

        if (event.target.name === 'lastName') {
            setFormData({
                ...formData,
                lastName: event.target.value
            });
        }

        if (event.target.name === 'password') {
            setFormData({
                ...formData,
                password: event.target.value
            });
        }

        if (event.target.name === 'repeatedPassword') {
            setFormData({
                ...formData,
                repeatedPassword: event.target.value
            });
        }
    };
    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.firstName.length === 0) {
            setError('Privaloma nurodyti vardą');

            return;
        }

        if (formData.lastName.length === 0) {
            setError('Privaloma nurodyti pavardę');

            return;
        }

        if (formData.email.length === 0) {
            setError('Privaloma nurodyti vartotojo prisijungimo vardą');

            return;
        }

        if (formData.password.length === 0) {
            setError('Privaloma nurodyti prisijungimo slaptažodį');

            return;
        }

        if (formData.repeatedPassword.length === 0) {
            setError('Privaloma pakartotinai nurodyti prisijungimo slaptažodį');

            return;
        }

        if (formData.password !== formData.repeatedPassword) {
            setError('Įvesti slaptažodžiai nesutampa');

            return;
        }

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        createUserAction(formData)
            .then((response: any) => {
                const responseData: any = response.data;

                history.push("/admin/users");

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            });
    };

    return (
        <>
            <PageContentTop title="Vartotojai"/>
            <PageContentBody>
                <div className="row">
                    <div className="col-md-7">
                        <Card title="Naujas vartotojas">
                            {
                                error.length > 0
                                    ? <div className="alert alert-danger">
                                        {error}
                                    </div>
                                    : null
                            }
                            <form action="/" method="POST" onSubmit={onFormSubmit}>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <label>Vardas</label>
                                            <input type="text" name="firstName" className="form-control" required
                                                   onChange={onInputChange}/>
                                        </div>
                                        <div className="col-md-6">
                                            <label>Pavardė</label>
                                            <input type="text" name="lastName" className="form-control" required
                                                   onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label>Vartotojo vardas</label>
                                            <input type="text" name="email" className="form-control" required
                                                   onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-6">
                                            <label>Slaptažodis</label>
                                            <input type="password" name="password" className="form-control" required
                                                   onChange={onInputChange}/>
                                        </div>
                                        <div className="col-md-6">
                                            <label>Pakartokite slaptažodį</label>
                                            <input type="password" name="repeatedPassword" className="form-control"
                                                   required
                                                   onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-6 mx-auto">
                                            <button className="btn btn-primary btn-block">
                                                Išsaugoti
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </Card>
                    </div>
                </div>
            </PageContentBody>
        </>
    );
}

export default Create;

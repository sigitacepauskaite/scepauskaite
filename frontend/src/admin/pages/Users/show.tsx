import React, {useEffect, useState} from 'react';
import {UserModel, UserModelData} from "../../../shared/models/response/User/UserModel";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {getUserAction} from "../../actions/UserActions";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {useParams} from "react-router-dom";

function Show() {
    const {id}: any = useParams();
    const [user, setUser] = useState(UserModelData as UserModel);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getUserAction(id)
            .then((response) => {
                const responseItems = response.data;

                setUser(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    return (
        <>
            <PageContentTop title="Vartotojai"/>
            <PageContentBody>
                <Card title={"Vartotojo informacija"}>
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>Informacijos vienetas</th>
                                <th>Reikšmė</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>#ID</td>
                                    <td>{user.id}</td>
                                </tr>
                                <tr>
                                    <td>Vartotojo vardas/el. paštas</td>
                                    <td>{user.email}</td>
                                </tr>
                                <tr>
                                    <td>Vardas/Pavardė</td>
                                    <td>{user.firstName} {user.lastName}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default Show;

import React from 'react';
import {useRouteMatch} from "react-router";
import {Route, Switch} from "react-router-dom";
import List from "../Users/list";
import Create from "../Users/create";
import Update from "../Users/update";
import Show from "../Users/show";

function UsersPage() {
    let match = useRouteMatch();

    return (
        <Switch>
            <Route exact path={`${match.path}`}>
                <List/>
            </Route>
            <Route exact path={`${match.path}/new`}>
                <Create/>
            </Route>
            <Route exact path={`${match.path}/:id/edit`}>
                <Update/>
            </Route>
            <Route exact path={`${match.path}/:id/show`}>
                <Show/>
            </Route>
        </Switch>
    );
}

export default UsersPage;

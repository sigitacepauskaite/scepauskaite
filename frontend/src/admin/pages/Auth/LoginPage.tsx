import React, {useState} from 'react';
import logo from '../../../shared/assets/img/logo.svg';
import {
    AdminLoginFormData,
    defaultAdminLoginData,
    defaultLoginData,
    LoginFormData
} from "../../../shared/models/request/UserModels";
import {getCurrentUser, loginAction} from "../../../app/actions/UserActions";
import {LoginResponse, UserModel} from "../../../shared/models/response/User/UserModel";
import {history} from "../../../shared/config/history";
import {adminLoginAction, getCurrentAdminUser} from "../../actions/UserActions";
import Spinner from "../../../shared/components/Spinner";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";

function LoginPage() {
    const [error, setError] = useState('');
    const [formData, setFormData] = useState(defaultAdminLoginData as AdminLoginFormData);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    const onInputChange = (event: any) => {
        if (event.target.name === 'username') {
            setFormData({
                ...formData,
                username: event.target.value
            });
        }

        if (event.target.name === 'password') {
            setFormData({
                ...formData,
                password: event.target.value
            });
        }
    };

    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.username.length === 0) {
            setError('Privaloma nurodyti vartotojo prisijungimo vardą');

            return;
        }

        if (formData.password.length === 0) {
            setError('Privaloma nurodyti prisijungimo slaptažodį');

            return;
        }

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        adminLoginAction(formData)
            .then((response) => {
                const responseData: LoginResponse = response.data;
                localStorage.setItem('adminAuthorizationToken', responseData.token);

                getCurrentAdminUser(responseData.token)
                    .then((response) => {
                        const responseData: UserModel = response.data;
                        localStorage.setItem('authorizedAdminUser', JSON.stringify(responseData));

                        history.push("/admin/places");

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });
                    })
            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            });
    };

    return (
        <div className="row no-gutters min-h-fullscreen bg-white login-screen">
            <div className="col-md-6 col-lg-7 col-xl-8 d-none d-md-block bg-img login-page-bg">

                <div className="row h-100 pl-50">
                    <div className="col-md-10 col-lg-8 align-self-center ml-auto mr-auto ">
                        <div className="logo p-20 logo-block">
                            <img className="img img-responsive d-block mr-auto ml-auto pt-2" height="60px" src={logo}/>
                        </div>
                    </div>
                </div>
            </div>
            <div className="col-md-6 col-lg-5 col-xl-4 align-self-center">
                <div className="px-80 py-30">
                    <h4>Prisijungimas</h4>
                    <p>
                        <small>Prisijunkite prie savo paskyros</small>
                    </p>
                    <br/>

                    {
                        error.length > 0
                            ? <div className="alert alert-danger">
                                {error}
                            </div>
                            : null
                    }

                    <form name="login" method="post" action="/" className="form-type-material" autoComplete="off" onSubmit={onFormSubmit}>


                        <div className="form-group">
                            <input type="text" id="login_username" name="username" required
                                   placeholder="Prisijungimo vardas" className="form-control" onChange={onInputChange}/>
                        </div>
                        <div className="form-group">
                            <input type="password" id="login_password" name="password" required
                                   placeholder="Slaptažodis" className="form-control" onChange={onInputChange}/>
                        </div>
                        <div className="form-group">
                            <button type="submit" id="login_submit"
                                    className="btn btn-bold btn-block btn-primary">Prisijunkite
                            </button>
                        </div>
                        <input type="hidden" id="login__csrf_token" name="login[_csrf_token]"
                               value="-J6N8YOgZ4iuTUCtI0u4VYVOc-xo_RPCAXfGt69vjTg"/>
                    </form>

                </div>
            </div>
        </div>
    );
}

export default LoginPage;

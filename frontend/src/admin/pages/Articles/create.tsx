import React, {useEffect, useState} from 'react';
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {history} from "../../../shared/config/history";
import {ArticleRequest, defaultArticleRequest} from "../../../shared/models/request/ArticleModels";
import {createArticleAction, uploadArticlePhotosAction} from "../../actions/ArticleActions";
import {useDropzone} from "react-dropzone";
import {uploadPlacePhotosAction} from "../../actions/PlaceActions";

function Create() {
    const [formData, setFormData] = useState(defaultArticleRequest as ArticleRequest);
    const [error, setError] = useState('');
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const [files, setFiles] = useState([] as File[]);
    const {getRootProps, getInputProps} = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            setFiles([...files, ...acceptedFiles]);
        }
    });

    const thumbs = files.map(file => (
        <div className="thumb" key={file.name}>
            <div className="thumb-inner">
                <img
                    src={URL.createObjectURL(file)}
                />
            </div>
        </div>
    ));

    useEffect(() => () => {
        files.forEach(file => URL.revokeObjectURL(URL.createObjectURL(file)));
    }, [files]);

    const onInputChange = (event: any) => {
        if (event.target.name === 'title') {
            setFormData({
                ...formData,
                name: event.target.value
            });
        }

        if (event.target.name === 'content') {
            setFormData({
                ...formData,
                content: event.target.value
            });
        }
    };
    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.name.length === 0) {
            setError('Privaloma nurodyti pavadinimą');

            return;
        }

        if (formData.content.length === 0) {
            setError('Privaloma nurodyti straipsnio turinį');

            return;
        }

        if (files.length === 0) {
            setError('Privaloma įkelti bent vieną nuotrauką');

            return;
        }

        if (files.length > 3) {
            setError('Maksimalus leistinas nuotraukų skaičius - 3');

            return;
        }

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        createArticleAction(formData)
            .then((response: any) => {
                const responseData: any = response.data;

                uploadArticlePhotosAction(files, responseData.id)
                    .then((response) => {
                        setFiles([]);

                        history.push("/admin/articles");

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });
                    });
            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    };

    return (
        <>
            <PageContentTop title="Kelionių istorijos"/>
            <PageContentBody>
                <div className="row">
                    <div className="col-md-7">
                        <Card title="Naujas straipsnis">
                            {
                                error.length > 0
                                    ? <div className="alert alert-danger">
                                        {error}
                                    </div>
                                    : null
                            }
                            <form action="/" method="POST" onSubmit={onFormSubmit}>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label>Pavadinimas</label>
                                            <input type="text" name="title" className="form-control" required
                                                   onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <section className="dropzone-container">
                                                <div {...getRootProps({className: 'dropzone'})}>
                                                    <input {...getInputProps()} />
                                                    <p>Tempkite nuotraukas į šį laukelį, arba paspauskite ir
                                                        pasirinkite
                                                        nuotraukas</p>
                                                </div>
                                                <aside>
                                                    {thumbs}
                                                </aside>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label>Turinys</label>
                                            <textarea name="content" className="form-control" required
                                                      onChange={onInputChange} rows={20}/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-6 mx-auto">
                                            <button className="btn btn-primary btn-block">
                                                Išsaugoti
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </Card>
                    </div>
                </div>
            </PageContentBody>
        </>
    );
}

export default Create;

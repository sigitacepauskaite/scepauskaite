import React from 'react';
import {useRouteMatch} from "react-router";
import {Route, Switch} from "react-router-dom";
import List from "../Articles/list";
import Create from "../Articles/create";
import Update from "../Articles/update";
import Show from "../Articles/show";

function BlogPage() {
    let match = useRouteMatch();

    return (
        <Switch>
            <Route exact path={`${match.path}`}>
                <List/>
            </Route>
            <Route exact path={`${match.path}/new`}>
                <Create/>
            </Route>
            <Route exact path={`${match.path}/:id/edit`}>
                <Update/>
            </Route>
            <Route exact path={`${match.path}/:id/show`}>
                <Show/>
            </Route>
        </Switch>
    );
}

export default BlogPage;

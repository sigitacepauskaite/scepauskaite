import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {history} from "../../../shared/config/history";
import {
    deleteArticlePhotosAction,
    getArticleAction,
    updateArticleAction,
    uploadArticlePhotosAction
} from "../../actions/ArticleActions";
import {
    ArticleModel,
    ArticleModelData,
    ArticleRequest,
    defaultArticleRequest
} from "../../../shared/models/request/ArticleModels";
import {useDropzone} from "react-dropzone";
import {PlacePhoto} from "../../../shared/assets/models/Place";
import {uploadPlacePhotosAction} from "../../actions/PlaceActions";

type Props = {};

function Update({}: Props) {
    const {id}: any = useParams();
    const [article, setArticle] = useState(ArticleModelData as ArticleModel);
    const [formData, setFormData] = useState(defaultArticleRequest as ArticleRequest);
    const [error, setError] = useState('');
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const [files, setFiles] = useState([] as File[]);
    const {getRootProps, getInputProps} = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            setFiles([...files, ...acceptedFiles]);
        }
    });

    const handlePhotoRemove = (article: ArticleModel, photo: PlacePhoto) => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        deleteArticlePhotosAction(article.id, photo.id)
            .then((response:any) => {
                setSpinnerData({
                    ...spinnerData,
                    active: true,
                });

                getArticleAction(id)
                    .then((response: any) => {
                        const responseData: ArticleModel = response.data;
                        setFormData({...formData, ...responseData});
                        setArticle(responseData);

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });
                    })
                    .catch((error) => {
                        setError(error.response.data.message);

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        })

                        return;
                    })
            })
        ;
    };

    const thumbs = files.map(file => (
        <div className="thumb" key={file.name}>
            <div className="thumb-inner">
                <img
                    src={URL.createObjectURL(file)}
                />
            </div>
        </div>
    ));

    let existingThumbs = null;
    if (article && article.photos) {
        existingThumbs = article.photos.map(photo => (
            <div className="thumb" key={photo.id}>
                <div className="thumb-inner">
                    {/*// @ts-ignore*/}
                    <div className="remove-photo" onClick={handlePhotoRemove.bind(this, article, photo)}>
                        Pašalinti
                    </div>
                    <img src={photo.url}/>
                </div>
            </div>
        ));
    }

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getArticleAction(id)
            .then((response: any) => {
                const responseData: ArticleModel = response.data;
                setFormData({...formData, ...responseData});
                setArticle(responseData);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    }, []);

    const onInputChange = (event: any) => {
        if (event.target.name === 'title') {
            setFormData({
                ...formData,
                name: event.target.value
            });
        }

        if (event.target.name === 'content') {
            setFormData({
                ...formData,
                content: event.target.value
            });
        }
    };
    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.name.length === 0) {
            setError('Privaloma nurodyti pavadinimą');

            return;
        }

        if (formData.content.length === 0) {
            setError('Privaloma nurodyti straipsnio turinį');

            return;
        }

        if (files.length > 3) {
            setError('Maksimalus leistinas nuotraukų skaičius - 3');

            return;
        }

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        updateArticleAction(id, formData)
            .then((response: any) => {
                const responseData: any = response.data;

                if (files.length > 0) {
                    uploadArticlePhotosAction(files, responseData.id)
                        .then((response) => {
                            setFiles([]);

                            history.push("/admin/articles");

                            setSpinnerData({
                                ...spinnerData,
                                active: false,
                            });
                        });
                } else {
                    history.push("/admin/plaarticlesces");

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                }
            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    };

    return (
        <>
            <PageContentTop title="Kelionių istorijos"/>
            <PageContentBody>
                <div className="row">
                    <div className="col-md-7">
                        <Card title="Straipsnio atnaujinimas">
                            {
                                error.length > 0
                                    ? <div className="alert alert-danger">
                                        {error}
                                    </div>
                                    : null
                            }
                            <form action="/" method="POST" onSubmit={onFormSubmit}>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label>Pavadinimas</label>
                                            <input type="text" name="title" className="form-control" required
                                                   value={formData.name} onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <section className="dropzone-container">
                                                <div {...getRootProps({className: 'dropzone'})}>
                                                    <input {...getInputProps()} />
                                                    <p>Tempkite nuotraukas į šį laukelį, arba paspauskite ir
                                                        pasirinkite
                                                        nuotraukas</p>
                                                </div>
                                                <aside>
                                                    {thumbs}
                                                    {existingThumbs}
                                                </aside>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-12">
                                            <label>Turinys</label>
                                            <textarea name="content" className="form-control" rows={20} required
                                                      value={formData.content} onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-6 mx-auto">
                                            <button className="btn btn-primary btn-block">
                                                Išsaugoti
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </Card>
                    </div>
                </div>
            </PageContentBody>
        </>
    );
}

export default Update;

import React, {useEffect, useState} from 'react';
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {useParams} from "react-router-dom";
import {getArticleAction} from "../../actions/ArticleActions";
import {ArticleModel, ArticleModelData} from "../../../shared/models/request/ArticleModels";

function Show() {
    const {id}: any = useParams();
    const [article, setArticle] = useState(ArticleModelData as ArticleModel);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getArticleAction(id)
            .then((response) => {
                const responseItems = response.data;

                setArticle(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                })
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    let existingThumbs = null;
    if (article && article.photos) {
        existingThumbs = article.photos.map(photo => (
            <div className="thumb" key={photo.id}>
                <div className="thumb-inner">
                    <img src={photo.url}/>
                </div>
            </div>
        ));
    }

    return (
        <>
            <PageContentTop title="Kelionių istorijos"/>
            <PageContentBody>
                <div className="row">
                    <div className="col-md-6">
                        <Card title={"Straipsnio informacija"}>
                            <div className="table-responsive">
                                <table className="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Informacijos vienetas</th>
                                        <th>Reikšmė</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>#ID</td>
                                        <td>{article.id}</td>
                                    </tr>
                                    <tr>
                                        <td>Pavadinimas</td>
                                        <td>{article.name}</td>
                                    </tr>
                                    <tr>
                                        <td>Turinys</td>
                                        <td>{article.content}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="dropzone-container">
                                {existingThumbs}
                            </div>
                        </Card>
                    </div>
                </div>
            </PageContentBody>
        </>
    );
}

export default Show;

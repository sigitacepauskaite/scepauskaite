import React, {useEffect, useState} from 'react';
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {Link} from 'react-router-dom';
import {ArticleModel, ArticleRequest, defaultArticleRequest} from "../../../shared/models/request/ArticleModels";
import {getArticlesAction} from "../../actions/ArticleActions";


function List() {
    const [articles, setArticles] = useState([] as ArticleModel[]);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getArticlesAction(0, 30)
            .then((response) => {
                const responseItems = response.data;

                setArticles(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                })
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    return (
        <>
            <PageContentTop title="Kelionių istorijos"/>
            <PageContentBody>
                <Card title="Kelionių istorijos">
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Pavadinimas</th>
                                <th>Straipsnio ištrauka</th>
                                <th>Veiksmai</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                articles.map((article: ArticleModel, key:any) => (
                                    <tr key={key}>
                                        <td>{article.id}</td>
                                        <td>{article.name}</td>
                                        <td>{article.content.slice(0, 100)}</td>
                                        <td>
                                            <Link to={"/admin/articles/"+article.id+"/edit"} className="pr-3">
                                                <i className="icon-pencil fw-700 fs-14 btn btn-sm btn-outline-info"></i>
                                            </Link>
                                            <Link to={"/admin/articles/"+article.id+"/show"}>
                                                <i className="icon-search fw-700 fs-14 btn btn-sm btn-outline-success"></i>
                                            </Link>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default List;

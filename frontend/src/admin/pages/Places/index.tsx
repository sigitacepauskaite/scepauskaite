import React from 'react';
import {Route, Switch} from "react-router-dom";
import {useRouteMatch} from "react-router";
import Create from "./create";
import Update from "./update";
import List from "./list";
import Show from "./show";

function PlacesPage() {
    let match = useRouteMatch();

    return (
        <Switch>
            <Route exact path={`${match.path}`}>
                <List/>
            </Route>
            <Route exact path={`${match.path}/new`}>
                <Create/>
            </Route>
            <Route exact path={`${match.path}/:id/edit`}>
                <Update/>
            </Route>
            <Route exact path={`${match.path}/:id/show`}>
                <Show/>
            </Route>
        </Switch>
    );
}

export default PlacesPage;

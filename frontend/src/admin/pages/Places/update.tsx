import React, {useEffect, useState} from 'react';
import {Place, PlacePhoto} from "../../../shared/assets/models/Place";
import {defaultPlaceRequest, PlaceRequest} from "../../../shared/models/request/PlaceModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {useDropzone} from "react-dropzone";
import {getPlaceDetails, getPlacesAutocomplete} from "../../../shared/actions/GoogleApiActions";
import {
    createPlaceAction,
    deletePlacePhotosAction,
    getPlaceAction, updatePlaceAction,
    uploadPlacePhotosAction
} from "../../actions/PlaceActions";
import {history} from "../../../shared/config/history";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import SpinnerImg from "../../../shared/assets/img/spinner.svg";
import {joinClasses} from "../../../shared/utils/ClassUtils";
import Select from "react-select";
import {availableActivities} from "../../../shared/data/PlaceData";
import Map from "../../../shared/components/Map/Map";
import {useParams} from "react-router-dom";

type PlacesAutocompleteItem = {
    name: string,
    placeId: string,
};

function Update() {
    const {id}: any = useParams();
    const [place, setPlace] = useState(null as Place | null);
    const [addressSpinnerState, setAddressSpinnerState] = useState(false);
    const [currentPlace, setCurrentPlace] = useState(null as Place | null);
    const [formError, setFormError] = useState('');
    const [addressesAutocompleteItems, setAddressesAutocompleteItems] = useState([] as PlacesAutocompleteItem[]);
    const [formData, setFormData] = useState(defaultPlaceRequest as PlaceRequest);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const [files, setFiles] = useState([] as File[]);
    const {getRootProps, getInputProps} = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            setFiles([...files, ...acceptedFiles]);
        }
    });
    const getAvailableActivitiesByValues = (): any => {
        let results:any = [];


        formData.tags.forEach((value) => {
            const foundItem: any = availableActivities.find((row: any) => row.value === value);
            results = [...results, foundItem]
        });

        return results;
    };
    const handlePhotoRemove = (place: Place, photo: PlacePhoto) => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        deletePlacePhotosAction(place.id, photo.id)
            .then((response) => {
                setSpinnerData({
                    ...spinnerData,
                    active: true,
                });

                getPlaceAction(id)
                    .then((response: any) => {
                        const responseData: Place = response.data;
                        setFormData({...formData, ...responseData});
                        setPlace(responseData);

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });

                    })
                    .catch((error) => {
                        setFormError(error.response.data.message);

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });

                        return;
                    })
            })
        ;
    };

    const thumbs = files.map(file => (
        <div className="thumb" key={file.name}>
            <div className="thumb-inner">
                <img
                    src={URL.createObjectURL(file)}
                />
            </div>
        </div>
    ));

    let existingThumbs = null;
    if (place && place.photos) {
        existingThumbs = place.photos.map(photo => (
            <div className="thumb" key={photo.id}>
                <div className="thumb-inner">
                    {/*// @ts-ignore*/}
                    <div className="remove-photo" onClick={handlePhotoRemove.bind(this, place, photo)}>
                        Pašalinti
                    </div>
                    <img src={photo.url}
                    />
                </div>
            </div>
        ));
    }

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getPlaceAction(id)
            .then((response: any) => {
                const responseData: Place = response.data;
                setFormData({...formData, ...responseData});
                setPlace(responseData)
                setCurrentPlace(responseData);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

            })
            .catch((error) => {
                setFormError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    }, []);
    useEffect(() => () => {
        files.forEach(file => URL.revokeObjectURL(URL.createObjectURL(file)));
    }, [files]);

    let typingTimer: any;
    const doneTypingInterval: number = 500;
    const onTagsInputChange = (selectedValues: any) => {
        const results: string[] = selectedValues.map((selectedValue: any) => {
            return selectedValue.value;
        });
        setFormData({
            ...formData,
            tags: results
        });
    };
    const onInputChange = (event: any) => {
        if (event.target.name === 'name') {
            setFormData({
                ...formData,
                name: event.target.value
            });
        }

        if (event.target.name === 'address') {
            setFormData({
                ...formData,
                address: event.target.value
            });
        }

        if (event.target.name === 'latitude') {
            setFormData({
                ...formData,
                latitude: event.target.value
            });
        }

        if (event.target.name === 'longitude') {
            setFormData({
                ...formData,
                longitude: event.target.value
            });
        }

        if (event.target.name === 'description') {
            setFormData({
                ...formData,
                description: event.target.value
            });
        }

        if (files.length > 3) {
            setFormError('Maksimalus leistinas nuotraukų skaičius - 3');

            return;
        }
    };
    const onAddressInputKeyUp = (event: any) => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(onAddressInputChange, doneTypingInterval, event);
    };
    const onAddressInputKeyDown = (event: any) => {
        const value = event.target.value;

        if (value && value.length > 3) {
            setAddressSpinnerState(true);
        }

        clearTimeout(typingTimer);
    };
    const onAddressInputChange = (event: any) => {
        const value = event.target.value;

        if (value && value.length > 3) {
            getPlacesAutocomplete(value)
                .then((response) => {
                    setAddressesAutocompleteItems(response.data);
                    setAddressSpinnerState(false);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    };
    const onAddressPlaceSelect = (placeDetails: PlacesAutocompleteItem): any => {
        getPlaceDetails(placeDetails.placeId)
            .then((response) => {
                // @ts-ignore
                document.getElementById('js-latitude-input').value = Number(response.data.latitude);
                // @ts-ignore
                document.getElementById('js-longitude-input').value = Number(response.data.longitude);

                setCurrentPlace(place);

                setFormData({
                    ...formData,
                    address: response.data.name,
                    latitude: response.data.latitude,
                    longitude: response.data.longitude,
                });
                setAddressesAutocompleteItems([]);
            })
            .catch((error) => {
                console.log(error);
            })
    };
    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.name.length === 0) {
            setFormError('Privaloma nurodyti vietovės pavadinimą.');

            return;
        }

        if (formData.address.length === 0) {
            setFormError('Privaloma nurodyti vietovės addresą.');

            return;
        }

        if (formData.tags.length === 0) {
            setFormError('Privaloma nurodyti vietovės žymas.');

            return;
        }

        if (formData.description.length === 0) {
            setFormError('Privaloma nurodyti vietovės aprašymą.');

            return;
        }

        setFormError('');
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        // @ts-ignore
        updatePlaceAction(place.id, formData)
            .then((response) => {
                const responseData = response.data;
                setFormData(defaultPlaceRequest);
                if (files.length > 0) {
                    uploadPlacePhotosAction(files, responseData.id)
                        .then((response) => {
                            // handleModalClose();
                            setFiles([]);

                            history.push("/admin/places");

                            setSpinnerData({
                                ...spinnerData,
                                active: false,
                            });
                        });
                } else {
                    history.push("/admin/places");

                    setSpinnerData({
                        ...spinnerData,
                        active: false,
                    });
                }

            })
            .catch((error) => {
                setFormError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            });
    };

    return (
        <>
            <PageContentTop title="Vietovės"/>
            <PageContentBody>
                <Card title="Vietovės informacijos redagavimas">
                    <div className="add-place-form">
                        {
                            formError.length > 0
                                ? <div className="alert alert-danger">
                                    {formError}
                                </div>
                                : null
                        }
                        <form action="/" method="POST" onSubmit={onFormSubmit}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-12">
                                                <input type="text"
                                                       className="form-control"
                                                       name="name"
                                                       placeholder="Pavadinimas"
                                                       autoComplete="off"
                                                       value={formData.name}
                                                       onChange={onInputChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="address-search-input-block">
                                                    <input type="text"
                                                           className="form-control"
                                                           name="address"
                                                           placeholder="Adresas"
                                                           autoComplete="off"
                                                           value={formData.address}
                                                           onKeyUp={onAddressInputKeyUp}
                                                           onKeyDown={onAddressInputKeyDown}
                                                    />
                                                    <div
                                                        className={"address-search-spinner " + (addressSpinnerState === true ? '' : 'd-none')}>
                                                        <img src={SpinnerImg}/>
                                                    </div>
                                                </div>
                                                <div
                                                    className={joinClasses(["address-autocomplete-results", (addressesAutocompleteItems.length > 0 ? '' : 'd-none')])}>
                                                    <ul>
                                                        {
                                                            addressesAutocompleteItems.map((element, key) => (
                                                                // @ts-ignore
                                                                <li key={key} onClick={onAddressPlaceSelect.bind(this, element)}>{element.name}</li>
                                                            ))
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-6">
                                                <input type="text"
                                                       className="form-control"
                                                       name="latitude"
                                                       id="js-latitude-input"
                                                       disabled
                                                       placeholder="Ilguma"
                                                       autoComplete="off"
                                                       value={formData.latitude}
                                                       onChange={onInputChange}/>
                                            </div>
                                            <div className="col-6">
                                                <input type="text"
                                                       className="form-control"
                                                       name="longitude"
                                                       id="js-longitude-input"
                                                       disabled
                                                       placeholder="Platuma"
                                                       autoComplete="off"
                                                       value={formData.longitude}
                                                       onChange={onInputChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-12">
                                                <Select
                                                    options={availableActivities}
                                                    isMulti
                                                    name="tags"
                                                    placeholder="Priskikite vietovės žymas"
                                                    className="search-tags-select"
                                                    value={getAvailableActivitiesByValues()}
                                                    onChange={onTagsInputChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <section className="dropzone-container">
                                                    <div {...getRootProps({className: 'dropzone'})}>
                                                        <input {...getInputProps()} />
                                                        <p>Tempkite nuotraukas į šį laukelį, arba paspauskite ir
                                                            pasirinkite
                                                            nuotraukas</p>
                                                    </div>
                                                    <aside>
                                                        {thumbs}
                                                        {existingThumbs}
                                                    </aside>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    {
                                        currentPlace
                                            ? <Map selectorId="add-place-map" places={[currentPlace]}></Map>
                                            : <Map selectorId="add-place-map" places={[]}></Map>
                                    }
                                </div>
                            </div>
                            <br/>
                            <div className="form-group">
                                <div className="row">
                                    <div className="col-md-12">
                                    <textarea className="form-control"
                                              name="description"
                                              onChange={onInputChange}
                                              autoComplete="off"
                                              style={{height: '240px'}}
                                              value={formData.description}
                                              placeholder="Pateiktite vietovės aprašymą, pateikite instrukcijas kaip nuvykti iki vietovės ir pan."
                                    ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <button type="submit"
                                        className="btn btn-primary col-12 col-md-4  py-3 d-block float-none mx-auto">
                                    Išsaugoti
                                </button>
                            </div>
                        </form>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default Update;

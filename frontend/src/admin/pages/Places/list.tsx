import React, {useEffect, useState} from 'react';
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {getPlacesAction} from "../../actions/PlaceActions";
import {Place} from "../../../shared/assets/models/Place";
import {Link} from "react-router-dom";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";

function List() {
    const [places, setPlaces] = useState([] as Place[]);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });
        getPlacesAction(0, 30)
            .then((response) => {
                const responsePlaces = response.data;

                setPlaces(responsePlaces);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, [])

    return (
        <>
            <PageContentTop title="Vietovės"/>
            <PageContentBody>
                <Card title="Vietovių sąrašas">
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Pavadinimas</th>
                                <th>Adresas</th>
                                <th>Koordinatės</th>
                                <th>Veiksmai</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                places.map((place: Place) => (
                                    <tr>
                                        <td>{place.id}</td>
                                        <td>{place.name}</td>
                                        <td>{place.address}</td>
                                        <td>Ilguma: {place.latitude} / Platuma: {place.longitude}</td>
                                        <td>
                                            <Link to={"/admin/places/"+place.id+"/edit"} className="pr-3">
                                                <i className="icon-pencil fw-700 fs-14 btn btn-sm btn-outline-info"></i>
                                            </Link>
                                            <Link to={"/admin/places/"+place.id+"/show"}>
                                                <i className="icon-search fw-700 fs-14 btn btn-sm btn-outline-success"></i>
                                            </Link>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default List;

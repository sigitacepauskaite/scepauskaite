import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {Place} from "../../../shared/assets/models/Place";
import {defaultPlaceRequest, PlaceRequest} from "../../../shared/models/request/PlaceModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {availableActivities} from "../../../shared/data/PlaceData";
import {getPlaceAction} from "../../actions/PlaceActions";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import Map from "../../../shared/components/Map/Map";

function Show() {
    const {id}: any = useParams();
    const [place, setPlace] = useState(null as Place | null);
    const [formError, setFormError] = useState('');
    const [formData, setFormData] = useState(defaultPlaceRequest as PlaceRequest);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const getAvailableActivitiesByValues = (): any => {
        let results: any = [];


        formData.tags.forEach((value) => {
            const foundItem: any = availableActivities.find((row: any) => row.value === value);
            results = [...results, foundItem]
        });

        return results;
    };

    let existingThumbs = null;
    if (place && place.photos) {
        existingThumbs = place.photos.map(photo => (
            <div className="thumb" key={photo.id}>
                <div className="thumb-inner">
                    <img src={photo.url}/>
                </div>
            </div>
        ));
    }

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getPlaceAction(id)
            .then((response: any) => {
                const responseData: Place = response.data;
                setFormData({...formData, ...responseData});
                setPlace(responseData);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

            })
            .catch((error) => {
                setFormError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    }, []);

    return (
        <>
            <PageContentTop title="Vietovės"/>
            <PageContentBody>
                <Card title="Vietovės informacijos peržiūra">
                    <div className="add-place-form">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="table-responsive">
                                    <table className="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Informacijos vienetas</th>
                                            <th>Reikšmė</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#ID</td>
                                            <td>{place ? place.id : ''}</td>
                                        </tr>
                                        <tr>
                                            <td>Pavadinimas</td>
                                            <td>{place ? place.name : ''}</td>
                                        </tr>
                                        <tr>
                                            <td>Adresas</td>
                                            <td>{place ? place.address : ''}</td>
                                        </tr>
                                        <tr>
                                            <td>Koordinatės</td>
                                            <td>{place ? place.latitude : ''}/{place ? place.longitude : ''}</td>
                                        </tr>
                                        <tr>
                                            <td>Aprašymas</td>
                                            <td>{place ? place.description : ''}</td>
                                        </tr>
                                        <tr>
                                            <td>Žymos</td>
                                            <td>{getAvailableActivitiesByValues().map((record: any) => (
                                                <p>{record.label}</p>
                                            ))}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div className="dropzone-container">
                                    {existingThumbs}
                                </div>
                            </div>
                            <div className="col-md-6">
                                {
                                    place
                                        ? <Map selectorId="add-place-map" places={[place]}></Map>
                                        : <Map selectorId="add-place-map" places={[]}></Map>
                                }
                            </div>
                        </div>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default Show;

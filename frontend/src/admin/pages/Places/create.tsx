import React, {useEffect, useState} from 'react';
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {Place} from "../../../shared/assets/models/Place";
import {defaultPlaceRequest, PlaceRequest} from "../../../shared/models/request/PlaceModels";
import {useDropzone} from "react-dropzone";
import {getPlaceDetails, getPlacesAutocomplete} from "../../../shared/actions/GoogleApiActions";
import SpinnerImg from "../../../shared/assets/img/spinner.svg";
import {joinClasses} from "../../../shared/utils/ClassUtils";
import Select from "react-select";
import {availableActivities} from "../../../shared/data/PlaceData";
import Map from "../../../shared/components/Map/Map";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {createPlaceAction, uploadPlacePhotosAction} from "../../actions/PlaceActions";
import {history} from "../../../shared/config/history";

type PlacesAutocompleteItem = {
    name: string,
    placeId: string,
};

function Create() {
    const [addressSpinnerState, setAddressSpinnerState] = useState(false);
    const [currentPlace, setCurrentPlace] = useState(null as Place | null);
    const [formError, setFormError] = useState('');
    const [addressesAutocompleteItems, setAddressesAutocompleteItems] = useState([] as PlacesAutocompleteItem[]);
    const [formData, setFormData] = useState(defaultPlaceRequest as PlaceRequest);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const [files, setFiles] = useState([] as File[]);
    const {getRootProps, getInputProps} = useDropzone({
        accept: 'image/*',
        onDrop: acceptedFiles => {
            // const uploadedFiles: DropZoneFile[] = acceptedFiles.map(file => Object.assign(file, {
            //     preview: URL.createObjectURL(file)
            // }));

            setFiles([...files, ...acceptedFiles]);
        }
    });
    const thumbs = files.map(file => (
        <div className="thumb" key={file.name}>
            <div className="thumb-inner">
                <img
                    src={URL.createObjectURL(file)}
                />
            </div>
        </div>
    ));

    useEffect(() => () => {
        files.forEach(file => URL.revokeObjectURL(URL.createObjectURL(file)));
    }, [files]);

    let typingTimer: any;
    const doneTypingInterval: number = 500;
    const onTagsInputChange = (selectedValues: any) => {
        const results: string[] = selectedValues.map((selectedValue: any) => {
            return selectedValue.value;
        });
        setFormData({
            ...formData,
            tags: results
        });
    };
    const onInputChange = (event: any) => {
        if (event.target.name === 'name') {
            setFormData({
                ...formData,
                name: event.target.value
            });
        }

        if (event.target.name === 'address') {
            setFormData({
                ...formData,
                address: event.target.value
            });
        }

        if (event.target.name === 'latitude') {
            setFormData({
                ...formData,
                latitude: event.target.value
            });
        }

        if (event.target.name === 'longitude') {
            setFormData({
                ...formData,
                longitude: event.target.value
            });
        }

        if (event.target.name === 'description') {
            setFormData({
                ...formData,
                description: event.target.value
            });
        }
    };
    const onAddressInputKeyUp = (event: any) => {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(onAddressInputChange, doneTypingInterval, event);
    };
    const onAddressInputKeyDown = (event: any) => {
        const value = event.target.value;

        if (value && value.length > 3) {
            setAddressSpinnerState(true);
        }

        clearTimeout(typingTimer);
    };
    const onAddressInputChange = (event: any) => {
        const value = event.target.value;

        if (value && value.length > 3) {
            getPlacesAutocomplete(value)
                .then((response) => {
                    setAddressesAutocompleteItems(response.data);
                    setAddressSpinnerState(false);
                })
                .catch((error) => {
                    console.log(error);
                })
        }
    };
    const onAddressPlaceSelect = (placeDetails: PlacesAutocompleteItem): any => {
        getPlaceDetails(placeDetails.placeId)
            .then((response) => {
                // @ts-ignore
                document.getElementById('js-latitude-input').value = Number(response.data.latitude);
                // @ts-ignore
                document.getElementById('js-longitude-input').value = Number(response.data.longitude);

                const place: Place = {
                    name: '',
                    address: response.data.name,
                    latitude: response.data.latitude,
                    longitude: response.data.longitude,
                    centered: true,
                    showPlaceInfoBox: false,
                    photos: [],
                };
                setCurrentPlace(place);

                setFormData({
                    ...formData,
                    address: response.data.name,
                    latitude: response.data.latitude,
                    longitude: response.data.longitude,
                });
                setAddressesAutocompleteItems([]);
            })
            .catch((error) => {
                console.log(error);
            })
    };
    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.name.length === 0) {
            setFormError('Privaloma nurodyti vietovės pavadinimą.');

            return;
        }

        if (formData.address.length === 0) {
            setFormError('Privaloma nurodyti vietovės addresą.');

            return;
        }

        if (formData.tags.length === 0) {
            setFormError('Privaloma nurodyti vietovės žymas.');

            return;
        }

        if (formData.description.length === 0) {
            setFormError('Privaloma nurodyti vietovės aprašymą.');

            return;
        }

        if (files.length === 0) {
            setFormError('Privaloma įkelti bent vieną nuotrauką.');

            return;
        }

        if (files.length > 3) {
            setFormError('Maksimalus leistinas nuotraukų skaičius - 3');

            return;
        }

        setFormError('');
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        createPlaceAction(formData)
            .then((response) => {
                const responseData = response.data;
                setFormData(defaultPlaceRequest);
                uploadPlacePhotosAction(files, responseData.id)
                    .then((response) => {
                        // handleModalClose();
                        setFiles([]);

                        history.push("/admin/places");

                        setSpinnerData({
                            ...spinnerData,
                            active: false,
                        });
                    });
            })
            .catch((error) => {
                setFormError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            });
    };

    return (
        <>
            <PageContentTop title="Vietovės"/>
            <PageContentBody>
                <Card title="Vietovės kūrimas">
                    <div className="add-place-form">
                        {
                            formError.length > 0
                                ? <div className="alert alert-danger">
                                    {formError}
                                </div>
                                : null
                        }
                        <form action="" onSubmit={onFormSubmit}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-12">
                                                <input type="text"
                                                       className="form-control"
                                                       name="name"
                                                       placeholder="Pavadinimas"
                                                       autoComplete="off"
                                                       onChange={onInputChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-12">
                                                <div className="address-search-input-block">
                                                    <input type="text"
                                                           className="form-control"
                                                           name="address"
                                                           placeholder="Adresas"
                                                           autoComplete="off"
                                                           onKeyUp={onAddressInputKeyUp}
                                                           onKeyDown={onAddressInputKeyDown}
                                                    />
                                                    <div
                                                        className={"address-search-spinner " + (addressSpinnerState === true ? '' : 'd-none')}>
                                                        <img src={SpinnerImg}/>
                                                    </div>
                                                </div>
                                                <div
                                                    className={joinClasses(["address-autocomplete-results", (addressesAutocompleteItems.length > 0 ? '' : 'd-none')])}>
                                                    <ul>
                                                        {
                                                            addressesAutocompleteItems.map((element, key) => (
                                                                // @ts-ignore
                                                                <li key={key} onClick={onAddressPlaceSelect.bind(this, element)}>{element.name}</li>
                                                            ))
                                                        }
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-6">
                                                <input type="text"
                                                       className="form-control"
                                                       name="latitude"
                                                       id="js-latitude-input"
                                                       disabled
                                                       placeholder="Ilguma"
                                                       autoComplete="off"
                                                       onChange={onInputChange}/>
                                            </div>
                                            <div className="col-6">
                                                <input type="text"
                                                       className="form-control"
                                                       name="longitude"
                                                       id="js-longitude-input"
                                                       disabled
                                                       placeholder="Platuma"
                                                       autoComplete="off"
                                                       onChange={onInputChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-12">
                                                <Select
                                                    options={availableActivities}
                                                    isMulti
                                                    name="tags"
                                                    placeholder="Priskikite vietovės žymas"
                                                    className="search-tags-select"
                                                    onChange={onTagsInputChange}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-md-12">
                                                <section className="dropzone-container">
                                                    <div {...getRootProps({className: 'dropzone'})}>
                                                        <input {...getInputProps()} />
                                                        <p>Tempkite nuotraukas į šį laukelį, arba paspauskite ir
                                                            pasirinkite
                                                            nuotraukas</p>
                                                    </div>
                                                    <aside>
                                                        {thumbs}
                                                    </aside>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    {
                                        currentPlace
                                            ? <Map selectorId="add-place-map" places={[currentPlace]}></Map>
                                            : <Map selectorId="add-place-map" places={[]}></Map>
                                    }
                                </div>
                            </div>
                            <br/>
                            <div className="form-group">
                                <div className="row">
                                    <div className="col-md-12">
                                    <textarea className="form-control"
                                              name="description"
                                              onChange={onInputChange}
                                              autoComplete="off"
                                              style={{height: '240px'}}
                                              placeholder="Pateiktite vietovės aprašymą, pateikite instrukcijas kaip nuvykti iki vietovės ir pan."
                                    ></textarea>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <button type="submit"
                                        className="btn btn-primary col-12 col-md-4  py-3 d-block float-none mx-auto">
                                    Išsaugoti
                                </button>
                            </div>
                        </form>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default Create;

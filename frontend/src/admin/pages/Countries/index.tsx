import React from 'react';
import {Route, Switch} from "react-router-dom";
import List from "../Countries/list";
import Create from "../Countries/create";
import Update from "../Countries/update";
import Show from "../Countries/show";
import {useRouteMatch} from "react-router";

function CountriesPage() {
    let match = useRouteMatch();

    return (
        <Switch>
            <Route exact path={`${match.path}/countries`}>
                <List/>
            </Route>
            <Route exact path={`${match.path}/countries/new`}>
                <Create/>
            </Route>
            <Route exact path={`${match.path}/countries/:id/edit`}>
                <Update/>
            </Route>
            <Route exact path={`${match.path}/countries/:id/show`}>
                <Show/>
            </Route>
        </Switch>
    );
}

export default CountriesPage;

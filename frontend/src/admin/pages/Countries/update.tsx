import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {UserRequest, UserRequestData} from "../../../shared/models/request/UserModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {createUserAction, getUserAction, updateUserAction} from "../../actions/UserActions";
import {history} from "../../../shared/config/history";
import {getArticleAction, updateArticleAction} from "../../actions/ArticleActions";
import {ArticleRequest, defaultArticleRequest} from "../../../shared/models/request/ArticleModels";
import {createCountryAction, getCountryAction, updateCountryAction} from "../../actions/CountryActions";
import {CountryRequest, defaultCountryRequest} from "../../../shared/models/request/CountryModels";

type Props = {};

function Update({}: Props) {
    const {id}: any = useParams();
    const [formData, setFormData] = useState(defaultCountryRequest as CountryRequest);
    const [error, setError] = useState('');
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getCountryAction(id)
            .then((response: any) => {
                const responseData: ArticleRequest = response.data;
                setFormData({...formData, ...responseData});

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    }, []);

    const onInputChange = (event: any) => {
        if (event.target.name === 'name') {
            setFormData({
                ...formData,
                name: event.target.value
            });
        }

        if (event.target.name === 'code') {
            setFormData({
                ...formData,
                code: event.target.value
            });
        }
    };
    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.name.length === 0) {
            setError('Privaloma nurodyti šalies pavadinimą');

            return;
        }

        if (formData.code.length === 0) {
            setError('Privaloma nurodyti šalies kodą');

            return;
        }

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        updateCountryAction(id, formData)
            .then((response: any) => {
                const responseData: any = response.data;

                history.push("/admin/settings/countries");

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    };

    return (
        <>
            <PageContentTop title="Nustatymai"/>
            <PageContentBody>
                <div className="row">
                    <div className="col-md-7">
                        <Card title="Šalies informacijos atnaujinimas">
                            {
                                error.length > 0
                                    ? <div className="alert alert-danger">
                                        {error}
                                    </div>
                                    : null
                            }
                            <form action="/" method="POST" onSubmit={onFormSubmit}>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <label>Pavadinimas</label>
                                            <input type="text" name="name" className="form-control" required
                                                   value={formData.name} onChange={onInputChange}/>
                                        </div>
                                        <div className="col-md-4">
                                            <label>Šalies kodas</label>
                                            <input type="text" name="code" minLength={2} maxLength={2}
                                                   className="form-control" required
                                                   value={formData.code} onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-6 mx-auto">
                                            <button className="btn btn-primary btn-block">
                                                Išsaugoti
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </Card>
                    </div>
                </div>
            </PageContentBody>
        </>
    );
}

export default Update;

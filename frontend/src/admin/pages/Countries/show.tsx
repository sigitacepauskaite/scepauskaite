import React, {useEffect, useState} from 'react';
import {UserModel, UserModelData} from "../../../shared/models/response/User/UserModel";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {useParams} from "react-router-dom";
import {getArticleAction} from "../../actions/ArticleActions";
import {CountryModel, CountryModelData} from "../../../shared/models/request/CountryModels";
import {getCountryAction} from "../../actions/CountryActions";

function Show() {
    const {id}: any = useParams();
    const [country, setCountry] = useState(CountryModelData as CountryModel);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getCountryAction(id)
            .then((response) => {
                const responseItems = response.data;

                setCountry(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    return (
        <>
            <PageContentTop title="Nustatymai"/>
            <PageContentBody>
                <Card title={"Šalies nustatymai"}>
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>Informacijos vienetas</th>
                                <th>Reikšmė</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>#ID</td>
                                    <td>{country.id}</td>
                                </tr>
                                <tr>
                                    <td>Pavadinimas</td>
                                    <td>{country.name}</td>
                                </tr>
                                <tr>
                                    <td>Šalies kodas</td>
                                    <td>{country.code}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default Show;

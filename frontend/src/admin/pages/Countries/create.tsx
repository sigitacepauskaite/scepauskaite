import React, {useState} from 'react';
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {history} from "../../../shared/config/history";
import {CountryRequest, defaultCountryRequest} from "../../../shared/models/request/CountryModels";
import {createCountryAction} from "../../actions/CountryActions";

function Create() {
    const [formData, setFormData] = useState(defaultCountryRequest as CountryRequest);
    const [error, setError] = useState('');
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    const onInputChange = (event: any) => {
        if (event.target.name === 'name') {
            setFormData({
                ...formData,
                name: event.target.value
            });
        }

        if (event.target.name === 'code') {
            setFormData({
                ...formData,
                code: event.target.value
            });
        }
    };
    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.name.length === 0) {
            setError('Privaloma nurodyti šalies pavadinimą');

            return;
        }

        if (formData.code.length === 0) {
            setError('Privaloma nurodyti šalies kodą');

            return;
        }

        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        createCountryAction(formData)
            .then((response: any) => {
                const responseData: any = response.data;

                history.push("/admin/settings/countries");

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch((error) => {
                setError(error.response.data.message);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });

                return;
            })
    };

    return (
        <>
            <PageContentTop title="Nustatymai"/>
            <PageContentBody>
                <div className="row">
                    <div className="col-md-7">
                        <Card title="Nauja šalis">
                            {
                                error.length > 0
                                    ? <div className="alert alert-danger">
                                        {error}
                                    </div>
                                    : null
                            }
                            <form action="/" method="POST" onSubmit={onFormSubmit}>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-8">
                                            <label>Pavadinimas</label>
                                            <input type="text" name="name" className="form-control" required
                                                   onChange={onInputChange}/>
                                        </div>
                                        <div className="col-md-4">
                                            <label>Šalies kodas</label>
                                            <input type="text" name="code" minLength={2} maxLength={2}
                                                   className="form-control" required
                                                   onChange={onInputChange}/>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div className="form-group">
                                    <div className="row">
                                        <div className="col-md-6 mx-auto">
                                            <button className="btn btn-primary btn-block">
                                                Išsaugoti
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </Card>
                    </div>
                </div>
            </PageContentBody>
        </>
    );
}

export default Create;

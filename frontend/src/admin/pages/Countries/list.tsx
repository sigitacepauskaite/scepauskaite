import React, {useEffect, useState} from 'react';
import PageContentTop from "../../components/PageContentTop";
import PageContentBody from "../../components/PageContentBody";
import Card from "../../components/Card";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {Link} from 'react-router-dom';
import {ArticleModel, ArticleRequest, defaultArticleRequest} from "../../../shared/models/request/ArticleModels";
import {getArticlesAction} from "../../actions/ArticleActions";
import {CountryModel} from "../../../shared/models/request/CountryModels";
import {getCountriesAction} from "../../actions/CountryActions";


function List() {
    const [countries, setCountries] = useState([] as CountryModel[]);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getCountriesAction(0, 30)
            .then((response) => {
                const responseItems = response.data;

                setCountries(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    return (
        <>
            <PageContentTop title="Nustatymai"/>
            <PageContentBody>
                <Card title="Šalys">
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Pavadinimas</th>
                                <th>Kodas</th>
                                <th>Veiksmai</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                countries.map((country: CountryModel, key: any) => (
                                    <tr key={key}>
                                        <td>{country.id}</td>
                                        <td>{country.name}</td>
                                        <td>{country.code}</td>
                                        <td>
                                            <Link to={"/admin/settings/countries/"+country.id+"/edit"} className="pr-3">
                                                <i className="icon-pencil fw-700 fs-14 btn btn-sm btn-outline-info"></i>
                                            </Link>
                                            <Link to={"/admin/settings/countries/"+country.id+"/show"}>
                                                <i className="icon-search fw-700 fs-14 btn btn-sm btn-outline-success"></i>
                                            </Link>
                                        </td>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
                </Card>
            </PageContentBody>
        </>
    );
}

export default List;

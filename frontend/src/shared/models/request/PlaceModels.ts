export type PlaceRequest = {
    name: string,
    address: string,
    latitude: string,
    longitude: string,
    description: string,
    tags: string[],
};

export const defaultPlaceRequest: PlaceRequest = {
    name: '',
    address: '',
    latitude: '0',
    longitude: '0',
    description: '',
    tags: [],
};

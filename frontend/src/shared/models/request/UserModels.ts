export type RegistrationFormData = {
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    countryCode: string,
};

export const defaultRegistrationData : RegistrationFormData = {
    email: '',
    password: '',
    firstName: '',
    lastName: '',
    countryCode: 'LT',
};

export type LoginFormData = {
    email: string,
    password: string,
};

export const defaultLoginData: LoginFormData = {
    email: '',
    password: ''
};

export type UserRequest = {
    email: string,
    password: string,
    repeatedPassword: string,
    firstName: string,
    lastName: string,
    countryCode: string,
};

export const UserRequestData : UserRequest = {
    email: '',
    password: '',
    repeatedPassword: '',
    firstName: '',
    lastName: '',
    countryCode: 'LT',
};

export type AdminLoginFormData = {
    username: string,
    password: string,
};

export const defaultAdminLoginData: AdminLoginFormData = {
    username: '',
    password: ''
};

export type UserProfileUpdateRequest = {
    firstName: string,
    lastName: string,
};

export const UserProfileUpdateRequestData : UserProfileUpdateRequest = {
    firstName: '',
    lastName: '',
};

export type UserUpdatePasswordRequest = {
    password: string,
    repeatedPassword: string,
};

export const UserUpdatePasswordRequestData : UserUpdatePasswordRequest = {
    password: '',
    repeatedPassword: '',
};

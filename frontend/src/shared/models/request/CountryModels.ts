export type CountryRequest = {
    name: string,
    code: string,
};

export const defaultCountryRequest: CountryRequest = {
    name: '',
    code: '',
};

export type CountryModel = {
    id: number,
    name: string,
    code: string,
};

export const CountryModelData: CountryModel = {
    id: 0,
    name: '',
    code: '',
};

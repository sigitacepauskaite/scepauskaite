import {PlacePhoto} from "../../assets/models/Place";

export type ArticleRequest = {
    id: number,
    name: string,
    content: string,
    photos: PlacePhoto[],
};

export const defaultArticleRequest: ArticleRequest = {
    id: 0,
    name: '',
    content: '',
    photos: [],
};

export type ArticleModel = {
    id: number,
    name: string,
    content: string,
    photos: PlacePhoto[],
};

export const ArticleModelData: ArticleModel = {
    id: 0,
    name: '',
    content: '',
    photos: [],
};

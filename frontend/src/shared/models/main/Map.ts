import {Place, placeData} from "../../assets/models/Place";

export type TripModel = {
    id: number,
    name: string,
    tripDate: string,
    tripStops: TripStopModel[],
    places: Place[],
    duration: string,
    distance: string,
};

export const TripModelData: TripModel = {
    id: 0,
    name: '',
    tripDate: '',
    tripStops: [],
    places: [],
    duration: '',
    distance: '',
};

export type TripStopModel = {
    id: number,
    placeId: number,
    place: Place,
    orderNumber: number,
    note?: string,
};

export const TripStopModelData: TripStopModel = {
    id: 0,
    placeId: 0,
    place: placeData,
    orderNumber: 1,
    note: '',
};

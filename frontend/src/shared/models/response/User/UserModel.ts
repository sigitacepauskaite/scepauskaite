import {UserRequest} from "../../request/UserModels";

export type LoginResponse = {
    token: string,
};

export type UserModel = {
    id: number,
    email: string,
    firstName: string,
    lastName: string,
    roles: string[],
    countryCode: string,
};

export const UserModelData : UserModel = {
    id: 0,
    email: '',
    firstName: '',
    lastName: '',
    roles: [],
    countryCode: 'LT',
};

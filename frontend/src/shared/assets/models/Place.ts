export type Place = {
    id?: number,
    icon?: string,
    name?: string,
    photos?: PlacePhoto[],
    latitude: string,
    longitude: string,
    address: string,
    description?: string,
    color?: string,
    showPlaceInfoBox?: boolean,
    centered?: boolean,
}

export type PlacePhoto = {
    id: number,
    url: string,
};

export type PlaceRate = {
    place: Place,
    rate: number,
    note: string,
}

export const placeData: Place = {
    id : 0,
    icon : '',
    name : '',
    photos : [],
    latitude : '0',
    longitude : '0',
    address : '',
    description : '',
    color : '',
    showPlaceInfoBox : false,
    centered : false,
}

export const placeRateData: PlaceRate = {
    place: placeData,
    rate: 0,
    note: '',
}

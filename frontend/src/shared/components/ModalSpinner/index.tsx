import React, {useState} from 'react';
import SpinnerImg from '../../assets/img/spinner.svg';
import {joinClasses} from "../../utils/ClassUtils";
import './index.scss';

type Props = {
    active: boolean,
};

const ModalSpinner = ({active} : Props) => {
    if (!active) {
        return null;
    }

    return (
        <div className={joinClasses(['modal-spinner', active ? '' : 'd-none'])}>
            <img src={SpinnerImg}/>
        </div>
    );
}

export default ModalSpinner;

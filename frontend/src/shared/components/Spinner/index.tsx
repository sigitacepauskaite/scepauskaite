import React, {useState} from 'react';
import SpinnerImg from '../../assets/img/spinner.svg';
import {joinClasses} from "../../utils/ClassUtils";
import {useSpinnerContext} from "../../context/SpinnerContext";
import './index.scss';

const Spinner = () => {
    const {spinnerData}: any = useSpinnerContext();

    return (
        <div className={joinClasses(['spinner', spinnerData.active ? '' : 'd-none'])}>
            <img src={SpinnerImg}/>
        </div>
    );
}

export default Spinner;

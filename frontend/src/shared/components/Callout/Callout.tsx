import React, {useState} from 'react';
import "./index.scss";
import {useCalloutContext} from "../../context/CalloutContext";

const Callout = () => {
    const {calloutData}: any = useCalloutContext()

    if (calloutData.type === 'success') {
        return (
            <div className="callout callout-success">
                {calloutData.message}
            </div>
        );
    }

    if (calloutData.type === 'warning') {
        return (
            <div className="callout callout-warning">
                {calloutData.message}
                Jūsų pranešimas buvo sėkmingai išsiųstas.
            </div>
        );
    }

    if (calloutData.type === 'danger') {
        return (
            <div className="callout callout-danger">
                Atsiprašome, įvyko nenumatyta klaida. Prašome bandyti dar kartą.
            </div>
        );
    }

    return null;
};

export default Callout;

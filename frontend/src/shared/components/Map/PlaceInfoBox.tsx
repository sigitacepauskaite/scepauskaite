import React, {useState} from 'react';
import {Place} from "../../assets/models/Place";
import PlacePreviewModal from "../../modals/PlacePreview";
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';

type Props = {
    place: Place,
};
const PlaceInfoBox = ({place}: Props) => {
    const [placePreviewModalState, setPlacePreviewModalState] = useState(false);

    const loadPlacePreviewModal = () => {
        setPlacePreviewModalState(true);
    };

    const handleModalClose = () => {
        setPlacePreviewModalState(false);
    };

    return (
        <>
            <div className="place-info-box-content">
                <div className="cover"
                     style={{backgroundImage: 'url(' + (place.photos && place.photos.length > 0 ? place.photos[0].url : '') + ')'}}>
                    <div className="overlay"/>
                    <div className="cover-text">
                        <p className="place-name">{place.name}</p>
                        <p className="place-address">{place.address}</p>
                        <p className="place-address">{place.latitude} / {place.longitude}</p>
                    </div>
                </div>
                <div className="content">
                    {place.description}
                </div>
                <div className="read-more">
                    <button className="btn btn-primary btn-round btn-block" onClick={loadPlacePreviewModal}>
                        Peržiūrėti
                    </button>
                </div>
            </div>
            <PlacePreviewModal place={place} showModal={placePreviewModalState} handleModalClose={handleModalClose}/>
        </>
    );
};

export default PlaceInfoBox;

import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import {mapStyle} from "../../../user/components/TripForm/mapStyle";
import {Place} from "../../assets/models/Place";
import markerImg from '../../../shared/assets/img/marker.svg';
import PlaceInfoBox from "./PlaceInfoBox";

type Props = {
    selectorId: string,
    places: Place[],
    showPlaceInfoBox?: boolean,
    zoom?: number,
    children?: any
};

const Map = ({selectorId, places, showPlaceInfoBox, zoom, children}: Props) => {
    const zoomValue = zoom ? zoom : 17;
    const placeInfoBoxEnabled = showPlaceInfoBox ? showPlaceInfoBox : false;
    useEffect(() => {
        const map = initMap(selectorId);
        initMarkers(map, places);
    });

    const initMap = (selectorId: string): any => {
        return new google.maps.Map(document.getElementById(selectorId) as HTMLElement,
            {
                center: {lat: -34.397, lng: 150.644},
                zoom: zoomValue,
                zoomControl: false,
                streetViewControl: false,
                fullscreenControl: false,
                mapTypeControl: false,
                styles: mapStyle,
            }
        );
    };
    const initMarkers = (map: any, places: Place[]): void => {
        if (!places) {
            return;
        }

        places.forEach((place, key: number) => {
            if (place.latitude !== '0' && place.longitude !== '0') {
                const marker = new google.maps.Marker({
                    position: {lat: Number(place.latitude), lng: Number(place.longitude)},
                    map,
                    title: "Hello World!",
                    icon: {
                        url: place.icon || markerImg,
                        scaledSize: new google.maps.Size(50, 50), // scaled size
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0) // anchor
                    },
                });

                const infoWindow = new google.maps.InfoWindow({
                    content: '<div id="place-info-box"></div>',
                });

                if (place.centered || key === 0) {
                    map.setCenter(marker.getPosition());
                }

                if (placeInfoBoxEnabled) {

                    google.maps.event.addListener(infoWindow, 'domready', function (e) {
                        ReactDOM.render(<PlaceInfoBox place={place}/>, document.getElementById('place-info-box'));
                    });

                    marker.addListener("click", () => {
                        infoWindow.open(map, marker);
                    });
                }
            }
        });
    };

    return (
        <>
            <div className="map-container h-100">
                <div className="map" id={selectorId}></div>
                {children}
            </div>
        </>
    );
};

export default Map;

import axios from "axios";

export const getPlacesAutocomplete = (term: string) => {
    return axios.get('/api/public/google/places-autocomplete', {
        params: {
            term: term,
            XDEBUG_SESSION_START: 'PHPSTORM',
        }
    });
};

export const getPlaceDetails = (placeId: string) => {
    return axios.get('/api/public/google/places-details', {
        params: {
            placeId: placeId,
            XDEBUG_SESSION_START: 'PHPSTORM',
        }
    });
};

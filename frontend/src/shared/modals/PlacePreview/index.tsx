import React, {useState} from 'react';
import {Modal} from 'react-bootstrap';
import './index.scss';
import Carousel from "@brainhubeu/react-carousel";
import {Place} from "../../assets/models/Place";


type Props = {
    place: Place,
    showModal: boolean,
    handleModalClose: any,
};

const PlacePreviewModal = ({place, showModal, handleModalClose}: Props) => {
    return (
        <>
            <Modal className="place-preview-modal" show={showModal} onHide={handleModalClose}>
                <Modal.Body>
                    <div className="place-info-box-content h-100">
                        <div className="modal-close-btn" onClick={handleModalClose}>
                            <i className="fa fa-times"/>
                        </div>
                        <div className="row h-100">
                            <div className="col-md-8 h-100 position-relative overflow-hidden">
                                <Carousel plugins={['arrows']}
                                >
                                    {
                                        place.photos?.map((photo) => (
                                            <div className="cover-image" style={{backgroundImage: 'url(' + (photo.url) + ')'}} />
                                        ))
                                    }
                                </Carousel>
                                {/*<div className="cover"*/}
                                {/*     style={{backgroundImage: 'url(' + (place.photos && place.photos.length > 0 ? place.photos[0].url : '') + ')'}}>*/}
                                {/*</div>*/}
                            </div>
                            <div className="col-md-4 h-100">
                                <div className="content">
                                    <p className="place-name">{place.name}</p>
                                    <p className="place-address">{place.address}</p>
                                    <p className="place-address">{place.latitude} / {place.longitude}</p>
                                    <hr/>
                                    <p className="description">
                                        {place.description}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
};

export default PlacePreviewModal;

import {useState} from "react";

export const availableActivities = [
    {value: 'local-good', label: 'Vietinis maistas'},
    {value: 'beautiful-nature', label: 'Gamtos kampeliai'},
    {value: 'museums', label: 'Muziejai'},
    {value: 'tours', label: 'Pažintiniai turai'},
    {value: 'restaurants', label: 'Restoranai'},
    {value: 'picnic', label: 'Vietos iškylai'},
    {value: 'entertainments', label: 'Pramogų zonos'},
    {value: 'caves', label: 'Urvai'},
    {value: 'bunkers', label: 'Bunkeriai'},
    {value: 'abandoned-buildings', label: 'Apleisti pastatai'},
    {value: 'lakes', label: 'Ežerai'},
    {value: 'beaches', label: 'Paplūdimiai'},
    {value: 'forests', label: 'Miško takeliai'},
    {value: 'mountains', label: 'Kalnai'},
    {value: 'observation-towers', label: 'Apžvalgos bokštai'},
];

export const availableLocations = [
    {value: 'vilnius', label: 'Vilnius'},
    {value: 'kaunas', label: 'Kaunas'},
    {value: 'klaipeda', label: 'Klaipėda'},
    {value: 'siauliai', label: 'Šiauliai'},
    {value: 'panevezys', label: 'Panevėžys'},
    {value: 'alytus', label: 'Alytus'},
    {value: 'marijampole', label: 'Marijampolė'},
    {value: 'mazeikiai', label: 'Mažeikiai'},
    {value: 'utena', label: 'Utena'},
];

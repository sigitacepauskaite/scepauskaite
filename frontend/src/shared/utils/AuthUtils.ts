import {UserModel} from "../models/response/User/UserModel";

export function getAuthenticatedUserToken(): string|null {
    const userToken: string|null = localStorage.getItem('authorizationToken');

    if (userToken === null) {
        return null;
    }

    return userToken;
}

export function getAuthenticatedUser(): UserModel|null {
    const userToken: string|null = localStorage.getItem('authorizationToken');
    const userJsonData: string|null = localStorage.getItem('authorizedUser');

    if (userToken === null || userJsonData === null) {
        return null;
    }

    const userData : UserModel =  JSON.parse(userJsonData);

    return userData;
}

export function getAuthenticatedAdminUserToken(): string|null {
    const userToken: string|null = localStorage.getItem('adminAuthorizationToken');

    if (userToken === null) {
        return null;
    }

    return userToken;
}

export function getAuthenticatedAdminUser(): UserModel|null {
    const userToken: string|null = localStorage.getItem('adminAuthorizationToken');
    const userJsonData: string|null = localStorage.getItem('authorizedAdminUser');

    if (userToken === null || userJsonData === null) {
        return null;
    }

    const userData : UserModel =  JSON.parse(userJsonData);

    return userData;
}

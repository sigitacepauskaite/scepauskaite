import React, {useMemo, useState} from 'react'

type SpinnerContextData = {
    active: boolean,
};

const contextDefaults: SpinnerContextData = {
    active: false,
};

const SpinnerContext = React.createContext<SpinnerContextData>(contextDefaults);

export function SpinnerContextProvider(props: any): any {
    const [spinnerData, setSpinnerData] = useState(contextDefaults);
    const value : any = useMemo(() => ({ spinnerData, setSpinnerData }), [spinnerData]);

    return (
        <SpinnerContext.Provider value={value}>
            {props.children}
        </SpinnerContext.Provider>
    )
}

export function useSpinnerContext(): any {
    const context = React.useContext(SpinnerContext);
    if (!context) {
        throw new Error(`SpinnerContext must be used within a Provider`);
    }

    return context;
}

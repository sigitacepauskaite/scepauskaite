import React, {useMemo, useState} from 'react'

type CalloutContextData = {
    type: string,
    message: string,
};

const contextDefaults: CalloutContextData = {
    type: '',
    message: '',
};

const CalloutContext = React.createContext<CalloutContextData>(contextDefaults);

export function CalloutContextProvider(props: any): any {
    const [calloutData, setCalloutData] = useState(contextDefaults);
    const value : any = useMemo(() => ({ calloutData, setCalloutData }), [calloutData]);

    return (
        <CalloutContext.Provider value={value}>
            {props.children}
        </CalloutContext.Provider>
    )
}

export function useCalloutContext(): any {
    const context = React.useContext(CalloutContext);
    if (!context) {
        throw new Error(`CalloutContext must be used within a Provider`);
    }

    return context;
}

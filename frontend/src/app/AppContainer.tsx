import React from 'react';
import Header from "./components/Header";
import {Route, Switch} from "react-router";
import Homepage from "./pages/Homepage";
import PublicAboutPage from "./pages/About";
import Spinner from "../shared/components/Spinner";
import Callout from "../shared/components/Callout/Callout";
import {useCalloutContext} from "../shared/context/CalloutContext";
import PublicBlogPage from "./pages/Blog";
import PublicBlogPreviewPage from "./pages/Blog/preview";

function AppContainer() {
    const {calloutData} = useCalloutContext();

    return (
        <>
            <Callout/>
            <Header/>
            <div className={"page-content "+(calloutData.type.length > 0 ? 'callout-active' : '')} id="page-content">
                <Switch>
                    <Route exact path={'/'}>
                        <Homepage/>
                    </Route>
                    <Route exact path={'/about'}>
                        <PublicAboutPage/>
                    </Route>
                    <Route exact path={'/blog'}>
                        <PublicBlogPage/>
                    </Route>
                    <Route exact path={'/blog/:id'}>
                        <PublicBlogPreviewPage/>
                    </Route>
                </Switch>
            </div>
            <Spinner/>
        </>
    );
}

export default AppContainer;

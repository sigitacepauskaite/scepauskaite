import React, {useState} from 'react';
import {Button, Form} from "react-bootstrap";
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import "./index.scss";
import {availableActivities, availableLocations} from "../../../shared/data/PlaceData";
import {getPlacesAppAction} from "../../actions/PlaceActions";
import {Place} from "../../../shared/assets/models/Place";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";

const animatedComponents = makeAnimated();

type Props = {
    places: Place[],
    setPlaces: any,
};

const Search = ({places, setPlaces}:Props) => {
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const [activityFilters, setActivityFilters] = useState([] as string[]);
    const [locationFilters, setLocationFilters] = useState([] as string[]);

    const updateActivityFilters = (values: any) => {
        const filterValues = values.map((value: any) => {
            return value.value
        });

        setActivityFilters(filterValues);
    };
    const updateLocationFilters = (values: any) => {
        const filterValues = values.map((value: any) => {
            return value.value
        });

        setLocationFilters(filterValues);
    };
    const onSearchSubmit = (event: any) => {
        event.preventDefault();

        setSpinnerData({
            ...spinnerData,
            active: true,
        });
        getPlacesAppAction(0, 20, activityFilters, locationFilters)
            .then((response) => {
                setPlaces(response.data);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    };

    return (
        <Form onSubmit={onSearchSubmit}>
            <div className="row">
                <div className="col-md-5">
                    <Select
                        options={availableActivities}
                        isMulti
                        components={animatedComponents}
                        name="search-activities-tags"
                        placeholder="Ką norėtumėte aplankyti?"
                        className="search-tags-select"
                        onChange={updateActivityFilters}
                    />
                </div>
                <div className="col-md-4">
                    <Select
                        options={availableLocations}
                        isMulti
                        components={animatedComponents}
                        name="search-locations-tags"
                        placeholder="Kokios apskritys domina?"
                        className="search-tags-select"
                        onChange={updateLocationFilters}
                    />
                </div>
                <div className="col-md-3">
                    <Button className="btn-round" type="submit">Ieškoti</Button>
                </div>
            </div>
        </Form>
    );
};

export default Search;

import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import logo from '../../../shared/assets/img/logo.svg';
import "./index.scss";
import LoginModal from "../../modals/Login";
import {Button} from "react-bootstrap";
import RegistrationModal from "../../modals/Registration";
import PlaceModal from "../../modals/Place";
import {useCalloutContext} from "../../../shared/context/CalloutContext";
import {UserModel} from "../../../shared/models/response/User/UserModel";
import {getAuthenticatedUser} from "../../../shared/utils/AuthUtils";
import {useRouteMatch} from "react-router";

const Header = () => {
    const [showLoginModal, setShowLoginModal] = useState(false);
    const handleLoginModalClose = () => setShowLoginModal(false);
    const handleLoginModalShow = () => setShowLoginModal(true);

    const [showRegistrationModal, setShowRegistrationModal] = useState(false);
    const handleRegistrationModalClose = () => setShowRegistrationModal(false);
    const handleRegistrationModalShow = () => setShowRegistrationModal(true);

    const [showPlaceModal, setPlaceModal] = useState(false);
    const handlePlaceModalClose = () => setPlaceModal(false);
    const handlePlaceModalShow = () => setPlaceModal(true);
    const {calloutData} = useCalloutContext();
    const authenticatedUser: UserModel | null = getAuthenticatedUser();

    const onMenuItemClick = (event:any) => {
        const items = document.querySelectorAll('.menu-item a').forEach((element) => {
            element.classList.remove('active');
        });
        event.target.classList.add('active')


        const item = document.querySelector('.btn-close-mobile-menu');
        if (item) {
            //@ts-ignore
            item.click();
        }
    };

    return (
        <header className={"main-app " + (calloutData.type.length > 0 ? 'callout-active' : '')}>
            <div className="top-bar" id="js-top-bar">
                <div className="row">
                    <div className="col-md-3 d-block logo">
                        <a href="/">
                            <img src={logo} alt=""/>
                        </a>
                    </div>
                    <div className="col-md-5 top-menu">
                        <div className="menu-main-menu-container">
                            <nav className="navbar navbar-expand-lg navbar-light">
                                <button className="navbar-toggler collapsed" type="button" data-toggle="collapse"
                                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                        aria-expanded="false" aria-label="Toggle navigation">
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                    <span className="icon-bar"></span>
                                </button>
                                <div className="navbar-collapse collapse navbar-content" id="navbarSupportedContent">
                                    <div className="menu-main-menu-container">
                                        <ul id="menu-main-menu" className="menu">
                                            <li className="menu-item">
                                                <Link className={(window.location.pathname === '/' ? 'active' : '')} onClick={onMenuItemClick} to='/'>Vietų paieška</Link>
                                            </li>
                                            <li className="menu-item">
                                                <Link className={(window.location.pathname.includes('/about') ? 'active' : '')} onClick={onMenuItemClick} to='/about'>Apie projektą</Link>
                                            </li>
                                            <li className="menu-item">
                                                <Link className={(window.location.pathname.includes('/blog') ? 'active' : '')} onClick={onMenuItemClick} to='/blog'>Kelionių istorijos</Link>
                                            </li>
                                        </ul>
                                    </div>
                                    <button className="btn-close-mobile-menu btn d-block d-md-none" data-toggle="collapse"
                                            data-target="#navbarSupportedContent">
                                        Uždaryti
                                    </button>
                                </div>
                            </nav>
                        </div>
                    </div>
                    <div className="col-md-4 vertical-center action-buttons">
                        <div className="btn-group actions">
                            {
                                authenticatedUser
                                    ? <Link className="btn btn-primary px-3 btn-round" to="/user/trips">
                                        Mano paskyra
                                    </Link>
                                    : <Button className="btn btn-primary px-3 btn-round" onClick={handleLoginModalShow}>
                                        Prisijungti
                                    </Button>
                            }
                            <Button className="btn btn-primary btn-round mx-3 px-3 btn-add-place" onClick={handlePlaceModalShow}>
                                Pridėti vietovę
                            </Button>
                        </div>
                        <LoginModal showModal={showLoginModal}
                                    handleModalClose={handleLoginModalClose}
                                    handleModalShow={handleLoginModalShow}
                                    handleRegistrationModalShow={handleRegistrationModalShow}
                        />
                        <RegistrationModal showModal={showRegistrationModal}
                                           handleModalClose={handleRegistrationModalClose}
                                           handleModalShow={handleRegistrationModalShow}
                                           handleLoginModalShow={handleLoginModalShow}
                        />
                        <PlaceModal showModal={showPlaceModal}
                                    handleModalClose={handlePlaceModalClose}
                                    handleLoginModalShow={handlePlaceModalShow}
                        />
                    </div>
                </div>
            </div>
        </header>
    );
};

export default Header;

import React, {useEffect} from 'react';
import Search from "../Search";
import "./index.scss";
import {Place} from "../../../shared/assets/models/Place";
import {HomepagePreviewModes} from "../../enum/AppEnum";
import {useAppContext} from "../../context/AppContext";

type Props = {
    places: Place[],
    setPlaces: any,
};

const Banner = ({places, setPlaces}: Props) => {
    const {appData, setAppData} = useAppContext();
    const switchPreviewMode: any = (previewMode: HomepagePreviewModes) => {
        setAppData({
            ...appData,
            homepagePreviewMode: previewMode,
        });
        localStorage.setItem('homepagePreviewMode', previewMode);
    };

    useEffect(() => {
        const previewType = localStorage.getItem('homepagePreviewMode');

        if (!previewType) {
            localStorage.setItem('homepagePreviewMode', HomepagePreviewModes.MAP);
        }
    }, []);

    return (
        <div className="banner" id="banner">
            <div className="banner-inner">
                <div className="heading">
                    Atraskite jus supančias vietas
                </div>
                <Search places={places} setPlaces={setPlaces}/>
            </div>
            <div className="preview-switcher">
                <button
                    className={"btn btn-primary" + (localStorage.getItem('homepagePreviewMode') === HomepagePreviewModes.MAP ? ' active' : '')}
                    onClick={switchPreviewMode.bind(this, HomepagePreviewModes.MAP)}>
                    <i className="icon-map"></i>
                </button>
                <button
                    className={"btn btn-primary" + (localStorage.getItem('homepagePreviewMode') === HomepagePreviewModes.LIST ? ' active' : '')}
                    onClick={switchPreviewMode.bind(this, HomepagePreviewModes.LIST)}>
                    <i className="icon-grid"></i>
                </button>
            </div>
        </div>
    );
};

export default Banner;

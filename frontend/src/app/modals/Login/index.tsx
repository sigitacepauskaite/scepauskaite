import React, {useState} from 'react';
import {Modal} from 'react-bootstrap';
import './index.scss';
import {getCurrentUser, loginAction} from "../../actions/UserActions";
import {history} from "../../../shared/config/history";
import {LoginResponse, UserModel} from "../../../shared/models/response/User/UserModel";
import ModalSpinner from "../../../shared/components/ModalSpinner";
import {defaultLoginData, LoginFormData} from "../../../shared/models/request/UserModels";


type Props = {
    showModal: boolean,
    handleModalClose: any,
    handleModalShow: any,
    handleRegistrationModalShow: any,
};

const LoginModal = ({showModal, handleModalClose, handleModalShow, handleRegistrationModalShow}: Props) => {
    const [error, setError] = useState('');
    const [formData, setFormData] = useState(defaultLoginData as LoginFormData);
    const [loading, setLoading] = useState(false);

    const onInputChange = (event: any) => {
        if (event.target.name === 'email') {
            setFormData({
                ...formData,
                email: event.target.value
            });
        }

        if (event.target.name === 'password') {
            setFormData({
                ...formData,
                password: event.target.value
            });
        }
    };

    const onRegistrationLinkClick = () => {
        handleModalClose();
        handleRegistrationModalShow();
    };

    const onFormSubmit = (event: any) => {
        event.preventDefault();

        if (formData.email.length === 0) {
            setError('Privaloma nurodyti vartotojo prisijungimo vardą');

            return;
        }

        if (formData.password.length === 0) {
            setError('Privaloma nurodyti prisijungimo slaptažodį');

            return;
        }

        setLoading(true);

        loginAction(formData)
            .then((response) => {
                const responseData: LoginResponse = response.data;
                localStorage.setItem('authorizationToken', responseData.token);

                getCurrentUser()
                    .then((response) => {
                        const responseData: UserModel = response.data;
                        localStorage.setItem('authorizedUser', JSON.stringify(responseData));

                        history.push("/user/trips");

                        setLoading(false);
                    })
            })
            .catch((error) => {
                setError(error.response.data.message);

                setLoading(false);
                return;
            })
    };

    return (
        <>
            <Modal className="modal-login" show={showModal} onHide={handleModalClose}>
                <Modal.Body>
                    <ModalSpinner active={loading}/>
                    <h2 className="text-center">
                        Prisijungimas
                    </h2>
                    <div className="login-form">
                        {
                            error.length > 0
                                ? <div className="alert alert-danger">
                                    {error}
                                </div>
                                : null
                        }
                        <form action="" onSubmit={onFormSubmit}>
                            <div className="form-group">
                                <input type="text" className="form-control" name="email" placeholder="El. pašto adresas"
                                       onChange={onInputChange}/>
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" name="password"
                                       placeholder="Slaptažodis"
                                       onChange={onInputChange}/>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary btn-block">Prisijungti</button>
                            </div>
                        </form>
                    </div>
                    <hr/>
                    <div className="signup-link text-right">
                        <p>Neturite vartotojo paskyros? <span onClick={onRegistrationLinkClick}>Užsiregistruokite</span>
                        </p>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
};

export default LoginModal;

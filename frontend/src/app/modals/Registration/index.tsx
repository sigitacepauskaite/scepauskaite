import React, {useState} from 'react';
import {Modal, Button} from 'react-bootstrap';
import './index.scss';
import {registerAction} from "../../actions/UserActions";
import {history} from "../../../shared/config/history";
import ModalSpinner from "../../../shared/components/ModalSpinner";
import {defaultRegistrationData, RegistrationFormData} from "../../../shared/models/request/UserModels";

type Props = {
    showModal: any,
    handleModalClose: any,
    handleModalShow: any,
    handleLoginModalShow: any,
};

const RegistrationModal = ({showModal, handleModalClose, handleModalShow, handleLoginModalShow}: Props) => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');
    const [formData, setFormData] = useState(defaultRegistrationData as RegistrationFormData);

    const onLoginLinkClick = () => {
        handleModalClose();
        handleLoginModalShow();
    };

    const onInputChange = (event: any) => {
        if (event.target.name === 'email') {
            setFormData({
                ...formData,
                email: event.target.value
            });
        }

        if (event.target.name === 'password') {
            setFormData({
                ...formData,
                password: event.target.value
            });
        }

        if (event.target.name === 'firstName') {
            setFormData({
                ...formData,
                firstName: event.target.value
            });
        }

        if (event.target.name === 'lastName') {
            setFormData({
                ...formData,
                lastName: event.target.value
            });
        }
    };

    const onFormSubmit = (event: any) => {
        event.preventDefault();
        setFormData({
            ...formData,
        });

        if (formData.email.length === 0) {
            setError('Privaloma nurodyti el. pašto adresą');

            return;
        }

        const emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!emailPattern.test(formData.email)) {
            setError('Įvestas netinkamas el. pašto adresas');

            return;
        }

        if (formData.firstName.length === 0) {
            setError('Privaloma nurodyti vardą');

            return;
        }

        if (formData.lastName.length === 0) {
            setError('Privaloma nurodyti pavardę');

            return;
        }

        if (formData.password.length === 0) {
            setError('Privaloma nurodyti slaptažodį');

            return;
        }

        setError('');
        setLoading(true);

        registerAction(formData)
            .then((response) => {
                history.push("/user");

                setLoading(false);
            })
            .catch((error) => {
                setError(error.response.data.message);

                setLoading(false);
            });
    };

    return (
        <>
            <Modal className="modal-login" show={showModal} onHide={handleModalClose}>
                <Modal.Body>
                    <ModalSpinner active={loading} />
                    <h2 className="text-center">
                        Registracija
                    </h2>
                    <div className="login-form">
                        {
                            error.length > 0
                                ? <div className="alert alert-danger">
                                    {error}
                                </div>
                                : null
                        }
                        <form action="" onSubmit={onFormSubmit}>
                            <div className="form-group">
                                <input type="text" className="form-control" name="email" placeholder="El. pašto adresas"
                                       onChange={onInputChange}/>
                            </div>
                            <div className="form-group">
                                <div className="row">
                                    <div className="col-md-6">
                                        <input type="text" className="form-control my-0" name="firstName" placeholder="Vardas"
                                               onChange={onInputChange}/>
                                    </div>
                                    <div className="col-md-6">
                                        <input type="text" className="form-control my-0" name="lastName" placeholder="Pavardė"
                                               onChange={onInputChange}/>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <input type="password" className="form-control" name="password" placeholder="Slaptažodis"
                                       onChange={onInputChange}/>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-primary btn-block">Registruotis</button>
                            </div>
                        </form>
                    </div>
                    <hr/>
                    <div className="signup-link text-right">
                        <p>Jau turite vartotojo paskyrą?&nbsp;
                            <span onClick={onLoginLinkClick}>Prisijunkite</span>
                        </p>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
};

export default RegistrationModal;

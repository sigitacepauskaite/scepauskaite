import React from 'react';
import {Place} from "../../../shared/assets/models/Place";
import Carousel from "@brainhubeu/react-carousel";

type Props = {
    currentPlace: Place;
    loadPlacePreviewModal: any;
};

const PlacesListItem = ({currentPlace,loadPlacePreviewModal}: Props) => {
        return (
            <div className="places-list-item">
                <div className="row h-100">
                    <div className="col-5 h-100">
                        <Carousel plugins={['arrows']}
                        >
                            {
                                currentPlace.photos?.map((photo) => (
                                    <div className="cover-image" style={{backgroundImage: 'url(' + (photo.url) + ')'}} />
                                ))
                            }
                        </Carousel>
                    </div>
                    <div className="col-7 h-100 place-preview-summary-right">
                        <div className="content">
                            <p className="place-name">{currentPlace.name}</p>
                            <p className="place-address">{currentPlace.address}</p>
                            <p className="place-address">{currentPlace.latitude} / {currentPlace.longitude}</p>
                            <hr/>
                            <p className="description">
                                {currentPlace.description}
                            </p>
                        </div>
                        <div className="read-more">
                            <button className="btn btn-primary btn-round px-5" onClick={loadPlacePreviewModal.bind(this, currentPlace)}>
                                Plačiau
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
;

export default PlacesListItem;

import React, {useEffect, useState} from 'react';
import {Place} from "../../../shared/assets/models/Place";
import Map from "../../../shared/components/Map/Map";
import PlacesSidebarItem from "./PlacesSidebarItem";

type Props = {
    places: Place[],
    setPlaces: any,
};

const HomepageMapPreview = ({places, setPlaces}: Props) => {
    // window.onscroll = debouncee(() => {
    //     const {
    //         loadApods
    //     } = this;
    //
    //     if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
    //         loadApods();
    //     }
    // }, 100);

    return (
        <div className="row mx-0">
            <div className="col-md-4 px-0">
                <div className="map-places-list">
                    {
                        places.map((place: Place, key: any) => (
                                <PlacesSidebarItem
                                    key={key}
                                    currentPlace={place}
                                    places={places}
                                    setPlaces={setPlaces}
                                />
                            )
                        )
                    }
                </div>
            </div>
            <div className="col-md-8 px-0">
                <div style={{height: '100vh', width: '100%'}}>
                    <Map selectorId="homepage-map" showPlaceInfoBox={true} places={places} zoom={7.7}/>
                </div>
            </div>
        </div>
    );
};

export default HomepageMapPreview;

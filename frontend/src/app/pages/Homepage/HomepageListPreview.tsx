import React, {useState} from 'react';
import {Place} from "../../../shared/assets/models/Place";
import PlacesListItem from "./PlacesListItem";
import PlacePreviewModal from "../../../shared/modals/PlacePreview";

type Props = {
    places: Place[],
    setPlaces: any,
};

const HomepageListPreview = ({places, setPlaces}: Props) => {
    const [placePreviewModalState, setPlacePreviewModalState] = useState(false);
    const [placeToPreview, setPlaceToPreview] = useState({} as Place);

    const loadPlacePreviewModal = (place: Place) => {
        setPlacePreviewModalState(true);
        setPlaceToPreview(place);
    };

    const handleModalClose = () => {
        setPlacePreviewModalState(false);
    };

    return (
        <>
            {
                places.map((place: Place, key: any) => (
                        <PlacesListItem
                            key={key}
                            currentPlace={place}
                            loadPlacePreviewModal={loadPlacePreviewModal.bind(this, place)}
                        />
                    )
                )
            }
            <PlacePreviewModal place={placeToPreview} showModal={placePreviewModalState}
                               handleModalClose={handleModalClose}/>
        </>
    );
};

export default HomepageListPreview;

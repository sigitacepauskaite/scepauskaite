import React, {useEffect, useState} from 'react';
import {Place} from "../../../shared/assets/models/Place";
import {getPlacesAppAction} from "../../actions/PlaceActions";
import Banner from "../../components/Banner";
import "./index.scss"
import {useAppContext} from "../../context/AppContext";
import {HomepagePreviewModes} from "../../enum/AppEnum";
import HomepageMapPreview from "./HomepageMapPreview";
import HomepageListPreview from "./HomepageListPreview";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";

const Homepage = () => {
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const [places, setPlaces] = useState([] as Place[]);
    const {appData, setAppData} = useAppContext()

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });
        getPlacesAppAction(0, 20)
            .then((response) => {
                setPlaces(response.data);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    // window.onscroll = debouncee(() => {
    //     const {
    //         loadApods
    //     } = this;
    //
    //     if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.offsetHeight) {
    //         loadApods();
    //     }
    // }, 100);

    return (
        <>
            <Banner places={places} setPlaces={setPlaces}/>
            <div className="homepage">
                {
                    localStorage.getItem('homepagePreviewMode') === HomepagePreviewModes.MAP
                        ? <HomepageMapPreview places={places} setPlaces={setPlaces}/>
                        : <HomepageListPreview places={places} setPlaces={setPlaces}/>
                }
            </div>
        </>
    );
};

export default Homepage;

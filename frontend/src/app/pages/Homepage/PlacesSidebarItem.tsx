import React from 'react';
import {Place} from "../../../shared/assets/models/Place";
import markerImg from '../../../shared/assets/img/marker.svg';

type Props = {
    currentPlace: Place;
    places: any;
    setPlaces: any;
};

const PlacesSidebarItem = ({currentPlace, places, setPlaces}: Props) => {
        const onSidebarPlaceClick = (place: Place) => {
            places.forEach((place: Place) => {
                place.centered = false;
                place.icon = markerImg;
            });

            const foundPlaceIndex: number = places.findIndex((iterablePlace: Place) => iterablePlace.id === place.id);
            places[foundPlaceIndex].centered = true;
            places[foundPlaceIndex].icon = 'https://icons.iconarchive.com/icons/paomedia/small-n-flat/1024/map-marker-icon.png';

            setPlaces([...places]);
        };

        return (
            <div className="map-places-list-item" onClick={onSidebarPlaceClick.bind(this, currentPlace)}>
                <div className="row h-100">
                    <div className="col-3 h-100 vertical-center">
                        <img className="img img-responsive img-thumbnail ml-4" src={
                            currentPlace.photos && currentPlace.photos.length > 0
                                ? currentPlace.photos[0].url
                                : 'https://medmarinetravel.com/wp-content/plugins/aheto/assets/images/placeholder.jpg'
                        }/>
                    </div>
                    <div className="col-9">
                        <div className="item-content">
                            <p>
                                <b>{currentPlace.name}</b>
                            </p>
                            <p>
                                {currentPlace.address}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
;

export default PlacesSidebarItem;

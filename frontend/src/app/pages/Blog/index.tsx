import React, {useEffect, useState} from 'react';
import {ArticleModel} from "../../../shared/models/request/ArticleModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {getArticlesAppAction} from "../../actions/ArticleActions";
import Carousel from "@brainhubeu/react-carousel";
import {Link} from 'react-router-dom';

const PublicBlogPage = () => {
    const [articles, setArticles] = useState([] as ArticleModel[]);
    const {spinnerData, setSpinnerData} = useSpinnerContext();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getArticlesAppAction(0, 30)
            .then((response) => {
                const responseItems = response.data;

                setArticles(responseItems);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                })
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    return (
        <div className="col-md-10 mx-auto mt-5 articles-list-grid">
            {
                articles.map((article: ArticleModel) => (
                    <div className="article">
                        <div className="card">
                            <div className="row h-100">
                                <div className="col-md-4 h-100 position-relative overflow-hidden">
                                    <Carousel plugins={['arrows']}
                                    >
                                        {
                                            article.photos?.map((photo) => (
                                                <div className="cover-image"
                                                     style={{backgroundImage: 'url(' + (photo.url.length > 0 ? photo.url : 'https://medmarinetravel.com/wp-content/plugins/aheto/assets/images/placeholder.jpg') + ')'}}/>
                                            ))
                                        }
                                    </Carousel>
                                </div>
                                <div className="col-md-8 h-100">
                                    <div className="content">
                                        <p className="title">{article.name}</p>
                                        <hr/>
                                        <p className="description">
                                            {article.content}
                                        </p>
                                        <div className="read-more">
                                            <Link to={"/blog/"+article.id} className="btn btn-primary btn-round px-5">
                                                Plačiau
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            }
        </div>
    );
}

export default PublicBlogPage;

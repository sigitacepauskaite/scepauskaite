import React, {useEffect, useState} from 'react';
import {ArticleModel, ArticleModelData} from "../../../shared/models/request/ArticleModels";
import {useSpinnerContext} from "../../../shared/context/SpinnerContext";
import {getArticleAppAction} from "../../actions/ArticleActions";
import Carousel from "@brainhubeu/react-carousel";
import {useParams} from "react-router-dom";

const PublicBlogPreviewPage = () => {
    const [article, setArticle] = useState(ArticleModelData as ArticleModel);
    const {spinnerData, setSpinnerData} = useSpinnerContext();
    const {id}: any = useParams();

    useEffect(() => {
        setSpinnerData({
            ...spinnerData,
            active: true,
        });

        getArticleAppAction(id)
            .then((response) => {
                const responseData: ArticleModel = response.data;

                setArticle(responseData);

                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            })
            .catch(() => {
                setSpinnerData({
                    ...spinnerData,
                    active: false,
                });
            });
    }, []);

    return (
        <div className="col-md-10 mx-auto mt-5 articles-preview-display">
            {
                <div className="article">
                    <div className="card">
                        <div className="row h-100">
                            <div className="col-md-4 h-100 position-relative overflow-hidden special-images">
                                <Carousel plugins={['arrows']}
                                >
                                    {
                                        article.photos?.map((photo) => (
                                            <div className="cover-image"
                                                 style={{backgroundImage: 'url(' + (photo.url.length > 0 ? photo.url : 'https://medmarinetravel.com/wp-content/plugins/aheto/assets/images/placeholder.jpg') + ')'}}/>
                                        ))
                                    }
                                </Carousel>
                            </div>
                            <div className="col-md-8 h-100">
                                <div className="content">
                                    <p className="title">{article.name}</p>
                                    <hr/>
                                    <p className="description">
                                        {article.content}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </div>
    );
}

export default PublicBlogPreviewPage;

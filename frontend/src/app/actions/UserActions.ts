import axios from "axios";
import {LoginFormData, RegistrationFormData, UserRequest} from "../../shared/models/request/UserModels";
import {UserModel} from "../../shared/models/response/User/UserModel";
import {getAuthenticatedUserToken} from "../../shared/utils/AuthUtils";

export const loginAction = (data: LoginFormData) => {
    return axios.post('/api/login_check', {
        username: data.email,
        password: data.password
    });
};

export const registerAction = (data: RegistrationFormData) => {
    return axios.post('/api/public/users/countries/' + data.countryCode, {
        email: data.email,
        password: data.password,
        firstName: data.firstName,
        lastName: data.lastName,
    });
};


export const getCurrentUser = () => {
    return axios.get('/api/users/current', {
        headers: {
            authorization: 'Bearer ' + getAuthenticatedUserToken(),
        }
    });
};

export const updateUserAppAction = (id:number, data: UserRequest) => {
    return axios.put(
        '/api/users/' + id,
        data,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

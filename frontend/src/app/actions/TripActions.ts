import axios from "axios";
import {TripModel} from "../../shared/models/main/Map";
import {getAuthenticatedUserToken} from "../../shared/utils/AuthUtils";

export const getTrips = (from: number, count: number) => {
    return axios.get(
        '/api/trips',
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
                start: from,
                limit: count,
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const getTrip = (id: number) => {
    return axios.get(
        '/api/trips/' + id,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const createTrip = (data: TripModel) => {
    return axios.post(
        '/api/trips',
        data,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const updateTrip = (id: number, data: TripModel) => {
    return axios.put(
        '/api/trips/' + id,
        data,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};


export const addTripStop = (tripId: number, placeId: number, note?: string) => {
    return axios.post(
        '/api/trips/'+tripId+'/places/'+placeId+'/tripStops',
        {
            note: note,
        },
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const removeTrip = (id: number) => {
    return axios.delete(
        '/api/trips/'+id,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const removeTripStop = (stopId: number) => {
    return axios.delete(
        '/api/trips/tripStops/'+stopId,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

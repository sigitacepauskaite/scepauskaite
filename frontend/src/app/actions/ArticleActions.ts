import axios from "axios";
import {ArticleRequest} from "../../shared/models/request/ArticleModels";
import {getAuthenticatedAdminUserToken, getAuthenticatedUserToken} from "../../shared/utils/AuthUtils";

export const getArticlesAppAction = (from: number, count: number, userId?: number) => {
    return axios.get(
        '/api/public/articles',
        {
            params: {
                start: from,
                limit: count,
                userId: userId ? userId : null,
            }
        }
    );
};

export const getArticleAppAction = (id: number) => {
    return axios.get(
        '/api/public/articles/'+id,
    );
};

export const createArticleAppAction = (articleRequest: ArticleRequest) => {
    return axios.post(
        'api/articles',
        articleRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const updateArticleAppAction = (id: number, articleRequest: ArticleRequest) => {
    return axios.put(
        'api/articles/'+id,
        articleRequest,
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const uploadArticlePhotosAppAction = (files: File[], placeId: number) => {
    const formData : FormData = new FormData();

    files.forEach((file) => {
        formData.append('images[]', file)
    });

    return axios.post(
        'api/articles/'+placeId+'/photos',
        formData,
        {
            headers: {
                'Content-type' : 'multipart/form-data',
                'authorization': 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const deleteArticlePhotosAppAction = (articleId: number, photoId: number) => {
    return axios.delete(
        'api/articles/'+articleId+'/photos/'+photoId,
        {
            headers: {
                'Content-type' : 'multipart/form-data',
                'authorization': 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const removeArticle = (id: number) => {
    return axios.delete(
        '/api/articles/'+id,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

import {PlaceRequest} from "../../shared/models/request/PlaceModels";
import axios from "axios";
import {PlaceRate} from "../../shared/assets/models/Place";
import {getAuthenticatedUserToken} from "../../shared/utils/AuthUtils";

export const getPlacesAppAction = (
    from: number,
    count: number,
    activityFilters?: string[],
    locationFilters?: string[],
    searchText?: string,
) => {
    if (!activityFilters) {
        activityFilters = [];
    }

    if (!locationFilters) {
        locationFilters = [];
    }

    return axios.get(
        '/api/public/places',
        {
            params: {
                start: from,
                limit: count,
                activityFilters: activityFilters,
                locationFilters: locationFilters,
                searchText: searchText,
                XDEBUG_SESSION_START: 'PHPSTORM',
            }
        }
    );
};

export const createPlace = (createPlaceRequest: PlaceRequest) => {
    return axios.post(
        '/api/public/places/countries/LT',
        createPlaceRequest,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            }
        }
    );
};

export const uploadPlacePhotos = (files: File[], placeId: number) => {
    const formData: FormData = new FormData();

    files.forEach((file) => {
        formData.append('images[]', file)
    });

    return axios.post(
        'api/public/places/' + placeId + '/photos',
        formData,
        {
            headers: {
                'Content-type': 'multipart/form-data',
            }
        }
    );
};

export const addPlaceVisitAppAction = (request: PlaceRate) => {
    return axios.post(
        '/api/places/' + request.place.id + '/visits',
        request,
        {
            params: {
                XDEBUG_SESSION_START: 'PHPSTORM',
            },
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

export const getVisitedPlacesAppAction = () => {
    return axios.get(
        '/api/places/visited',
        {
            headers: {
                authorization: 'Bearer ' + getAuthenticatedUserToken(),
            }
        }
    );
};

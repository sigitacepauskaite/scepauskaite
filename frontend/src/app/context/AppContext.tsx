import React, {useMemo, useState} from 'react'
import {HomepagePreviewModes} from "../enum/AppEnum";

type AppContextData = {
    homepagePreviewMode: HomepagePreviewModes,
};

const contextDefaults: AppContextData = {
    homepagePreviewMode: HomepagePreviewModes.MAP,
};

const AppContext = React.createContext<AppContextData>(contextDefaults);

export function AppContextProvider(props: any): any {
    const [appData, setAppData] = useState(contextDefaults);
    const value : any = useMemo(() => ({ appData, setAppData }), [appData]);

    return (
        <AppContext.Provider value={value}>
            {props.children}
        </AppContext.Provider>
    )
}

export function useAppContext(): any {
    const context = React.useContext(AppContext);
    if (!context) {
        throw new Error(`AppContext must be used within a Provider`);
    }

    return context;
}

import React from 'react';
import AppContainer from "./AppContainer";
import "./assets/scss/index.scss";
import {SpinnerContextProvider} from "../shared/context/SpinnerContext";
import {CalloutContextProvider} from "../shared/context/CalloutContext";
import {AppContextProvider} from "./context/AppContext";

function App() {
    return (
        <AppContextProvider>
            <CalloutContextProvider>
                <SpinnerContextProvider>
                    <AppContainer/>
                </SpinnerContextProvider>
            </CalloutContextProvider>
        </AppContextProvider>
    );
}

export default App;
